var myChart;
function generarEstadistica(tipo) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: 'generarEstadistica',
        data:{
            estado : $('#estado').val(),
            mes1 : $('#mes1').val(),
            mes2 : $('#mes2').val(),
            mes3 : $('#mes3').val(),
            year1 : $('#year1').val(),
            year2 : $('#year2').val(),
            year3 : $('#year3').val(),
            tipo : tipo
        },
        success: function (response) {
            var ctx= document.getElementById("myChart").getContext("2d");
            var gradientStroke = ctx.createLinearGradient(0, 800, 0, 100);
            gradientStroke.addColorStop(0, 'rgba(255, 255, 255,0)');
            gradientStroke.addColorStop(1, '#28a745');
            if (myChart != undefined) {
                 myChart.destroy()
            }
            if(response.type == "year"){
                meses = response.month
                myChart= new Chart(ctx,{
                    type: "bar",
                    data: {
                        labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                        datasets: [{
                        label: "Objetos "+response.estado+" - "+response.year,
                        borderColor: "#28a745",
                        borderWidth: 1,
                        data: [
                            meses[0].Enero,
                            meses[0].Febrero,
                            meses[0].Marzo,
                            meses[0].Abril,
                            meses[0].Mayo,
                            meses[0].Junio,
                            meses[0].Julio,
                            meses[0].Agosto,
                            meses[0].Septiembre,
                            meses[0].Octubre,
                            meses[0].Noviembre,
                            meses[0].Diciembre,
                        ],
                        backgroundColor: gradientStroke,
                        hoverBackgroundColor: 'rgba(0, 175, 49,0.4)',
                        }]
                    },
                    options:{
                        scales:{
                            yAxes:[{
                                    ticks:{
                                        beginAtZero:true
                                    }
                            }]
                        }
                        
                    }
                });
            } else{
                objetos = response.month
     
                const label = [];
                const data = [];
                for (let i = 1; i < objetos.length + 1 ; i++) {
                    label.push(i);
                }
                for (let i = 0; i < objetos.length ; i++) {
                    data.push(objetos[i]);
                }
                myChart= new Chart(ctx,{
                    type: "bar",
                    data: {
                        labels: label,
                        datasets: [{
                        label: "Objetos "+response.estado+" - "+response.year,
                        borderColor: "#28a745",
                        borderWidth: 1,
                        data: data,
                        backgroundColor: gradientStroke,
                        hoverBackgroundColor: 'rgba(0, 175, 49,0.4)',
                        }]
                    },
                    options:{
                        scales:{
                            yAxes:[{
                                    ticks:{
                                        beginAtZero:true
                                    }
                            }]
                        }
                        
                    }
                });
            }
           


        },
        statusCode: {
            404: function () {
                alert('web not found');
            }
        },
    })
}