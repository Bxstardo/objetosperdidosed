/* cargar mes*/
tipo = 1;

function Cmes(tip) {
    tipo = tip;
    if (tipo==1) {
        $('#estado').val(3);
    }else if(tipo==2){
        $('#estado').val(5);
    }else{
        $('#estado').val(4);
    }
}

$('.mes').change(function (){
    mes = "#mes";
    year = "#year";
    estado = 0;
    if (tipo==1) {
        mes += "1";
        year += "1";
        estado = 3;   
    }else if(tipo==2){
        mes += "2";
        year += "2";
        estado = 5;
    }else{
        mes += "3";
        year += "3";
        estado = 4;
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
        }
    }); 
    $.ajax({
        url : 'mes',
        data : {
            estado : estado,
            year : $(year).val()
        },  
        success: function (response) {
            var month =   response.month;

            datos = '';
            datos += '<option selected disabled value="">Mes</option>'
            for (i in month) {
                var month_actual = month[i];
                    datos += '<option value="' + month_actual.fecha + '">';
                    if (month_actual.fecha == 1) {
                        datos += 'Enero';
                    }else if(month_actual.fecha == 2){
                        datos += 'Febrero';
                    }else if(month_actual.fecha == 3){
                        datos += 'Marzo';
                    }else if(month_actual.fecha == 4){
                        datos += 'Abril';
                    }else if(month_actual.fecha == 5){
                        datos += 'Mayo';
                    }else if(month_actual.fecha == 6){
                        datos += 'Junio';
                    }else if(month_actual.fecha == 7){
                        datos += 'Julio';
                    }else if(month_actual.fecha == 8){
                        datos += 'Agosto';
                    }else if(month_actual.fecha == 9){
                        datos += 'Septiembre';
                    }else if(month_actual.fecha == 10){
                        datos += 'Octubre';
                    }else if(month_actual.fecha == 11){
                        datos += 'Noviembre';
                    }else if(month_actual.fecha == 12){
                        datos += 'Diciembre';
                    }
                    datos += '</option>';
            }
            $(mes).html(datos);
        },
        statusCode: {
            404: function() {
                alert('web not found');
            }
            },
            


    

    })
});

