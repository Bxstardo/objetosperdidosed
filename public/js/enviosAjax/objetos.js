
function datatablePeticion(){
    $("#peticionesDatatable").dataTable({
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ objetos",
            "sZeroRecords": "No se encontraron objetos",
            "sEmptyTable": "No se encontraron objetos",
            "sInfo": "Mostrando objetos del _START_ al _END_ de un total de _TOTAL_ objetos",
            "sInfoEmpty": "Mostrando objetos del 0 al 0 de un total de 0 objetos",
            "sInfoFiltered": "(filtrado de un total de _MAX_ objetos)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive: true

    });

}
function datatableTipo(){
    $("#tipoDatatable").dataTable({
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ peticiones",
            "sZeroRecords": "No se encontraron peticiones pendientes por validar",
            "sEmptyTable": "No se encontraron peticiones pendientes por validar",
            "sInfo": "Mostrando peticiones del _START_ al _END_ de un total de _TOTAL_ peticiones",
            "sInfoEmpty": "Mostrando peticiones del 0 al 0 de un total de 0 peticiones",
            "sInfoFiltered": "(filtrado de un total de _MAX_ peticiones)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        lengthMenu:	[[5, 10, 20, 25, 50, -1], [5, 10, 20, 25, 50, "Todos"]],
        bFilter: false,
        bInfo: false,
        bLengthChange : false,
        responsive: true
    
    });

}

/* Otros */

$("#tipoObjeto").change(function () {
    if ($('#tipoObjeto option:selected').val() == "otros") {
        var contenido = '<label for="otros">Nombre de objeto</label>'
        contenido += '<input type="text" name="otros" class="form-control form-control-lg" id="otros" pattern="[a-zA-Z ]+" required>'
        contenido += '<hr>'
        contenido += "<script>bootstrapValidate('#otros', 'regex:[a-zA-Z ]+:Solo puedes utilizar letras y sin espacios')</script>"
        $(".objeto-otro").html(contenido);
    } else {
        $(".objeto-otro").html("<input type='hidden' name='otros' value=''>");
    }
});

/* cargar lugar*/
$("#piso").change(function (e) {	 
    e.preventDefault();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
        }
    }); 
    $.ajax({
        url : 'cargarLugares',
        data : {
            
            zona : $("#zona").val(),
            piso : $("#piso").val(),
        },  
        success: function (response) {
            

            var lugares =   response.lugares;
            datos = '';
            datos += '<option selected disabled value="lugar">Lugar</option>'
            for (i in lugares) {
                var lugares_actual = lugares[i];
                    datos += '<option value="' + lugares_actual.IdLugar + '">';
                    datos += lugares_actual.NombreLugar;
                    datos += '</option>';
            }
            var contenedor_lugares =   document.getElementById('lugar');
            contenedor_lugares.innerHTML = datos;
        },
        statusCode: {
            404: function() {
                alert('web not found');
            }
            },
            


    

    })
});
$("#zona").change(function (e) {	 
    e.preventDefault();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
        }
    }); 
    $.ajax({
        url : 'cargarLugares',
        data : {
            
            zona : $("#zona").val(),
            piso : $("#piso").val(),
        },  
        success: function (response) {
            

            var lugares =   response.lugares;
            datos = '';
            datos += '<option selected disabled value="lugar">Lugar</option>'
            for (i in lugares) {
                var lugares_actual = lugares[i];
                    datos += '<option value="' + lugares_actual.IdLugar + '">';
                    datos += lugares_actual.NombreLugar;
                    datos += '</option>';
            }
            var contenedor_lugares =   document.getElementById('lugar');
            contenedor_lugares.innerHTML = datos;
        },
        statusCode: {
            404: function() {
                alert('web not found');
            }
            },
            


    

    })
});



function agregarTipo()
{
    if(!validacionTipo) return
    swal({
        title: "¿Estas segur@?",
        text: "Estas apunto de agregar un tipo",
        icon: "warning",
        buttons: {
            cancel : 'Cancelar',
            confirm : {text:'Si, estoy seguro',className:'sweet-warning'},
        },
    }).then((will)=>{
        if(will){
            $.ajax({
                url: 'agregarTipo',
                data: {
                    Tipo : $('#tipo').val()
                },
                type: "POST",
                success: function(response) {
                    let tipos = response.tipos
                    actualizarTipo(tipos)
                    swal({
                        title: "¡El tipo de objeto se ha agregado correctamente!",
                        text: "Se ha agregado el tipo de objeto "+$('#tipo').val(),
                        icon: "success",
                        button: "Cerrar",
                      });
                    $('#tipo').val("")
        
                },
                error: function() {
                    console.log("No se ha podido obtener la información");
                }
            });
        }else{
            $("#all_petugas").click();
        }
    })
   
}
//Validar Tipo

var validacionTipo = false;
var validacionTipoE = false;

$('#tipo').on('keyup', function() {
    if(validateInput('#tipo',['required','text']) == false){
        validacionTipo = false 
        return
    }
    $.ajax({
        url: 'validarTipo',
        data: {
            Tipo : $('#tipo').val()
        },
        type: "POST",
        success: function(response) {
            let validar = response.validar
            if (validar == false) {
                $('#tipo').addClass('is-invalid')
                $('#tipo-error').css('display','block')
                $('#tipo-error').html('Este tipo de objeto ya existe')
                validacionTipo = false
            }else{
                $('#tipo').addClass('is-valid')
                $('#tipo-error').css('display','none')
                validacionTipo = true
            }
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });
});

$('#tipoE').on('keyup', function() {
    if(validateInput('#tipoE',['required','text']) == false){
        validacionTipoE = false 
        return
    }
    validacionTipoE = true
});


IdTipoE = 0

function editarTipo()
{
    if(!validacionTipoE) return
    $.ajax({
        url: 'editarTipo',
        data: {
            IdTipo : IdTipoE,
            Tipo : $('#tipoE').val()
        },
        type: "POST",
        success: function(response) {
            if(response.validar == false) {
                $('#tipoE').addClass('is-invalid')
                $('#tipoE-error').css('display','block')
                $('#tipoE-error').html('Este tipo de objeto ya existe')
                validacionTipo = false
                return
            }
            else{
                let tipos = response.tipos
                actualizarTipo(tipos)
                volverTipo()
                swal({
                    title: "¡El tipo de objeto se ha actualizado correctamente!",
                    text: "Se ha editado el tipo de objeto de id "+IdTipoE,
                    icon: "success",
                    button: "Cerrar",
                });
            }
           
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });
}

function cambiarEstadoTipo(IdTipo,Estado) 
{
    $.ajax({
        url: 'cambiarEstadoTipo',
        data: {
            IdTipo : IdTipo,
            Estado : Estado
        },
        type: "POST",
        success: function(response) {
            let tipos = response.tipos
            actualizarTipo(tipos)
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });
}

function datosTipo(IdTipo) {
    $.ajax({
        url: 'datosTipo',
        data: {
            IdTipo : IdTipo,
        },
        type: "POST",
        success: function(response) {
            let tipo = response.tipo
            $('#tipoE').val(tipo.Tipo)
            $('#tipo-modal').modal('toggle')
            $('#tipoEditar-modal').modal('show')
            IdTipoE = tipo.IdTipo
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });
}

function volverTipo() {
    $('#tipoEditar-modal').modal('toggle')
    $('#tipo-modal').modal('show')
}

function actualizarTipo(tipos) {
    datos = ""
    for(i in tipos){
        datos += "<tr>"
        datos += "<td>"+tipos[i].IdTipo+"</td>"
        datos += "<td>"+tipos[i].Tipo+"</td>"
        datos += "<td>"
        if (tipos[i].Estado == 2) {
            datos += '<button class="btn btn-primary" onclick="cambiarEstadoTipo('+tipos[i].IdTipo+',1)">Activar'
            datos += '</button>'
        }else{
            datos += '<button class="btn btn-danger" onclick="cambiarEstadoTipo('+tipos[i].IdTipo+',2)">Inactivar'
            datos += '</button>'
        }
        datos += "</td>"
        datos += '<td><button class="btn btn-warning" onclick="datosTipo('+tipos[i].IdTipo+')"><i class="icon-pencil"></i></button>'
        datos += "</td>"
        datos += "</tr>"
    }
      
    $("#tipoDatatable").DataTable().destroy().clear();
                 
    $('#tipos').empty();

    $('#tipos').html(datos)

    datatableTipo();

    datos = '<select name="tipoObjeto" class="form-control form-control-lg" id="tipoObjeto" required>'
    datos += '<option selected disabled value="">Tipo de objeto</option>'
    for(i in tipos){
        if (tipos[i].Estado == 1) {
            datos += '<option value="'+tipos[i].Tipo+'">'+tipos[i].Tipo+'</option>'
        }
    }

    $('#tipoObjeto').html(datos)
}

bootstrapValidate('#otros', 'regex:[a-zA-Z ]+:required:Solo puedes utilizar letras');

