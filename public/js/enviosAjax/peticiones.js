/* Modal */
$("#btn-abrir-popup").click(function () {
    overlay.classList.add('active');
    popup.classList.add('active');
});

$("#btn-cerrar-popup").click(function (e) {
    e.preventDefault();
    overlay.classList.remove('active');
    popup.classList.remove('active');
});


/* Textarea */
function countChars(obj) {
    var maxLength = 250;
    var strLength = obj.value.length;

    if (strLength > maxLength) {
        document.getElementById("charNum").innerHTML = '<span style="color: red;">' + strLength + ' de ' + maxLength + ' caracteres</span>';
    } else {
        document.getElementById("charNum").innerHTML = strLength + ' de ' + maxLength + ' caracteres';
    }
}



var autorizado = "";
/* radiobuttons */
$("#si").click(function () {

    $("#autorizado-form").css("display", "block");
    autorizado = "si";
});
$("#no").click(function () {
    $("#autorizado-form").css("display", "none");
    autorizado = "no";
});

/* select */

$("#identificacion").change(function () {
    if ($('#identificacion option:selected').val() == "cedula") {
        $("#cedula-input").css("display", "block");
        $("#pasaporte-input").css("display", "none");
    }
    else {
        $("#pasaporte-input").css("display", "block");
        $("#cedula-input").css("display", "none");
    }
});
$("#identificacionA").change(function () {
    if ($('#identificacionA option:selected').val() == "cedula") {
        $("#cedula-inputA").css("display", "block");
        $("#pasaporte-inputA").css("display", "none");
    }
    else {
        $("#pasaporte-inputA").css("display", "block");
        $("#cedula-inputA").css("display", "none");
    }
});

/* Otros */

$("#tipoObjeto").change(function () {
    if ($('#tipoObjeto option:selected').val() == "otros") {
        $("#otros").css("display", "block");
    } else {
        $("#otros").css("display", "none");
    }
});

/* cargar lugar*/
$("#piso").change(function (e) {
    e.preventDefault();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: 'cargarLugares',
        data: {

            zona: $("#zona").val(),
            piso: $("#piso").val(),
        },
        success: function (response) {


            var lugares = response.lugares;
            datos = '';
            datos += '<option selected disabled value="lugar">Lugar</option>'
            for (i in lugares) {
                var lugares_actual = lugares[i];
                datos += '<option value="' + lugares_actual.IdLugar + '">';
                datos += lugares_actual.NombreLugar;
                datos += '</option>';
            }
            var contenedor_lugares = document.getElementById('lugar');
            contenedor_lugares.innerHTML = datos;
        },
        statusCode: {
            404: function () {
                alert('web not found');
            }
        },





    })
});
$("#zona").change(function (e) {
    e.preventDefault();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: 'cargarLugares',
        data: {

            zona: $("#zona").val(),
            piso: $("#piso").val(),
        },
        success: function (response) {


            var lugares = response.lugares;
            datos = '';
            datos += '<option selected disabled value="lugar">Lugar</option>'
            for (i in lugares) {
                var lugares_actual = lugares[i];
                datos += '<option value="' + lugares_actual.IdLugar + '">';
                datos += lugares_actual.NombreLugar;
                datos += '</option>';
            }
            var contenedor_lugares = document.getElementById('lugar');
            contenedor_lugares.innerHTML = datos;
        },
        statusCode: {
            404: function () {
                alert('web not found');
            }
        },





    })
});

/**
 * Envio de peticion --------------------------------------------------------------------------------------------------------------------------------------------
 */



function enviarPeticion() {
    swal({
        title: "¿Estas segur@?",
        text: "Estas a punto de enviar la petición por favor revisa que los datos estén escritos correctamente ",
        icon: "warning",
        buttons: {
            cancel: 'Cancelar',
            confirm: { text: 'Si, estoy seguro', className: 'sweet-warning' },
        },
    }).then((will) => {
        if (will) {
            $(".onoffswitch-checkbox").prop('checked', false);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: 'insertarPeticion',
                type: 'post',
                data: {
                    nombres: $('#nombres').val(),
                    apellidos: $('#apellidos').val(),
                    identificacion: $('#identificacion').val(),
                    cedula: $('#cedula-input').val(),
                    pasaporte: $('#pasaporte-input').val(),
                    correo: $('#correo').val(),
                    telefono: $('#telefono').val(),
                    nombresA: $('#nombresA').val(),
                    apellidosA: $('#apellidosA').val(),
                    identificacionA: $('#identificacionA').val(),
                    cedulaA: $('#cedula-inputA').val(),
                    pasaporteA: $('#pasaporte-inputA').val(),
                    telefonoA: $('#telefonoA').val(),
                    zona: $("#zona").val(),
                    piso: $("#piso").val(),
                    lugar: $('#lugar').val(),
                    autorizado: autorizado,
                    tipoObjeto: $('#tipoObjeto').val(),
                    tamanoObjeto: $('#tamanoObjeto').val(),
                    colorObjeto: $('#colorObjeto').val(),
                    descripcion: $('#descripcion').val(),
                    nombreZona: $('#zona option:selected').text(),
                    nombreLugar : $('#lugar option:selected').text(),
                    tipoObjetoInput: $('#tipoObjeto-input').val(),
                
                },
                success: function (response) {
                    var idpeticion = response.idpeticion;
                    datos = "";
                    for (i in idpeticion) {
                        var idpeticion_actual = idpeticion[i];
                        datos += idpeticion_actual.idpeticion;

                    }
                    $(location).attr('href', '/?peticion=1&idpeticion=' + datos);
                },
                statusCode: {
                    404: function () {
                        alert('web not found');
                    }
                },





            })
        } else {
            $("#all_petugas").click();
        }
    });

}



$("#btn-abrir-popup-peticion").click(function () {
    var validacion = validarModal3();
    if (!validacion) {
        return;
    }
    overlay.classList.add('active');
    popup.classList.add('active');
    datosCliente = "";
    datosAutorizado = "";
    datosObjeto = "";
    datosLugar = "";
    datosCliente += "<h4>Datos del cliente</h4>";
    datosCliente += "<p><span>Nombres: </span>" + $('#nombres').val() + "</p>";
    datosCliente += "<p><span>Apellidos: </span>" + $('#apellidos').val() + "</p>";
    datosCliente += "<p><span>Tipo de identificación: </span>" + $('#identificacion').val() + "</p>";
    if ($('#identificacion').val() == "cedula") {
        datosCliente += "<p><span>Cédula ciudadana: </span>" + $('#cedula-input').val() + "</p>";
    }
    else {
        datosCliente += "<p><span>Pasaporte: </span>" + $('#pasaporte-input').val() + "</p>";
    }
    datosCliente += "<p><span>Correo: </span>" + $('#correo').val() + "</p>";
    datosCliente += "<p><span>Telefono: </span>" + $('#telefono').val() + "</p>";

    var contenedor_datosCliente = document.getElementById('cliente-peticion');
    contenedor_datosCliente.innerHTML = datosCliente;

    if (autorizado == "si") {
        datosAutorizado += "<h4>Datos del autorizado</h4>";
        datosAutorizado += "<p><span>Nombres: </span>" + $('#nombresA').val() + "</p>";
        datosAutorizado += "<p><span>Apellidos: </span>" + $('#apellidosA').val() + "</p>";
        datosAutorizado += "<p><span>Tipo de identificacion: </span>" + $('#identificacionA').val() + "</p>";
        if ($('#identificacionA').val() == "cedula") {
            datosAutorizado += "<p><span>Cedula ciudadana: </span>" + $('#cedula-inputA').val() + "</p>";
        }
        else {
            datosAutorizado += "<p><span>Pasaporte: </span>" + $('#pasaporte-inputA').val() + "</p>";
        }
        datosAutorizado += "<p><span>Telefono: </span>" + $('#telefonoA').val() + "</p>";

        var contenedor_datosAutorizado = document.getElementById('autorizado-peticion');
        contenedor_datosAutorizado.innerHTML = datosAutorizado;
    }
    else {
        var contenedor_datosAutorizado = document.getElementById('autorizado-peticion');
        contenedor_datosAutorizado.innerHTML = datosAutorizado;
    }

    datosLugar += "<h4>Lugar de perdida</h4>";
    datosLugar += "<p><span>Zona: </span>" + $('#zona option:selected').text() + "</p>";
    datosLugar += "<p><span>Piso: </span>" + $('#piso option:selected').text() + "</p>";
    datosLugar += "<p><span>Lugar: </span>" + $('#lugar option:selected').text() + "</p>";

    var contenedor_datosLugar = document.getElementById('lugar-peticion');
    contenedor_datosLugar.innerHTML = datosLugar;


    datosObjeto += "<h4>Descripción del objeto</h4>";
    if ($('#tipoObjeto').val() == "otros") {
        datosObjeto += "<p><span>Tipo de objeto: </span>" + $('#tipoObjeto-input').val() + "</p>";
    }
    else {
        datosObjeto += "<p><span>Tipo de objeto: </span>" + $('#tipoObjeto').val() + "</p>";
    }
    datosObjeto += "<p><span>Tamaño de objeto: </span>" + $('#tamanoObjeto').val() + "</p>";
    datosObjeto += "<p><span>Color de objeto: </span>" + $('#colorObjeto').val() + "</p>";
    datosObjeto += "<p><span>Descripcion: </span>" + $('#descripcion').val() + "</p>";

    var contenedor_datosObjeto = document.getElementById('objeto-peticion');
    contenedor_datosObjeto.innerHTML = datosObjeto;

})

/**
 * Datos Peticion --------------------------------------------------------------------------------------------------------------------------------------------
 */

$("#datosPeticion").click(function (e) {
    e.preventDefault();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: 'datosPeticion',
        data: {
            idpeticion: $('#idpeticion').val(),
            cedula: $("#cedula").val(),
            pasaporte: $("#pasaporte").val(),
        },
        success: function (response) {


            var datosPeticion = response.datosPeticion;
            datos = '';
            for (i in datosPeticion) {
                var datosPeticion_actual = datosPeticion[i];
                datos += '<tr>';
                datos += '<td>' + datosPeticion_actual.idPeticion + '</td>';
                datos += '<td>' + datosPeticion_actual.FechaPeticion + '</td>';
                if (datosPeticion_actual.EstadoPeticion == "Validado" || datosPeticion_actual.EstadoPeticion == "Pendiente") {
                    datos += '<td><i class="icon-circle" style="color: #ffd54b"></i> Pendiente </td>';
                } else if (datosPeticion_actual.EstadoPeticion == "Denegado") {
                    datos += '<td><i class="icon-circle" style="color: red"></i> ' + datosPeticion_actual.EstadoPeticion + ' </td>';
                } else {
                    datos += '<td><i class="icon-circle" style="color: green"></i> ' + datosPeticion_actual.EstadoPeticion + '</td>';
                }
                datos += '<td><a href="#" onclick="abrirPopup(' + datosPeticion_actual.idPeticion + ')"><i class="icon-eye" style="font-size: 30px"></i></a></td>';
                datos += '</tr>';
            }
            $('#idpeticion').val('');
            $('#cedula').val('');
            $('#pasaporte').val('');

            var contenedor_datosPeticion = document.getElementById('peticiones');
            contenedor_datosPeticion.innerHTML = datos;


        },
        statusCode: {
            404: function () {
                alert('web not found');
            }
        },







    })
});


/**
 * Detalle peticion --------------------------------------------------------------------------------------------------------------------------------------------
 */

function abrirPopup(id) {
    overlay.classList.add('active');
    popup.classList.add('active');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: 'detallePeticion',
        data: {
            idpeticion: id
        },
        success: function (response) {
            console.log(response);

            var datosPeticion = response.idpeticion;

            for (i in datosPeticion) {
                var datosPeticion_actual = datosPeticion[i];
            }

            datosObjeto = "";
            datosObjeto += "<h4>Detalle</h4>";
            if (datosPeticion_actual.DetalleNegacion == null) {
                datosObjeto += "<p> Su petición aun esta siendo revisada </p>";
            }
            else {
                datosObjeto += "<p>" + datosPeticion_actual.DetalleNegacion + "</p>";
            }
            var contenedor_datosObjeto = document.getElementById('objeto-peticion');
            contenedor_datosObjeto.innerHTML = datosObjeto;
        },
        statusCode: {
            404: function () {
                alert('web not found');
            }
        },







    })
};

function cerrarPopup() {
    overlay.classList.remove('active');
    popup.classList.remove('active');
};

// Validaciones



const textPattern = new RegExp(/^[A-Z ]+$/i);
const namePattern = new RegExp("^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$");
const emailPattern = new RegExp("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
const numberPattern = new RegExp("^[0-9]+$");

$("#button1").click(function () {
    function quitarInvalidacion() {
        $("#nombres").removeClass("invalido");
        $("#apellidos").removeClass("invalido");
        $("#identificacion").removeClass("invalido");
        $("#pasaporte-input").removeClass("invalido");
        $("#cedula-input").removeClass("invalido");
        $("#correo").removeClass("invalido");
        $("#telefono").removeClass("invalido");
        $("#nombresA").removeClass("invalido");
        $("#apellidosA").removeClass("invalido");
        $("#identificacionA").removeClass("invalido");
        $("#pasaporte-inputA").removeClass("invalido");
        $("#cedula-inputA").removeClass("invalido");
        $("#correoA").removeClass("invalido");
        $("#telefonoA").removeClass("invalido");

    }
    quitarInvalidacion();
    var validacion = true;
    var errores = "";
    var erroresA = "";

    // Cliente
    if (!$("#nombres").val()) {
        $("#nombres").addClass("invalido");
        errores += '<p class="error">Por favor no deje el campo nombres vació</p>'
        validacion = false;
    }
    else if (!namePattern.test($("#nombres").val())) {
        $("#nombres").addClass("invalido");
        errores += '<p class="error">Por favor ingrese solo letras en el campo nombre</p>'
        validacion = false;
    }

    if (!$("#apellidos").val()) {
        $("#apellidos").addClass("invalido");
        errores += '<p class="error">Por favor no deje el campo apellidos vació</p>'
        validacion = false;
    }
    else if (!namePattern.test($("#apellidos").val())) {
        $("#apellidos").addClass("invalido");
        errores += '<p class="error">Por favor ingrese solo letras en el campo apellidos</p>'
        validacion = false;
    }
    if (!$("#identificacion").val()) {
        $("#identificacion").addClass("invalido");
        errores += '<p class="error">Por favor seleccione un tipo de identificacion</p>'
        validacion = false;
    }
    if ($("#identificacion").val() == "pasaporte") {
        if (!$("#pasaporte-input").val()) {
            $("#pasaporte-input").addClass("invalido");
            errores += '<p class="error">Por favor no deje el campo pasaporte vació</p>'
            validacion = false;
        }
    }
    else if ($("#identificacion").val() == "cedula") {
        if (!$("#cedula-input").val()) {
            $("#cedula-input").addClass("invalido");
            errores += '<p class="error">Por favor no deje el campo cedula vació</p>'
            validacion = false;
        }
        else if (!numberPattern.test($("#cedula-input").val())) {
            $("#cedula-input").addClass("invalido");
            errores += '<p class="error">Por favor ingresa solo numeros en el campo cedula</p>'
            validacion = false;
        }
    }
    if (!$("#correo").val()) {
        $("#correo").addClass("invalido");
        errores += '<p class="error">Por favor no deje el campo correo vació</p>'
        validacion = false;
    }
    else if (!emailPattern.test($("#correo").val())) {
        $("#correo").addClass("invalido");
        errores += '<p class="error">Por favor ingrese un correo valido en minusculas</p>'
        validacion = false;
    }
    if (!$("#telefono").val()) {
        $("#telefono").addClass("invalido");
        errores += '<p class="error">Por favor no deje el campo teléfono vació</p>'
        validacion = false;
    }
    else if (!numberPattern.test($("#telefono").val())) {
        $("#telefono").addClass("invalido");
        errores += '<p class="error">Por favor ingrese solo numeros en el campo teléfono</p>'
        validacion = false;
    }

    // Autorizado
    if ($("input:radio[name=autorizado]:checked").val() == "si") {
        if (!$("#nombresA").val()) {
            $("#nombresA").addClass("invalido");
            erroresA += '<p class="error">Por favor no deje el campo nombres vació</p>'
            validacion = false;
        }
        else if (!textPattern.test($("#nombresA").val())) {
            $("#nombresA").addClass("invalido");
            erroresA += '<p class="error">Por favor ingrese solo letras en el campo nombre</p>'
            validacion = false;
        }

        if (!$("#apellidosA").val()) {
            $("#apellidosA").addClass("invalido");
            erroresA += '<p class="error">Por favor no deje el campo apellidos vació</p>'
            validacion = false;
        }
        else if (!textPattern.test($("#apellidosA").val())) {
            $("#apellidosA").addClass("invalido");
            erroresA += '<p class="error">Por favor ingrese solo letras en el campo apellidos</p>'
            validacion = false;
        }
        if (!$("#identificacionA").val()) {
            $("#identificacionA").addClass("invalido");
            erroresA += '<p class="error">Por favor seleccione un tipo de identificacion</p>'
            validacion = false;
        }
        if ($("#identificacionA").val() == "pasaporte") {
            if (!$("#pasaporte-inputA").val()) {
                $("#pasaporte-inputA").addClass("invalido");
                erroresA += '<p class="error">Por favor no deje el campo pasaporte vació</p>'
                validacion = false;
            }
        }
        else if ($("#identificacionA").val() == "cedula") {
            if (!$("#cedula-inputA").val()) {
                $("#cedula-inputA").addClass("invalido");
                erroresA += '<p class="error">Por favor no deje el campo cedula vació</p>'
                validacion = false;
            }
            else if (!numberPattern.test($("#cedula-inputA").val())) {
                $("#cedula-inputA").addClass("invalido");
                erroresA += '<p class="error">Por favor ingresa solo numeros en el campo cedula</p>'
                validacion = false;
            }
        }
        if (!$("#telefonoA").val()) {
            $("#telefonoA").addClass("invalido");
            erroresA += '<p class="error">Por favor no deje el campo teléfono vació</p>'
            validacion = false;
        }
        else if (!numberPattern.test($("#telefonoA").val())) {
            $("#telefonoA").addClass("invalido");
            erroresA += '<p class="error">Por favor ingrese solo numeros en el campo teléfono</p>'
            validacion = false;
        }

    }


    if (!validacion) {
        $("#errores").html(errores);
        $("#erroresA").html(erroresA);
        return;
    }

    $("#errores").html("");
    $("#erroresA").html("");
    quitarInvalidacion();

    $(".step").css('opacity','0');

    $("#progress2").addClass("active");
    $("#step-1").removeClass("active").addClass("inactive to-left");
    $("#step-2").addClass("active").removeClass("inactive");
    
    $(".step").animate({
        opacity: '1'
    });
})

$("#button2").click(function () {
    function quitarInvalidacion() {
        $("#tipoObjeto").removeClass("invalido");
        $("#tipoObjeto-input").removeClass("invalido");
        $("#tamanoObjeto").removeClass("invalido");
        $("#colorObjeto").removeClass("invalido");
        $("#descripcion").removeClass("invalido");

    }
    quitarInvalidacion();
    var validacion = true;
    var erroresO = "";

    if (!$("#tipoObjeto").val()) {
        $("#tipoObjeto").addClass("invalido");
        erroresO += '<p class="error">Por favor seleccione un tipo de objeto</p>'
        validacion = false;
    }
    if ($("#tipoObjeto").val() == "otros"){
        if (!$("#tipoObjeto-input").val()) {
            $("#tipoObjeto-input").addClass("invalido");
            erroresO += '<p class="error">Por favor no deje el campo nombre objeto vacio</p>'
            validacion = false;
        }
        else if (!textPattern.test($("#tipoObjeto-input").val())) {
            $("#tipoObjeto-input").addClass("invalido");
            erroresO += '<p class="error">Por favor ingrese solo letras en el campo nombre objeto</p>'
            validacion = false;
        }
    }
    if (!$("#tamanoObjeto").val()) {
        $("#tamanoObjeto").addClass("invalido");
        erroresO += '<p class="error">Por favor seleccione un tamaño de objeto</p>'
        validacion = false;
    }
    if (!$("#colorObjeto").val()) {
        $("#colorObjeto").addClass("invalido");
        erroresO += '<p class="error">Por favor seleccione un color de objeto</p>'
        validacion = false;
    }
    if (!$("#descripcion").val()) {
        $("#descripcion").addClass("invalido");
        erroresO += '<p class="error">Por favor no deje el campo descripcion vacio</p>'
        validacion = false;
    }

    if (!validacion) {
        $("#erroresO").html(erroresO);
        return;
    }

    $("#erroresO").html("");
    quitarInvalidacion();

    $(".step").css('opacity','0');
    $("#progress3").addClass("active");
    $("#step-2").removeClass("active").addClass("inactive to-left");
    $("#step-3").addClass("active").removeClass("inactive");
    $(".step").animate({
        opacity: '1'
    });
})

function validarModal3(){
    function quitarInvalidacion() {
        $("#zona").removeClass("invalido");
        $("#piso").removeClass("invalido");
        $("#lugar").removeClass("invalido");
    }
    quitarInvalidacion();
    var validacion = true;
    var erroresL = "";

    if (!$("#zona").val()) {
        $("#zona").addClass("invalido");
        erroresL += '<p class="error">Por favor seleccione una zona</p>'
        validacion = false;
    }
    if (!$("#piso").val()) {
        $("#piso").addClass("invalido");
        erroresL += '<p class="error">Por favor seleccione un piso</p>'
        validacion = false;
    }
    if (!$("#lugar").val()) {
        $("#lugar").addClass("invalido");
        erroresL += '<p class="error">Por favor seleccione un lugar</p>'
        validacion = false;
    }

    if (!validacion) {
        $("#erroresL").html(erroresL);
        return validacion;
    }
    $("#erroresL").html("");
    quitarInvalidacion();
    return validacion;
}


// Click para quitar invalidacion

//Cliente
$("#nombres").click(function () {
    $("#nombres").removeClass("invalido");
})
$("#apellidos").click(function () {
    $("#apellidos").removeClass("invalido");
})
$("#identificacion").click(function () {
    $("#identificacion").removeClass("invalido");
})
$("#pasaporte-input").click(function () {
    $("#pasaporte-input").removeClass("invalido");
})
$("#cedula-input").click(function () {
    $("#cedula-input").removeClass("invalido");
})
$("#correo").click(function () {
    $("#correo").removeClass("invalido");
})
$("#telefono").click(function () {
    $("#telefono").removeClass("invalido");
})

//Autorizado

$("#nombresA").click(function () {
    $("#nombresA").removeClass("invalido");
})
$("#apellidosA").click(function () {
    $("#apellidosA").removeClass("invalido");
})
$("#identificacionA").click(function () {
    $("#identificacionA").removeClass("invalido");
})
$("#pasaporte-inputA").click(function () {
    $("#pasaporte-inputA").removeClass("invalido");
})
$("#cedula-inputA").click(function () {
    $("#cedula-inputA").removeClass("invalido");
})
$("#correoA").click(function () {
    $("#correoA").removeClass("invalido");
})
$("#telefonoA").click(function () {
    $("#telefonoA").removeClass("invalido");
})

//Objeto

$("#tipoObjeto").click(function () {
    $("#tipoObjeto").removeClass("invalido");
})
$("#tipoObjeto-input").click(function () {
    $("#tipoObjeto-input").removeClass("invalido");
})
$("#tamanoObjeto").click(function () {
    $("#tamanoObjeto").removeClass("invalido");
})
$("#colorObjeto").click(function () {
    $("#colorObjeto").removeClass("invalido");
})
$("#descripcion").click(function () {
    $("#descripcion").removeClass("invalido");
})

//Lugar

$("#zona").click(function () {
    $("#zona").removeClass("invalido");
})
$("#piso").click(function () {
    $("#piso").removeClass("invalido");
})
$("#lugar").click(function () {
    $("#lugar").removeClass("invalido");
})
