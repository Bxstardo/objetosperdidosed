function consultarDatosPeticion(id){
  
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
        }
    }); 
    $.ajax({
        url : 'datosPeticionPorId',
        data : {
            idpeticion : id
        },  
        success: function (response) {
	
            

			var datosPeticion =   response.idpeticion;
			var objeto = response.objeto;
				
			for (i in datosPeticion) {
                var datosPeticion_actual = datosPeticion[i];
			}
			$('#idPeticionObjeto').val("");
			$('#idObjeto').val("");
			datosCliente = "";
			datosAutorizado = "";
			datosObjeto = "";
			datosObjetoPerdido = "";
			botones = "";
			datosCliente += "<h4>Datos del cliente</h4>";
			datosCliente += "<p><span>Nombres: </span>"+datosPeticion_actual.Nombres+"</p>";
			datosCliente += "<p><span>Apellidos: </span>"+datosPeticion_actual.Apellidos+"</p>";
			if(datosPeticion_actual.Identificacion != null){
				datosCliente += "<p><span>Tipo de identificación: </span>Cedula ciudadana</p>";
				datosCliente += "<p><span>Cédula ciudadana: </span>"+datosPeticion_actual.Identificacion+"</p>";
			}
			else{
				datosCliente += "<p><span>Tipo de identificación: </span>Pasaporte</p>";
				datosCliente += "<p><span>Pasaporte: </span>"+datosPeticion_actual.Pasaporte+"</p>";
			}
			datosCliente += "<p><span>Correo: </span>"+datosPeticion_actual.Correo+"</p>";
			datosCliente += "<p><span>Telefono: </span>"+datosPeticion_actual.Telefono+"</p>";
		
			var contenedor_datosCliente =   document.getElementById('cliente-peticion');
			contenedor_datosCliente.innerHTML = datosCliente;
		
			if(datosPeticion_actual.nombresautorizado != null){
                datosAutorizado += "<hr class='hrA'>";
				datosAutorizado += "<h4>Datos del autorizado</h4>";
				datosAutorizado += "<p><span>Nombres: </span>"+datosPeticion_actual.nombresautorizado+"</p>";
				datosAutorizado += "<p><span>Apellidos: </span>"+datosPeticion_actual.apellidosautorizado+"</p>";
				if(datosPeticion_actual.identificacionautorizado != null){
					datosAutorizado += "<p><span>Tipo de identificación: </span>Cedula ciudadana</p>";
					datosAutorizado += "<p><span>Cédula ciudadana: </span>"+datosPeticion_actual.identificacionautorizado+"</p>";
				}
				else{
					datosCliente += "<p><span>Tipo de identificación: </span>Pasaporte</p>";
					datosAutorizado += "<p><span>Pasaporte: </span>"+datosPeticion_actual.pasaporteautorizado+"</p>";
				}
				datosAutorizado += "<p><span>Telefono: </span>"+datosPeticion_actual.telefonoautorizado+"</p>";
			
				var contenedor_datosAutorizado =   document.getElementById('autorizado-peticion');
				contenedor_datosAutorizado.innerHTML = datosAutorizado;
			}
			else{
				var contenedor_datosAutorizado =   document.getElementById('autorizado-peticion');
				contenedor_datosAutorizado.innerHTML = datosAutorizado;
			}
            datosObjeto += "<hr style='margin-top:0'>";
			datosObjeto += "<h4>Descripción del objeto solicitado</h4>";
			datosObjeto += '<div class="table-responsive">';
			datosObjeto += "<table class='table table-striped table-bordered dt-responsive nowrap' style='width:100%' id='objetosDatatable2'>";
			datosObjeto += '<thead style="font-size: 10px">';
			datosObjeto += "<tr>"
			datosObjeto += "<th>#</th>"
			datosObjeto += "<th>Tipo objeto</th>"
			datosObjeto += "<th>Tamaño objeto</th>"
			datosObjeto += "<th>Color objeto</th>"
			datosObjeto += "<th>Lugar</th>"
			datosObjeto += "<th>Descripción</th>"
			datosObjeto += "</tr>"
			datosObjeto += "</thead>"
			datosObjeto += "<tbody>"
			datosObjeto += "<tr>"
			datosObjeto += "<td>"+datosPeticion_actual.idObjeto+"</td>"
			datosObjeto += "<td>"+datosPeticion_actual.Tipo+"</td>"
			datosObjeto += "<td>"+datosPeticion_actual.Tamano+"</td>"
			datosObjeto += "<td>"+datosPeticion_actual.Color+"</td>"
			datosObjeto += "<td>"+datosPeticion_actual.NombreLugar+" - Piso "+datosPeticion_actual.Piso+" - Zona "+datosPeticion_actual.Zona+"</td>"
			datosObjeto += "<td>"+datosPeticion_actual.Descripcion+"</td>"
			datosObjeto += "</tr>"
			datosObjeto += "</tbody>"
			datosObjeto += "</table>"
			datosObjeto += "</div>"

	
		
			var contenedor_datosObjeto =   document.getElementById('objetoSolicitado-peticion');
			contenedor_datosObjeto.innerHTML = datosObjeto;

            if (datosPeticion_actual.EstadoPeticion == "Validado") {
                datosObjetoPerdido += "<hr>";
                datosObjetoPerdido += '<h4 class="text-center text-warning">Objeto coincidido</h4>';
                datosObjetoPerdido += '<div class="table-responsive">';
                datosObjetoPerdido += '<table class="table table-hover table-striped table-bordered dt-responsive nowrap" style="width:100%" id="objetosDatatable">';
                datosObjetoPerdido += '<thead>';
                datosObjetoPerdido += '<tr>';
                datosObjetoPerdido += '<th>No Objeto</th>';
                datosObjetoPerdido += '<th>Tipo objeto</th>';
                datosObjetoPerdido += '<th>Tamaño objeto</th>';
                datosObjetoPerdido += '<th>Color objeto</th>';
                datosObjetoPerdido += '<th>Lugar</th>';
                datosObjetoPerdido += '<th>Descripción</th>';
                datosObjetoPerdido += '</tr>';
                datosObjetoPerdido += '</thead>';
			datosObjetoPerdido += '<tbody>';
			for (i in objeto) {
				var objeto_actual = objeto[i];
				datosObjetoPerdido += '<tr id="celda'+i+'">';
				datosObjetoPerdido += "<td>"+objeto_actual.IdObjeto+"</td>"
				datosObjetoPerdido += "<td>"+objeto_actual.Tipo+"</td>"
				datosObjetoPerdido += "<td>"+objeto_actual.Tamano+"</td>"
				datosObjetoPerdido += "<td>"+objeto_actual.Color+"</td>"
				datosObjetoPerdido += "<td>"+objeto_actual.NombreLugar+" - Piso "+datosPeticion_actual.Piso+" - Zona "+datosPeticion_actual.Zona+"</td>"
				datosObjetoPerdido += "<td>"+objeto_actual.Descripcion+"</td>"
				datosObjetoPerdido += '</tr>';
			
            }
			datosObjetoPerdido += '</tbody>';
            datosObjetoPerdido += '</table>';	
            datosObjetoPerdido += "</div>"
            }else if(datosPeticion_actual.EstadoPeticion == "Denegado"){
                datosObjetoPerdido += "<hr>";
                datosObjetoPerdido += '<h4 style="font-size:20px; font-weight:600;">Detalle negación</h4>';
                datosObjetoPerdido += '<p>'+datosPeticion_actual.DetalleNegacion+'<p>';
            }
            


			$('#idPeticionSolicitud').val(datosPeticion_actual.idPeticion);
			

			$("#objetosDatatable").DataTable().destroy().clear();
                         
			$('#objeto-peticion').empty();

			var contenedor_datosObjetoPerdido =   document.getElementById('objeto-peticion');
			contenedor_datosObjetoPerdido.innerHTML = datosObjetoPerdido;

			
			botones += '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>';
	
			
			var contenedor_botones =   document.getElementById('botones-peticion');
			contenedor_botones.innerHTML = botones;

	  
            $('#peticion-modal-lg').modal().show()
            
			$("#objetosDatatable").DataTable({
              
				"lengthMenu":	[[5, 10, 20, 25, 50, -1], [5, 10, 20, 25, 50, "Todos"]],
				"language": {
					"sProcessing": "Procesando...",
					"sLengthMenu": "Mostrar _MENU_ objetos",
					"sZeroRecords": "No se encontraron objetos similares",
					"sEmptyTable": "No se encontraron objetos similares",
					"sInfo": "Mostrando objetos del _START_ al _END_ de un total de _TOTAL_ objetos",
					"sInfoEmpty": "Mostrando objetos del 0 al 0 de un total de 0 objetos",
					"sInfoFiltered": "(filtrado de un total de _MAX_ objetos)",
					"sInfoPostFix": "",
					"sSearch": "Buscar:",
					"sUrl": "",
					"sInfoThousands": ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst": "Primero",
						"sLast": "Último",
						"sNext": "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				   
                },
   
               
                
		   
			});
            $("#objetosDatatable2").DataTable({
             
				"lengthMenu":	[[5, 10, 20, 25, 50, -1], [5, 10, 20, 25, 50, "Todos"]],
				"language": {
					"sProcessing": "Procesando...",
					"sLengthMenu": "Mostrar _MENU_ objetos",
					"sZeroRecords": "No se encontraron objetos similares",
					"sEmptyTable": "No se encontraron objetos similares",
					"sInfo": "Mostrando objetos del _START_ al _END_ de un total de _TOTAL_ objetos",
					"sInfoEmpty": "Mostrando objetos del 0 al 0 de un total de 0 objetos",
					"sInfoFiltered": "(filtrado de un total de _MAX_ objetos)",
					"sInfoPostFix": "",
					"sSearch": "Buscar:",
					"sUrl": "",
					"sInfoThousands": ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst": "Primero",
						"sLast": "Último",
						"sNext": "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				   
                },
      
              
		   
            });
            
        },
        statusCode: {
            404: function() {
                alert('web not found');
            }
        },
    })
}

$(".peticionesC").click(function (e) {	 
    e.preventDefault();


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
        }
    }); 
    $.ajax({
        url : 'peticiones',
        data : {
            idpeticion : $('#idpeticion').val(),
            identificacion : $("#identificacion").val(),
            pasaporte : $("#pasaporte").val(),
        },  
        success: function (response) {

            var datosPeticion =   response.peticiones;
            datos = '';
            for (i in datosPeticion) {
                var datosPeticion_actual = datosPeticion[i];
                    datos += '<tr>';
                    datos += '<td>'+datosPeticion_actual.idPeticion+'</td>';
                    datos += '<td><i class="icon-circle" style="color:' ;
                    if(datosPeticion_actual.EstadoPeticion == 'Denegado'){
                        datos +=    'red';
                    }
                    else if(datosPeticion_actual.EstadoPeticion == 'Autorizado'){
                        datos +=    'green';
                    }
                    else if(datosPeticion_actual.EstadoPeticion == 'Pendiente'){
                        datos +=  'lightslategrey';
                    }
                    else{
                        datos +=   '#ffd54b';
                    }
                    datos += '"></i>';
                    datos += datosPeticion_actual.EstadoPeticion+'</td>';
                    datos += '<td>'+datosPeticion_actual.FechaPeticion+'</td>';
                    datos += '<td><a href="#" class="btn btn-warning"><i';
                    datos += ' class="icon-eye"';
                    datos += ' onclick="consultarDatosPeticion('+datosPeticion_actual.idPeticion+')"></i></a>';
                    datos += '</tr>';
            }
            $('#idpeticion').val('');
            $('#identificacion').val('');
            $('#pasaporte').val('');

            $("#peticionesDatatable").DataTable().destroy().clear();
                         
            $('#peticiones').empty();
      
            $('#peticiones').html(datos);

            datatablePeticion();

            
           
        },
        statusCode: {
            404: function() {
                alert('web not found');
            }
            },

            
            


    

    })
});



$("#peticionesCTodos").click(function (e) {	 
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
        }
    }); 
    $.ajax({
        url : 'peticionesTodos',
        success: function (response) {

            var datosPeticion =   response.peticiones;
            datos = '';
            for (i in datosPeticion) {
                var datosPeticion_actual = datosPeticion[i];
                datos += '<tr>';
                datos += '<td>'+datosPeticion_actual.idPeticion+'</td>';
                datos += '<td><i class="icon-circle" style="color:' ;
                if(datosPeticion_actual.EstadoPeticion == 'Denegado'){
                    datos +=    'red';
                }
                else if(datosPeticion_actual.EstadoPeticion == 'Autorizado'){
                    datos +=    'green';
                }
                else if(datosPeticion_actual.EstadoPeticion == 'Pendiente'){
                    datos +=  'lightslategrey';
                    
                }
                else{
                    datos +=   '#ffd54b';
                }
                datos += '"></i>';
                datos += datosPeticion_actual.EstadoPeticion+'</td>';
                datos += '<td>'+datosPeticion_actual.FechaPeticion+'</td>';
                datos += '<td><a href="#" class="btn btn-warning"><i';
                datos += ' class="icon-eye"';
                datos += ' onclick="consultarDatosPeticion('+datosPeticion_actual.idPeticion+')"></i></a>';
                datos += '</tr>';
            }
            $('#idpeticion').val('');
            $('#cedula').val('');
            $('#pasaporte').val('');

            $("#peticionesDatatable").DataTable().destroy().clear();
                         
            $('#peticiones').empty();
      
            $('#peticiones').html(datos);

            datatablePeticion();

            
           
        },
        statusCode: {
            404: function() {
                alert('web not found');
            }
            },

            
            


    

    })
});


function datatablePeticion2(){
    $("#peticionesDatatable").dataTable({
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ peticiones",
            "sZeroRecords": "No se encontraron peticiones pendientes por validar",
            "sEmptyTable": "No se encontraron peticiones pendientes por validar",
            "sInfo": "Mostrando peticiones del _START_ al _END_ de un total de _TOTAL_ peticiones",
            "sInfoEmpty": "Mostrando peticiones del 0 al 0 de un total de 0 peticiones",
            "sInfoFiltered": "(filtrado de un total de _MAX_ peticiones)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        lengthMenu:	[[5, 10, 20, 25, 50, -1], [5, 10, 20, 25, 50, "Todos"]],
        bFilter: false,
        bInfo: false,
        bLengthChange : false,
        responsive: true

    });
    $("#peticionesDatatable3").dataTable({
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ peticiones",
            "sZeroRecords": "No se encontraron peticiones pendientes por validar",
            "sEmptyTable": "No se encontraron peticiones pendientes por validar",
            "sInfo": "Mostrando peticiones del _START_ al _END_ de un total de _TOTAL_ peticiones",
            "sInfoEmpty": "Mostrando peticiones del 0 al 0 de un total de 0 peticiones",
            "sInfoFiltered": "(filtrado de un total de _MAX_ peticiones)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        lengthMenu:	[[5, 10, 20, 25, 50, -1], [5, 10, 20, 25, 50, "Todos"]],
        bFilter: false,
        bInfo: false,
        bLengthChange : false,
        responsive: true

    });
}