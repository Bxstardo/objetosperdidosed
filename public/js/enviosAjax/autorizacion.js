
/* Textarea */
function countChars(obj){
    var maxLength = 250;
    var strLength = obj.value.length;
    
    if(strLength > maxLength){
        document.getElementById("charNum").innerHTML = '<span style="color: red;">'+strLength+' de '+maxLength+' caracteres</span>';
    }else{
        document.getElementById("charNum").innerHTML = strLength+' de '+maxLength+' caracteres';
    }
}


// Validar Peticion  --------------------------------------------------------------------------------------------------------------------------------------------


function abrirPopupDatosPeticion(id){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
        }
    }); 
    $.ajax({
        url : 'datosAutorizadoPorId',
        data : {
            idpeticion : id
        },  
        success: function (response) {
	
            

			var datosPeticion =   response.idpeticion;
			var objeto = response.objeto;
				
			for (i in datosPeticion) {
                var datosPeticion_actual = datosPeticion[i];
			}
			$('#idPeticionObjeto').val("");
			$('#idObjeto').val("");
			datosCliente = "";
			datosAutorizado = "";
			datosObjeto = "";
			datosObjetoPerdido = "";
			botones = "";
			datosCliente += "<h4>Datos del cliente</h4>";
			datosCliente += "<p><span>Nombres: </span>"+datosPeticion_actual.Nombres+"</p>";
			datosCliente += "<p><span>Apellidos: </span>"+datosPeticion_actual.Apellidos+"</p>";
			if(datosPeticion_actual.Identificacion != null){
				datosCliente += "<p><span>Tipo de identificacion: </span>Cedula ciudadana</p>";
				datosCliente += "<p><span>Cedula ciudadana: </span>"+datosPeticion_actual.Identificacion+"</p>";
			}
			else{
				datosCliente += "<p><span>Tipo de identificacion: </span>Pasaporte</p>";
				datosCliente += "<p><span>Pasaporte: </span>"+datosPeticion_actual.Pasaporte+"</p>";
			}
			datosCliente += "<p><span>Correo: </span>"+datosPeticion_actual.Correo+"</p>";
			datosCliente += "<p><span>Telefono: </span>"+datosPeticion_actual.Telefono+"</p>";
		
			var contenedor_datosCliente =   document.getElementById('cliente-peticion');
			contenedor_datosCliente.innerHTML = datosCliente;
		
			if(datosPeticion_actual.nombresautorizado != null){
                datosAutorizado += "<hr class='hrA'>";
				datosAutorizado += "<h4>Datos del autorizado</h4>";
				datosAutorizado += "<p><span>Nombres: </span>"+datosPeticion_actual.nombresautorizado+"</p>";
				datosAutorizado += "<p><span>Apellidos: </span>"+datosPeticion_actual.apellidosautorizado+"</p>";
				if(datosPeticion_actual.identificacionautorizado != null){
					datosAutorizado += "<p><span>Tipo de identificacion: </span>Cedula ciudadana</p>";
					datosAutorizado += "<p><span>Cedula ciudadana: </span>"+datosPeticion_actual.identificacionautorizado+"</p>";
				}
				else{
					datosCliente += "<p><span>Tipo de identificacion: </span>Pasaporte</p>";
					datosAutorizado += "<p><span>Pasaporte: </span>"+datosPeticion_actual.pasaporteautorizado+"</p>";
				}
				datosAutorizado += "<p><span>Telefono: </span>"+datosPeticion_actual.telefonoautorizado+"</p>";
			
				var contenedor_datosAutorizado =   document.getElementById('autorizado-peticion');
				contenedor_datosAutorizado.innerHTML = datosAutorizado;
			}
			else{
				var contenedor_datosAutorizado =   document.getElementById('autorizado-peticion');
				contenedor_datosAutorizado.innerHTML = datosAutorizado;
			}
						
            datosObjeto += "<hr style='margin-top:0'>";
            datosObjeto += "<h4>Descripcion del objeto solicitado</h4>";
            datosObjeto += '<div class="table-responsive">';
			datosObjeto += "<table class='table table-striped table-bordered display nowrap' cellspacing='0' width='100%' id='solicitadoDatatable' >";
			datosObjeto += '<thead style="font-size: 10px">';
			datosObjeto += "<tr>"
			datosObjeto += "<th>Tipo objeto</th>"
			datosObjeto += "<th>Tamaño objeto</th>"
			datosObjeto += "<th>Color objeto</th>"
			datosObjeto += "<th>Lugar</th>"
			datosObjeto += "<th>Descripcion</th>"
			datosObjeto += "</tr>"
			datosObjeto += "</thead>"
			datosObjeto += "<tbody>"
			datosObjeto += "<tr>"
			datosObjeto += "<td>"+datosPeticion_actual.Tipo+"</td>"
			datosObjeto += "<td>"+datosPeticion_actual.Tamano+"</td>"
			datosObjeto += "<td>"+datosPeticion_actual.Color+"</td>"
			datosObjeto += "<td>"+datosPeticion_actual.NombreLugar+" - Piso "+datosPeticion_actual.Piso+" - Zona "+datosPeticion_actual.Zona+"</td>"
			datosObjeto += "<td>"+datosPeticion_actual.Descripcion+"</td>"
			datosObjeto += "</tr>"
			datosObjeto += "</tbody>"
			datosObjeto += "</table>"
			datosObjeto += "</div>"

	
		
			var contenedor_datosObjeto =   document.getElementById('objetoSolicitado-peticion');
			contenedor_datosObjeto.innerHTML = datosObjeto;

            datosObjetoPerdido += "<hr>";
			datosObjetoPerdido += '<h4 class="text-center text-warning">Objeto validado</h4>';
            datosObjetoPerdido += '<p class="text-center">Determine si el objeto solicitado coincide correctamente con el objeto validado</p>';
            datosObjetoPerdido += '<div class="table-responsive">';
			datosObjetoPerdido += '<table class="table table-hover table-striped table-bordered display nowrap" cellspacing="0" width="100%" id="objetosDatatable">';
			datosObjetoPerdido += '<thead style="font-size: 10px">';
			datosObjetoPerdido += '<tr>';
			datosObjetoPerdido += '<th>No Objeto</th>';
			datosObjetoPerdido += '<th>Tipo objeto</th>';
			datosObjetoPerdido += '<th>Tamaño objeto</th>';
			datosObjetoPerdido += '<th>Color objeto</th>';
			datosObjetoPerdido += '<th>Lugar</th>';
			datosObjetoPerdido += '<th>Descripcion</th>';
			datosObjetoPerdido += '</tr>';
			datosObjetoPerdido += '</thead>';
			datosObjetoPerdido += '<tbody>';
			for (i in objeto) {
				var objeto_actual = objeto[i];
				datosObjetoPerdido += '<tr class="bg-warning" id="celda'+i+'">';
				datosObjetoPerdido += "<td>"+objeto_actual.IdObjeto+"</td>"
				datosObjetoPerdido += "<td>"+objeto_actual.Tipo+"</td>"
				datosObjetoPerdido += "<td>"+objeto_actual.Tamano+"</td>"
				datosObjetoPerdido += "<td>"+objeto_actual.Color+"</td>"
				datosObjetoPerdido += "<td>"+objeto_actual.NombreLugar+" - Piso "+datosPeticion_actual.Piso+" - Zona "+datosPeticion_actual.Zona+"</td>"
				datosObjetoPerdido += "<td>"+objeto_actual.Descripcion+"</td>"
				datosObjetoPerdido += '</tr>';
			    $('#idPeticionObjeto').val(objeto_actual.IdPeticion);
                $('#idObjeto').val(objeto_actual.IdObjeto);
            }
			datosObjetoPerdido += '</tbody>';
            datosObjetoPerdido += '</table>';	
            datosObjetoPerdido += "</div>";
          

         

			$('#idPeticionSolicitud').val(datosPeticion_actual.idPeticion);
			

			$("#objetosDatatable").DataTable().destroy().clear();
                         
			$('#objeto-peticion').empty();

			var contenedor_datosObjetoPerdido =   document.getElementById('objeto-peticion');
			contenedor_datosObjetoPerdido.innerHTML = datosObjetoPerdido;

			
			botones += '<button type="button" onclick="cerrarModalPeticion()" class="btn btn-danger">Denegar peticion</button>';
			botones += '<button type="button" onclick="autorizarPeticion(8)" class="btn btn-warning" id="btn-validar">Autorizar peticion</button>';
			
			var contenedor_botones =   document.getElementById('botones-peticion');
			contenedor_botones.innerHTML = botones;

              
            $('#peticion-modal-lg').modal().show()

            $("#solicitadoDatatable").dataTable({
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ peticiones",
                    "sZeroRecords": "No se encontraron peticiones pendientes por autorizar",
                    "sEmptyTable": "No se encontraron peticiones pendientes por autorizar",
                    "sInfo": "Mostrando peticiones del _START_ al _END_ de un total de _TOTAL_ peticiones",
                    "sInfoEmpty": "Mostrando peticiones del 0 al 0 de un total de 0 peticiones",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ peticiones)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                responsive : true
        
            });

            $("#objetosDatatable").dataTable({
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ peticiones",
                    "sZeroRecords": "No se encontraron peticiones pendientes por autorizar",
                    "sEmptyTable": "No se encontraron peticiones pendientes por autorizar",
                    "sInfo": "Mostrando peticiones del _START_ al _END_ de un total de _TOTAL_ peticiones",
                    "sInfoEmpty": "Mostrando peticiones del 0 al 0 de un total de 0 peticiones",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ peticiones)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                responsive : true
        
            });
			
        },
        statusCode: {
            404: function() {
                alert('web not found');
            }
        },
    })
}





$("#peticionesValidadasTodos").click(function (e) {	 
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
        }
    }); 
    $.ajax({
        url : 'peticionesValidadasTodos',
        success: function (response) {

            var datosPeticion =   response.peticiones;
            datos = '';
            for (i in datosPeticion) {
                var datosPeticion_actual = datosPeticion[i];
                    datos += '<tr>';
                    datos += '<td>'+datosPeticion_actual.objeto+'</td>';
                    datos += '<td>'+datosPeticion_actual.Tamano+'</td>';
                    datos += '<td>'+datosPeticion_actual.color+'</td>';
                    datos += '<td>'+datosPeticion_actual.idpeticion+'</td>';
                    datos += '<td>'+datosPeticion_actual.fechapeticion+'</td>';
                    datos += '<td><a href="#" class="btn btn-warning"><i';
                    datos += ' class="icon-pencil"';
                    datos += ' onclick="abrirPopupDatosPeticion('+datosPeticion_actual.idpeticion+')"></i></a>';
                    datos += '</tr>';
            }
            $('#idpeticion').val('');
            $('#cedula').val('');
            $('#pasaporte').val('');

            $("#peticionesDatatable").DataTable().destroy().clear();
                         
            $('#peticiones').empty();
      
            $('#peticiones').html(datos);

            datatablePeticion();

            
           
        },
        statusCode: {
            404: function() {
                alert('web not found');
            }
            },

            
            


    

    })
});

$(".peticionesValidadas").click(function (e) {	 
    e.preventDefault();


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
        }
    }); 
    $.ajax({
        url : 'peticionesValidadas',
        data : {
            idpeticion : $('#idpeticion').val(),
            identificacion : $("#identificacion").val(),
            pasaporte : $("#pasaporte").val(),
        },  
        success: function (response) {

            var datosPeticion =   response.peticiones;
            datos = '';
            for (i in datosPeticion) {
                var datosPeticion_actual = datosPeticion[i];
                    datos += '<tr>';
                    datos += '<td>'+datosPeticion_actual.objeto+'</td>';
                    datos += '<td>'+datosPeticion_actual.Tamano+'</td>';
                    datos += '<td>'+datosPeticion_actual.color+'</td>';
                    datos += '<td>'+datosPeticion_actual.idpeticion+'</td>';
                    datos += '<td>'+datosPeticion_actual.fechapeticion+'</td>';
                    datos += '<td><a href="#" class="btn btn-warning"><i';
                    datos += ' class="icon-pencil"';
                    datos += ' onclick="abrirPopupDatosPeticion('+datosPeticion_actual.idpeticion+')"></i></a>';
                    datos += '</tr>';
            }
            $('#idpeticion').val('');
            $('#identificacion').val('');
            $('#pasaporte').val('');

            $("#peticionesDatatable").DataTable().destroy().clear();
                         
            $('#peticiones').empty();
      
            $('#peticiones').html(datos);

            datatablePeticion();

            
           
        },
        statusCode: {
            404: function() {
                alert('web not found');
            }
            },

            
            


    

    })
});



function volverPeticion(){

    $('textarea[id="detalle"]').val("");
    $("#detalle-modal").modal("toggle");
    $("#peticion-modal-lg").modal("show");
    $("#peticion-modal-lg").css("overflow-x","hidden");
    $("#peticion-modal-lg").css("overflow-y","auto");
}

function cerrarModalPeticion(){
    $('textarea[id="detalle"]').val("");
    $("#peticion-modal-lg").modal("toggle");
    $("#detalle-modal").modal("show");
}


function datatablePeticion(){
    $("#peticionesDatatable").dataTable({
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ peticiones",
            "sZeroRecords": "No se encontraron peticiones pendientes por autorizar",
            "sEmptyTable": "No se encontraron peticiones pendientes por autorizar",
            "sInfo": "Mostrando peticiones del _START_ al _END_ de un total de _TOTAL_ peticiones",
            "sInfoEmpty": "Mostrando peticiones del 0 al 0 de un total de 0 peticiones",
            "sInfoFiltered": "(filtrado de un total de _MAX_ peticiones)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive : true

    });
}
function datatableObjetos(id){
    $(id).dataTable({
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ peticiones",
            "sZeroRecords": "No se encontraron peticiones pendientes por autorizar",
            "sEmptyTable": "No se encontraron peticiones pendientes por autorizar",
            "sInfo": "Mostrando peticiones del _START_ al _END_ de un total de _TOTAL_ peticiones",
            "sInfoEmpty": "Mostrando peticiones del 0 al 0 de un total de 0 peticiones",
            "sInfoFiltered": "(filtrado de un total de _MAX_ peticiones)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive : true

    });
}

function autorizarPeticion(estado){
    
    if(estado == 8){
        detalle = "¡Su peticion ha sido autorizado con exito!";
        $('textarea[id="detalle"]').val(detalle);
        estadoTexto = "autorizar";
    }
    else{ 
        estadoTexto = "denegar";
        if($('textarea[id="detalle"]').val() == ""){
            swal("Por favor describe la razon del porque denegaras la peticion");
            return;
        }
    }      
    swal({
        title: "¿Estas segur@?",
        text: "Estas apunto de "+estadoTexto+" la peticion",
        icon: "warning",
        buttons: {
            cancel : 'Cancelar',
            confirm : {text:'Si, estoy seguro',className:'sweet-warning'},
        },
    }).then((will)=>{
        if(will){
           

            $(".onoffswitch-checkbox").prop('checked',false);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
                }
            }); 
            $.ajax({
                url : 'autorizarPeticion',
                data : {
                    idPeticionSolicitud : $("#idPeticionSolicitud").val(),
                    idPeticionObjeto : $('#idPeticionObjeto').val(),
                    estado : estado,
                    detalle :  $('textarea[id="detalle"]').val(),
                    identificacionDirector : $("#identificacionDirector").val(),
                },  
                success: function (response) {
                    
                    var datosPeticion = response.peticiones;
                    datos = "";
                    for (i in datosPeticion) {
                        var datosPeticion_actual = datosPeticion[i];
                        datos += '<tr>';
                        datos += '<td>'+datosPeticion_actual.objeto+'</td>';
                        datos += '<td>'+datosPeticion_actual.Tamano+'</td>';
                        datos += '<td>'+datosPeticion_actual.color+'</td>';
                        datos += '<td>'+datosPeticion_actual.idpeticion+'</td>';
                        datos += '<td>'+datosPeticion_actual.fechapeticion+'</td>';
                        datos += '<td><a href="#" class="btn btn-warning" onclick="abrirPopupDatosPeticion('+datosPeticion_actual.idpeticion+')"><i';
                        datos += ' class="icon-pencil"';
                        datos += ' ></i></a>';
                        datos += '</tr>';
                    }

                
                    $("#peticionesDatatable").DataTable().destroy().clear();
                         
                    $('#peticiones').empty();
              
                    $('#peticiones').html(datos);

                    datatablePeticion();
                  
                    $('#peticion-modal-lg').modal('hide');
                    $('#detalle-modal').modal('hide');
                    if(estado == 8){
                        swal({
                            title: "¡La peticion se ha autorizado correctamente!",
                            text: "Se ha autorizado la peticion No. "+$("#idPeticionSolicitud").val()+" con el objeto No. "+$("#idObjeto").val(),
                            icon: "success",
                            button: "Cerrar",
                          });
                    }
                    else{
                        swal({
                            title: "¡La peticion se ha denegado correctamente!",
                            text: "Se ha denegado la peticion No. "+$("#idPeticionSolicitud").val(),
                            icon: "success",
                            button: "Cerrar",
                          });
                    }      
                },
                statusCode: {
                    404: function() {
                        alert('web not found');
                    }
                },
        
            })
            
        }else{
            $("#all_petugas").click();
        }
    })
   

}

