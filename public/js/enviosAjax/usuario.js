const emailPattern = new RegExp("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
const numberPattern = new RegExp("^[0-9]+$");

$('#btnDatos').click(function () {
    if ($('#btnDatos').val() == "guardar") {
        var validacion = true;
        if($('#Correo').val()==""){
            $('#validate-correo-r').css("display","block");
            $('#Correo').css("border-color","#dc3545");
            validacion = false;
        }else if(!emailPattern.test($('#Correo').val())){
            $('#validate-correo-c').css("display","block");
            $('#Correo').css("border-color","#dc3545");
            validacion = false;
        }else{
            $('#Correo').css("border-color","#28a745");
        }
        if($('#Telefono').val()==""){
            $('#validate-telefono-r').css("display","block");
            $('#Telefono').css("border-color","#dc3545");
            validacion = false;
        }else if(!numberPattern.test($('#Telefono').val())){
            $('#validate-telefono-n').css("display","block");
            $('#Telefono').css("border-color","#dc3545");
            validacion = false;
        }else{
            $('#Telefono').css("border-color","#28a745");
        }
        if (!validacion) {
            return;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'datosPersonales',
            data: {
                IdentificacionUsuario : $('#IdentificacionUsuario').val(),
                Correo : $('#Correo').val(),
                Telefono : $('#Telefono').val(),
            },
            success: function (response) {
                $('#perfil-modal').modal("toggle");
                $('#btnDatos').val("cambiar");
                $('#btnDatos').html("Actualizar datos");
                $('#Correo').prop('readonly', true);
                $('#Telefono').prop('readonly', true);
                swal("Se ha actualizado su perfil correctamente!", "", "success");
                $('#Telefono').css("border-color","#ced4da");
                $('#Correo').css("border-color","#ced4da");
            },
            statusCode: {
                404: function () {
                    alert('web not found');
                }
            },
            error: function() {
                $('#Correo').css("border-color","#ced4da");
                swal("Lo sentimos, no se ha actualizado su perfil debido a que este correo ya existe en otro usuario");
            }
        })
    }else{
        $('#Correo').prop('readonly', false);
        $('#Telefono').prop('readonly', false);
        $('#btnDatos').val("guardar");
        $('#btnDatos').html("Guardar Cambios");
    }
})


$('#Correo').click(function () {
    $('#validate-correo-r').css("display","none");
    $('#validate-correo-c').css("display","none");
    $('#Correo').css("border-color","#ced4da");
})

$('#Telefono').click(function () {
    $('#validate-telefono-r').css("display","none");
    $('#validate-telefono-n').css("display","none");
    $('#Telefono').css("border-color","#ced4da");
})

$('#clave').click(function () {
    $('#clave').css("border-color","#ced4da");
})
$('#clave_nueva').click(function () {
    $('#clave_nueva').css("border-color","#ced4da");
})
$('#confirmacion_clave').click(function () {
    $('#confirmacion_clave').css("border-color","#ced4da");
})

$('#clave').strength({
    showMeter: false,
    templates:{
      toggle: '<span style="background: #eee;display: flex;align-items: center;width: 43px;justify-content: center;border-radius: 0 4px 4px 0;border: 1px solid #ced4da;  " class="input-group-addon"><span  class="icon-eye {toggleClass}"></span></span>'
    },
    scoreLables: {
        empty: 'Vació',
        invalid: 'Invalido',
        weak: 'Débil',
        good: 'Bueno',
        strong: 'Fuerte'
    }   
})

$('#clave_nueva').strength({
    templates:{
      toggle: '<span style="background: #eee;display: flex;align-items: center;width: 43px;justify-content: center;border-radius: 0 4px 4px 0;border: 1px solid #ced4da;  " class="input-group-addon"><span  class="icon-eye {toggleClass}"></span></span>'
    },
    scoreLables: {
        empty: 'Vació',
        invalid: 'Invalido',
        weak: 'Débil',
        good: 'Bueno',
        strong: 'Fuerte'
    }   
})
$('#confirmacion_clave').strength({
    templates:{
      toggle: '<span style="background: #eee;display: flex;align-items: center;width: 43px;justify-content: center;border-radius: 0 4px 4px 0;border: 1px solid #ced4da;  " class="input-group-addon"><span  class="icon-eye {toggleClass}"></span></span>'
    },
    scoreLables: {
        empty: 'Vació',
        invalid: 'Invalido',
        weak: 'Débil',
        good: 'Bueno',
        strong: 'Fuerte'
    }   
})

