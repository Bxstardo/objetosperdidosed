function datatablePeticion(){
    $("#peticionesDatatable").dataTable({
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ lugares",
            "sZeroRecords": "No se encontraron lugares",
            "sEmptyTable": "No se encontraron lugares",
            "sInfo": "Mostrando lugares del _START_ al _END_ de un total de _TOTAL_ lugares",
            "sInfoEmpty": "Mostrando lugares del 0 al 0 de un total de 0 lugares",
            "sInfoFiltered": "(filtrado de un total de _MAX_ lugares)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive: true

    });
}
function abrirPopupDatosLugar(IdLugar){
    //abrir modal
    $('#peticion-modal-lg').modal().show()
    //cambiar texto de una etiqueta boton
    $("#boton").html('Editar')
    //cambiar action del formulario
    $("#form").attr("action","editarLugar");
    //enviar peticion ajax
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        //Ruta
        url: 'consultarLugarId',
        data: {
            //parametros a enviar
            IdLugar: IdLugar,

        },
        success: function (response) {
            //response trae los datos  
            //perfecto, ahora ese array de datos lugares esta en la variable lugares 
            //ahora si cambia el valor del input
            var lugares = response.lugares;
              //cambiar texto de un input con jquery
              // en val vas a pones el valor, seria datosLugares[0]->NombreLugar, sin comillas a espera es con punto
              // como es un array, se pone el nombre del array, la posicion que seria 0, que es la primer 
              
              //posicion, y despues del punto el atributo
              // vamos a probar si funciona
            $('#IdLugar').val(IdLugar)
            $('#NombreLugar').val(lugares[0].NombreLugar)
            $('#Idzona').val(lugares[0].IdZona)
            $('#Piso').val(lugares[0].Piso)
            if(lugares[0].Estado=="Activo"){
                $('#estado').val(1)
            }else{
                $('#estado').val(2)
            }

        },
        statusCode: {
            404: function () {
                alert('web not found');
            }
        },
    })



 
}

$('#agregar').click(function () {
    $('#peticion-modal-lg').modal().show()
    $('#IdLugar').val("")
    $('#NombreLugar').val("")
    $('#Idzona').val("")
    $('#Piso').val("")
    $('#estado').val("")
    $("#boton").html('Agregar')
})