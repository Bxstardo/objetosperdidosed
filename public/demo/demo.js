var objetosPerdidos = [];
var objetosPerdidosAnio = [];
var objetosDesechados = [];
var objetosEntregados = [];
var labelobjetosPerdidos = [];
var dataobjetosPerdidos = [];
var labelobjetosDesechados = [];
var dataobjetosDesechados = [];
var labelobjetosEntregados = [];
var dataobjetosEntregados = [];


$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$.ajax({
  url: 'generarEstadisticasInicio',
  success: function (response) {
    objetosPerdidos = response.objetosPerdidos
    objetosPerdidosAnio = response.objetosPerdidosAnio
    objetosDesechados = response.objetosDesechados
    objetosEntregados = response.objetosEntregados

    for (let i = 1; i < objetosPerdidos.length + 1; i++) {
      labelobjetosPerdidos.push(i);
    }
    for (let i = 0; i < objetosPerdidos.length; i++) {
      dataobjetosPerdidos.push(objetosPerdidos[i]);
    }

    for (let i = 1; i < objetosDesechados.length + 1; i++) {
      labelobjetosDesechados.push(i);
    }
    for (let i = 0; i < objetosDesechados.length; i++) {
      dataobjetosDesechados.push(objetosDesechados[i]);
    }

    for (let i = 1; i < objetosEntregados.length + 1; i++) {
      labelobjetosEntregados.push(i);
    }
    for (let i = 0; i < objetosEntregados.length; i++) {
      dataobjetosEntregados.push(objetosEntregados[i]);
    }
  },
  statusCode: {
    404: function () {
      alert('web not found');
    }
  },
  async: false,
})




chartColor = "#FFFFFF";

// General configuration for the charts with Line gradientStroke
gradientChartOptionsConfiguration = {
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  tooltips: {
    bodySpacing: 4,
    mode: "nearest",
    intersect: 0,
    position: "nearest",
    xPadding: 10,
    yPadding: 10,
    caretPadding: 10
  },
  responsive: 1,

  scales: {
    yAxes: [{
      display: 0,
      gridLines: 0,
      ticks: {
        display: false,
        beginAtZero: true
      },
      gridLines: {
        zeroLineColor: "transparent",
        drawTicks: false,
        display: false,
        drawBorder: false
      }
    }],
    xAxes: [{
      display: 0,
      gridLines: 0,
      ticks: {
        display: false
      },
      gridLines: {
        zeroLineColor: "transparent",
        drawTicks: false,
        display: false,
        drawBorder: false
      }
    }]
  },
  layout: {
    padding: {
      left: 0,
      right: 0,
      top: 15,
      bottom: 15
    }
  }
};

gradientChartOptionsConfigurationWithNumbersAndGrid = {
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  tooltips: {
    bodySpacing: 4,
    mode: "nearest",
    intersect: 0,
    position: "nearest",
    xPadding: 10,
    yPadding: 10,
    caretPadding: 10
  },
  responsive: true,
  scales: {
    yAxes: [{
      gridLines: 0,
      gridLines: {
        zeroLineColor: "transparent",
        drawBorder: false
      }
    }],
    xAxes: [{
      display: 0,
      gridLines: 0,
      ticks: {
        display: false,
        beginAtZero: true
      },
      gridLines: {
        zeroLineColor: "transparent",
        drawTicks: false,
        display: false,
        drawBorder: false
      }
    }]
  },
  layout: {
    padding: {
      left: 0,
      right: 0,
      top: 15,
      bottom: 15
    }
  }
};

var ctx = document.getElementById('bigDashboardChart').getContext("2d");

var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
gradientStroke.addColorStop(0, '#80b6f4');
gradientStroke.addColorStop(1, chartColor);

var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],
    datasets: [{
      label: "Objetos",
      borderColor: chartColor,
      pointBorderColor: chartColor,
      pointBackgroundColor: "#1e3d60",
      pointHoverBackgroundColor: "#1e3d60",
      pointHoverBorderColor: chartColor,
      pointBorderWidth: 1,
      pointHoverRadius: 7,
      pointHoverBorderWidth: 2,
      pointRadius: 5,
      fill: true,
      backgroundColor: gradientFill,
      borderWidth: 2,
      data: [
        objetosPerdidosAnio[0].Enero,
        objetosPerdidosAnio[0].Febrero,
        objetosPerdidosAnio[0].Marzo,
        objetosPerdidosAnio[0].Abril,
        objetosPerdidosAnio[0].Mayo,
        objetosPerdidosAnio[0].Junio,
        objetosPerdidosAnio[0].Julio,
        objetosPerdidosAnio[0].Agosto,
        objetosPerdidosAnio[0].Septiembre,
        objetosPerdidosAnio[0].Octubre,
        objetosPerdidosAnio[0].Noviembre,
        objetosPerdidosAnio[0].Diciembre,
      ]
    }]
  },
  options: {
    layout: {
      padding: {
        left: 20,
        right: 20,
        top: 0,
        bottom: 0
      }
    },
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: '#fff',
      titleFontColor: '#333',
      bodyFontColor: '#666',
      bodySpacing: 4,
      xPadding: 12,
      mode: "nearest",
      intersect: 0,
      position: "nearest"
    },
    legend: {
      position: "bottom",
      fillStyle: "#FFF",
      display: false
    },
    scales: {
      yAxes: [{
        ticks: {
          fontColor: "rgba(255,255,255,0.4)",
          fontStyle: "bold",
          beginAtZero: true,
          maxTicksLimit: 5,
          padding: 10
        },
        gridLines: {
          drawTicks: true,
          drawBorder: false,
          display: true,
          color: "rgba(255,255,255,0.1)",
          zeroLineColor: "transparent"
        }

      }],
      xAxes: [{
        gridLines: {
          zeroLineColor: "transparent",
          display: false,

        },
        ticks: {
          padding: 10,
          fontColor: "rgba(255,255,255,0.4)",
          fontStyle: "bold"
        }
      }]
    }
  }
});

var cardStatsMiniLineColor = "#fff",
  cardStatsMiniDotColor = "#fff";

ctx = document.getElementById('lineChartExample').getContext("2d");

gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
gradientStroke.addColorStop(0, '#80b6f4');
gradientStroke.addColorStop(1, chartColor);

gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
gradientFill.addColorStop(1, "rgba(249, 99, 59, 0.40)");

myChart = new Chart(ctx, {
  type: 'line',
  responsive: true,
  data: {
    labels: labelobjetosPerdidos,
    datasets: [{
      label: "Objetos",
      borderColor: "#f96332",
      pointBorderColor: "#FFF",
      pointBackgroundColor: "#f96332",
      pointBorderWidth: 2,
      pointHoverRadius: 4,
      pointHoverBorderWidth: 1,
      pointRadius: 4,
      fill: true,
      backgroundColor: gradientFill,
      borderWidth: 2,
      data: dataobjetosPerdidos
    }]
  },
  options: gradientChartOptionsConfigurationWithNumbersAndGrid
});


ctx = document.getElementById('lineChartExampleWithNumbersAndGrid').getContext("2d");

gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
gradientStroke.addColorStop(0, '#18ce0f');
gradientStroke.addColorStop(1, chartColor);

gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
gradientFill.addColorStop(1, hexToRGB('#18ce0f', 0.4));

myChart = new Chart(ctx, {
  type: 'line',
  responsive: true,
  data: {
    labels: labelobjetosEntregados,
    datasets: [{
      label: "Objetos",
      borderColor: "#18ce0f",
      pointBorderColor: "#FFF",
      pointBackgroundColor: "#18ce0f",
      pointBorderWidth: 2,
      pointHoverRadius: 4,
      pointHoverBorderWidth: 1,
      pointRadius: 4,
      fill: true,
      backgroundColor: gradientFill,
      borderWidth: 2,
      data: dataobjetosEntregados
    }]
  },
  options: gradientChartOptionsConfigurationWithNumbersAndGrid
});

var e = document.getElementById("barChartSimpleGradientsNumbers").getContext("2d");

gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
gradientFill.addColorStop(1, hexToRGB('#2CA8FF', 0.6));

var a = {
  type: "bar",
  data: {
    labels: labelobjetosDesechados,
    datasets: [{
      label: "Objetos",
      backgroundColor: gradientFill,
      borderColor: "#2CA8FF",
      pointBorderColor: "#FFF",
      pointBackgroundColor: "#2CA8FF",
      pointBorderWidth: 2,
      pointHoverRadius: 4,
      pointHoverBorderWidth: 1,
      pointRadius: 4,
      fill: true,
      borderWidth: 1,
      data: dataobjetosDesechados
    }]
  },
  options: gradientChartOptionsConfigurationWithNumbersAndGrid
};

var viewsChart = new Chart(e, a);
