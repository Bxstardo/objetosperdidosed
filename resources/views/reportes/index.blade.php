@extends('layoutEmpleado')

@section('titulo')
    Reportes
@endsection

@section('reportes')
    class="active "
@endsection

@section('contenido')
    <div class="panel-header panel-header-sm" style="background: #414141">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header " style=" padding-bottom: 1px;">
                        <h5 class="title">Reportes</h5>
                        <hr>
                    </div>
                    <div class="card-body">
                        <form method="GET" action="{{ route("generarReporte") }}">
                            @csrf
                            <div class="row justify-content-center">
                                <input type="hidden" name="estado" id="estado">
                                <div class="col-md-12">
                                    <script>$('#estado').val(3);</script>
                                    @if ($errors->get('year2') || $errors->get('mes2'))
                                    <script>$('#estado').val(5);</script>
                                    @endif
                                    @if ($errors->get('year3') || $errors->get('mes3'))
                                    <script>$('#estado').val(4);</script>
                                    @endif
                                    <div class="tabset" align="center">
                                        
                                        <!-- Tab 1 -->
                                        <input type="radio" name="tabset" id="tab1"  onclick="Cmes(1)" aria-controls="idpeticion-tab" checked
                                        >
                                        <label for="tab1">Objetos perdidos</label>
                                        <!-- Tab 2 -->
                                        <input type="radio" name="tabset" id="tab2"  onclick="Cmes(2)" aria-controls="cedula-tab"
                                        @if ($errors->get('year2') || $errors->get('mes2'))
                                        checked @endif>

                                        <label for="tab2">Objetos entregados</label>
                                        <!-- Tab 3 -->
                                        <input type="radio" name="tabset" id="tab3"  onclick="Cmes(3)" aria-controls="pasaporte-tab"
                                        @if ($errors->get('year3') || $errors->get('mes3'))
                                        checked @endif>
                                        
                                        <label for="tab3">Objetos desechados</label>

                                        <div class="tab-panels">
                                            <section id="idpeticion-tab" class="tab-panel">
                                                <div class="row justify-content-center">

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="year1">Año</label>
                                                            <select name="year1" class="form-control form-control-lg  mes"
                                                                id="year1"  >
                                                                <option selected disabled value="">Año</option>
                                                                @foreach ($years as $year)
                                                                <option value="{{$year->fecha}}">{{$year->fecha}}</option>  
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="mes1">Mes</label>
                                                            <select name="mes1" class="form-control form-control-lg"
                                                                id="mes1" >
                                                                <option selected disabled value="">Mes</option>
                                                               
                                                            </select>
                                                        </div>
                                                    </div>
                                                   
                                                </div>

                                                @error('year1')
                                                <br>
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert" align="center" style="width: 40%;">
                                                    Por favor seleccione el año en el que desee generar reporte
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                @enderror
                                                @error('mes1')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert" align="center" style="width: 40%;">
                                                    Por favor seleccione el mes en el que desee generar reporte
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                @enderror
                                            </section>
                                            <section id="cedula-tab" class="tab-panel">
                                                <div class="row justify-content-center">

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="year2">Año</label>
                                                            <select name="year2" class="form-control form-control-lg mes"
                                                                id="year2" >
                                                                <option selected disabled value="">Año</option>
                                                                @foreach ($years as $year)
                                                                <option value="{{$year->fecha}}">{{$year->fecha}}</option>  
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="mes2">Mes</label>
                                                            <select name="mes2" class="form-control form-control-lg "
                                                                id="mes2" >
                                                                <option selected disabled value="">Mes</option>
                                                               
                                                            </select>
                                                        </div>
                                                    </div>
                                               
                                                </div>
                                                @error('year2')
                                                <br>
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert" align="center" style="width: 40%;">
                                                    Por favor seleccione el año en el que desee generar reporte
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                @enderror
                                                @error('mes2')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert" align="center" style="width: 40%;">
                                                    Por favor seleccione el mes en el que desee generar reporte
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                @enderror
                                            </section>
                                            <section id="pasaporte-tab" class="tab-panel">
                                                <div class="row justify-content-center">

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="year3">Año</label>
                                                            <select name="year3" class="form-control form-control-lg mes"
                                                                id="year3"  >
                                                                <option selected disabled value="">Año</option>
                                                                @foreach ($years as $year)
                                                                <option value="{{$year->fecha}}">{{$year->fecha}}</option>  
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="mes3">Mes</label>
                                                            <select name="mes3" class="form-control form-control-lg"
                                                                id="mes3" >
                                                                <option selected disabled value="">Mes</option>
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                                @error('year3')
                                                <br>
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert" align="center" style="width: 40%;">
                                                    Por favor seleccione el año en el que desee generar reporte
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                @enderror
                                                @error('mes3')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert" align="center" style="width: 40%;">
                                                    Por favor seleccione el mes en el que desee generar reporte
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                @enderror
                                              
                                            </section>
                                        </div>

                                    </div>

                                </div>
                                
                              

                                <div class="col-md-12" align="center">
                                    <button class=" btn btn-danger" type="submit" name="tipo" value="month">Descargar reporte por mes <i class="icon-file-pdf"></i></button>
                                    <button class=" btn btn-danger" type="submit" name="tipo" value="year">Descargar reporte por año <i class="icon-file-pdf"></i></button>
                                </div>

                        </form>

                    </div>


                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript" src="{!!  asset('js/enviosAjax/reportes.js') !!}"></script>
@endsection
