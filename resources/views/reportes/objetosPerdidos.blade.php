<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
        @page{
    margin: 0px;
    padding: 0px;
    font-size: 1rem;
    
}

body{
    margin: 3cm 2cm 2cm;
}
header{
    position: fixed;
    top: 0cm;
    left: 0cm;
    right: 0cm;
    background: #292929;
    color:white;
    text-align: center;
    line-height: 30px
}
section{
    position: fixed;
    bottom: 0cm;
    left: 0cm;
    right: 0cm;
    height: 2cm;
    background: #292929;
    color:white;    
    text-align: center;
    line-height: 30px
}
p{
    margin: 20px;
    padding: 0;
    font-size: 25px;
    font-weight: 1000
}
    </style>
<title>Objetos {{ $estado }} - {{ $fecha }}</title>
 
</head>
<body>  
    <header>

        <p>Objetos {{ $estado }} -  {{ $fecha }} </p>

    </header>

        <table class="table table-sm table-striped">
            <thead style="background: #292929; color: white; font-weight: 600">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Tipo</th>
                <th scope="col">Color</th>
                <th scope="col">Tamaño</th>
                <th scope="col">Descripción</th>
                @if ($estado == "Entregados")
                <th scope="col">Fecha de encuentro</th>
                <th scope="col">Fecha de entrega</th>
                @elseif ($estado == "Desechados")
                <th scope="col">Fecha de encuentro</th>
                <th scope="col">Fecha de desechado</th>
                @else
                <th scope="col">Fecha de encuentro</th>
                @endif
              </tr>
            </thead>
            <tbody>
              @foreach ($objetos as $objeto)
              <tr>
                <td>{{ $objeto->IdObjeto }}</td>
                <td>{{ $objeto->Tipo }}</td>
                <td>{{ $objeto->Color }}</td>
                <td>{{ $objeto->Tamano }}</td>
                <td>{{ $objeto->Descripcion}}</td>
                @if ($estado == "Perdidos")
                <td>{{ $objeto->Fecha_encuentro }}</td>
                @else
                <td>{{ $objeto->Fecha_encuentro}}</td>
                <td>{{ $objeto->Fechaentrega}}</td>
                @endif
              </tr>
              @endforeach
             
            </tbody>
          </table>
</body>
</html>