@extends('layoutEmpleado')

@section('autorizarPeticion')
    class="active "
@endsection

@section('titulo')
    Autorizar Petición
@endsection

@section('contenido')
    <div class="panel-header panel-header-sm" style="background: #414141">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header " style=" padding-bottom: 1px;">
                        <h5 class="title">Autorizar petición</h5>
                        <hr>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tabset" align="center">
                                        <!-- Tab 1 -->
                                        <input type="radio" name="tabset" id="tab1" aria-controls="idpeticion-tab" checked>
                                        <label for="tab1">Numero petición</label>
                                        <!-- Tab 2 -->
                                        <input type="radio" name="tabset" id="tab2" aria-controls="cedula-tab">
                                        <label for="tab2">Cedula ciudadana</label>
                                        <!-- Tab 3 -->
                                        <input type="radio" name="tabset" id="tab3" aria-controls="pasaporte-tab">
                                        <label for="tab3">Pasaporte</label>

                                        <div class="tab-panels">
                                            <section id="idpeticion-tab" class="tab-panel">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6" style="margin-top: 10px">
                                                                <input type="text" class="form-control"
                                                                placeholder="Numero de peticion" name="idpeticion"
                                                                id="idpeticion">
                                                            </div>
                                                            <div class="col-md-6" style="text-align: start;margin-top:10px">
                                                                <button class=" btn btn-warning peticionesValidadas" style=" margin: 0; height: 44px; width:100%;">&nbsp; Consultar &nbsp;</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section id="cedula-tab" class="tab-panel">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6" style="margin-top: 10px">
                                                                <input type="text" class="form-control" name="identificacion"
                                                                id="identificacion" placeholder="Cedula ciudadana">
                                                            </div>
                                                            <div class="col-md-6" style="text-align: start;margin-top:10px">
                                                                <button class=" btn btn-warning peticionesValidadas" style=" margin: 0; height: 44px; width:100%;">&nbsp; Consultar &nbsp;</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section id="pasaporte-tab" class="tab-panel">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6" style="margin-top: 10px">
                                                                <input type="text" class="form-control" placeholder="Pasaporte"
                                                                id="pasaporte" name="pasaporte">
                                                            </div>
                                                            <div class="col-md-6" style="text-align: start;margin-top:10px">
                                                                <button class=" btn btn-warning peticionesValidadas" style=" margin: 0; height: 44px; width:100%;">&nbsp; Consultar &nbsp;</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>

                                    </div>

                                </div>

                          
                                <div class="col-md-6">
                                    <a href="{{route("consultarPeticion")}}" type="button" class="btn btn-secondary">Peticiones</a>
                                    <a href="#" type="button" class="btn btn-warning disabled">Peticiones por autorizar</a>
                                </div>
                                <div class="col-md-6" align="right">
                                    <button id="peticionesValidadasTodos" class=" btn btn-warning">Consultar
                                        Todo</button>
                                </div>
                        </form>
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%" id="peticionesDatatable">
                                <thead>
                                    <tr>
                                        <th>Tipo objeto</th>
                                        <th>No. Petición</th>
                                        <th>Tamaño objeto</th>
                                        <th>Color objeto</th>
                                        <th>Fecha Petición</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="peticiones">
                                    @foreach ($peticiones as $peticion)
                                        <tr>
                                            <td>{{ $peticion->objeto }}</td>
                                            <td>{{ $peticion->idpeticion }}</td>
                                            <td>{{ $peticion->Tamano }}</td>
                                            <td>{{ $peticion->color }}</td>
                                            <td>{{ $peticion->fechapeticion }}</td>
                                            <td><a href="#" class="btn btn-warning" onclick="abrirPopupDatosPeticion({{ $peticion->idpeticion }})"><i
                                                        class="icon-pencil" 
                                                        ></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>


                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="peticion-modal-lg" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="exampleModalLabel">Datos de la petición</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div id="cliente-peticion" class="col-md-6">
                                          
                                            </div>
                                            <div id="autorizado-peticion" class="col-md-6">
                                          
                                            </div>
                                            <div id="objetoSolicitado-peticion" class="col-md-12">
                                            
                                            </div>

                                            <div id="objeto-peticion" class="col-md-12">
                                              
                                            </div>
                                        </div>

                                    </div>

                                    <div class="modal-footer justify-content-right">
                                        <input type="hidden" name="idPeticionObjeto" id="idPeticionObjeto">
                                        <input type="hidden" name="idPeticionSolicitud" id="idPeticionSolicitud">
                                        <input type="hidden" name="idObjeto" id="idObjeto">
                                        <input type="hidden" name="identificacionDirector" id="identificacionDirector" value="79743645">
                                        <input type="hidden" name="detalle" id="detalle" value="">
                                        <div id="botones-peticion">
                                          
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- modal detalle negacion -->
                        <div class="modal fade" id="detalle-modal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog  modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="exampleModalLabel">Detalle de negación</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                    <p>Por favor escribe la razon del por que denegaras la petición</p>
                                    <div class="input-form">
                                        <textarea name="detalle" id="detalle"
                                        onkeyup="countChars(this);" maxlength="250"></textarea>
                                    <p id="charNum">0 caracteres</p>
                                    </div>
                                    <div class="modal-footer justify-content-right">
                                        <button type="button" onclick="volverPeticion()" class="btn btn-warning">Volver</button>
                                        <button type="button" onclick="autorizarPeticion(9)" class="btn btn-danger">Denegar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                       
                        <script type="text/javascript">
                            $(document).ready(function() {
                                datatablePeticion();
                            })

                        </script>


                    </div>


                </div>
            </div>
        </div>
    </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript" src="{!!  asset('js/enviosAjax/autorizacion.js') !!}"></script>
@endsection