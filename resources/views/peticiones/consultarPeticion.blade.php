@extends('layoutEmpleado')

@if(auth()->user()->Rol == "operario")
@section('validarPeticion')
    class="active "
@endsection
@endif
@if(auth()->user()->Rol == "operario")
@section('autorizarPeticion')
    class="active "
@endsection
@endif

@section('titulo')
    Consultar petición
@endsection

@section('contenido')
<div class="panel-header panel-header-sm" style="background: #414141">
</div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header " style=" padding-bottom: 1px;">
                        <h5 class="title">Consultar petición</h5>
                        <hr>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tabset" align="center">
                                        <!-- Tab 1 -->
                                        <input type="radio" name="tabset" id="tab1" aria-controls="idpeticion-tab" checked>
                                        <label for="tab1">Numero petición</label>
                                        <!-- Tab 2 -->
                                        <input type="radio" name="tabset" id="tab2" aria-controls="cedula-tab">
                                        <label for="tab2">Cédula ciudadana</label>
                                        <!-- Tab 3 -->
                                        <input type="radio" name="tabset" id="tab3" aria-controls="pasaporte-tab">
                                        <label for="tab3">Pasaporte</label>

                                        <div class="tab-panels">
                                            <section id="idpeticion-tab" class="tab-panel">
                                                <div class="col-md-4 inputResponsive">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6" style="margin-top: 10px">
                                                                <input type="text" class="form-control"
                                                                placeholder="Numero de petición" name="idpeticion"
                                                                id="idpeticion">
                                                            </div>
                                                            <div class="col-md-6" style="text-align: start; margin-top: 10px">
                                                                <button class=" btn btn-warning peticionesC" style=" margin: 0; height: 44px; width:100%;">&nbsp; Consultar &nbsp;</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section id="cedula-tab" class="tab-panel">
                                                <div class="col-md-4 inputResponsive">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6" style="margin-top: 10px">
                                                                <input type="text" class="form-control" name="identificacion"
                                                                id="identificacion" placeholder="Cédula ciudadana">
                                                            </div>
                                                            <div class="col-md-6" style="text-align: start; margin-top: 10px">
                                                                <button class=" btn btn-warning peticionesC"  style=" margin: 0; height: 44px; width:100%;">&nbsp; Consultar &nbsp;</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </section>
                                            <section id="pasaporte-tab" class="tab-panel ">
                                                <div class="col-md-4 inputResponsive">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6" style="margin-top: 10px">
                                                                <input type="text" class="form-control" placeholder="Pasaporte"
                                                                id="pasaporte" name="pasaporte">
                                                            </div>
                                                            <div class="col-md-6" style="text-align: start; margin-top: 10px">
                                                                <button class=" btn btn-warning peticionesC"   style=" margin: 0; height: 44px; width:100%;">&nbsp; Consultar &nbsp;</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </section>
                                        </div>

                                    </div>

                                </div>

                         
                                    
                               
                                    <div class="col-md-6">
                                            @if (auth()->user()->Rol == "director")
                                                <a href="#" type="button" class="btn btn-warning disabled">Peticiones</a>
                                                <a href="{{route("autorizarPeticionVista")}}" type="button" class="btn btn-secondary">Peticiones por autorizar</a>
                                            @elseif (auth()->user()->Rol == "operario")  
                                                <a href="#" type="button" class="btn btn-warning disabled">Peticiones</a>
                                                <a href="{{route("validarPeticionVista")}}" type="button" class="btn btn-secondary">Peticiones por validar</a>
                                            @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button id="peticionesCTodos" class=" btn btn-warning">Consultar
                                            Todo</button>
                                       
                                    </div>
    
                              
                  

                        </form>
                        <div class="row">
                            
                        </div>
                        <div class="col-lg-12">
                            <table class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%" id="peticionesDatatable">
                                <thead>
                                    <tr>
                                        <th>No. Petición</th>
                                        <th>Fecha de petición</th>
                                        <th>Estado</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="peticiones">
                                    @foreach ($peticiones as $peticion)
                                        <tr>
                                            <td>{{ $peticion->idpeticion }}</td>
                                            <td>{{ $peticion->fechapeticion }}</td>
                                            <td><i class="icon-circle" style="color: 
                                                @if($peticion->estado=='Denegado')
                                                red
                                                @elseif($peticion->estado=='Autorizado')
                                                green
                                                @elseif($peticion->estado=='Pendiente')
                                                lightslategrey
                                                @else
                                                #ffd54b
                                                @endif
                                                "></i>{{ $peticion->estado }}</td>
                                            <td><a href="#" class="btn btn-warning" onclick="consultarDatosPeticion({{ $peticion->idpeticion }})"><i
                                                        class="icon-eye"
                                                     ></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>


                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="peticion-modal-lg" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document" >
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="exampleModalLabel">Datos de la petición</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div id="cliente-peticion" class="col-md-6">
                                          
                                            </div>
                                            <div id="autorizado-peticion" class="col-md-6">
                                          
                                            </div>
                                            <div id="objetoSolicitado-peticion" class="col-lg-12">
                                            
                                            </div>
                                            
                                            <div id="objeto-peticion" class="col-lg-12">
                                              
                                            </div>
                                   
                                        
                                        </div>

                                    </div>

                                    <div class="modal-footer justify-content-right">
                                        <input type="hidden" name="idPeticionObjeto" id="idPeticionObjeto">
                                        <input type="hidden" name="idPeticionSolicitud" id="idPeticionSolicitud">
                                        <input type="hidden" name="idObjeto" id="idObjeto">
                                        <input type="hidden" name="identificacionOperario" id="identificacionOperario" value="1000199999">
                                        <input type="hidden" name="detalle" id="detalle" value="">
                                        <div id="botones-peticion">
                                          
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function() {
                                datatablePeticion();
                            })

                        </script>


                    </div>


                </div>
            </div>
        </div>
    </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript" src="{!!  asset('js/enviosAjax/validaciones.js') !!}"></script>
    <script type="text/javascript" src="{!!  asset('js/enviosAjax/extra.js') !!}"></script>
@endsection