@extends('layout')
<link type="text/css" rel="stylesheet" href="{!!  asset('css/formulario.css') !!}" media="screen,projection" />
<link type="text/css" rel="stylesheet" href="{!!  asset('css/tabs.css') !!}" media="screen,projection" />
@section('contenido')
    <div class="contenidoCentrado">
        <div class="formStyle-Titulo">
            Consultar Petición
        </div>
        <div class="formStyle">
            
            <p>Verifica el estado de la petición ingresando el numero de petición que se te fue dado
                al realizar tu petición o consúltalo mediante tu identificación
            </p>
<div class="tabset">
    <!-- Tab 1 -->
    <input type="radio" name="tabset" id="tab1" aria-controls="idpeticion-tab" checked>
    <label for="tab1">Numero petición</label>
    <!-- Tab 2 -->
    <input type="radio" name="tabset" id="tab2" aria-controls="cedula-tab">
    <label for="tab2">Cédula ciudadana</label>
    <!-- Tab 3 -->
    <input type="radio" name="tabset" id="tab3" aria-controls="pasaporte-tab">
    <label for="tab3">Pasaporte</label>
    
    <div class="tab-panels">
      <section id="idpeticion-tab" class="tab-panel">
        <h4>Numero petición</h4>
        <div class="input_form center">
            <input type="text" id="idpeticion" pattern="[0-9]" title="Solo numeros por favor" style="min-width: 200px"
        </div>

    </section>
      <section id="cedula-tab" class="tab-panel" pattern="[0-9]" title="Solo numeros por favor" > 
        <h4>Cédula ciudadana</h4>
        <div class="input_form center">
            <input type="text" id="cedula" style="min-width: 200px">
        </div>
      </section>
      <section id="pasaporte-tab" class="tab-panel" >
        <h4>Pasaporte</h4>
        <div class="input_form center">
            <input type="text" id="pasaporte" style="min-width: 200px">
        </div>
      </section>
    </div>
    
  </div>

      
            <div class="input_form center">
                <button class="btn-form"  id="datosPeticion">Consultar</button>
            </div>

            <table class="container">
                <thead>
                    <tr>
                        <th>
                            <h1>No. petición</h1>
                        </th>
                        <th>
                            <h1>Fecha de petición</h1>
                        </th>
                        <th>
                            <h1>Estado</h1>
                        </th>
                        <th>
                            <h1>Detalle</h1>
                        </th>
                    </tr>
                </thead>
                <tbody id="peticiones">
                
                </tbody>
            </table>
        </div>
    </div>
    
       <!-- Modal -->
       <div class="overlay" id="overlay">
        <div class="popup" id="popup">
            <h3>Información de la petición</h3>
            <div class="contenido-popup">
                <hr>
                <div class="subtituloMover">
                <div class="input-modal">
                    <div id="objeto-peticion">
                        <!-- Contenido cargado por js -->
                    </div>
                </div>
                </div>
                <hr>
            </div>
            <button type="button" class="step__button" id="btn-cerrar-popup">Cerrar</button>


        </div>
    </div>

</div>



@endsection
@section('scripts')
<script type="text/javascript" src="{!!  asset('js/enviosAjax/peticiones.js') !!}"></script>
@endsection