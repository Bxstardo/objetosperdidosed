@extends('layout')

@section('estilos')
    <link type="text/css" rel="stylesheet" href="{!!  asset('css/formulario.css') !!}" media="screen,projection" />
   
@endsection

@section('contenido')
    <!-- multistep form -->
    <div class="formulario-contenido">
        <img src="{{ asset('images/DatosPersonales.svg') }}" class="imgMediana">
        <form class="form-register" action="{{ route('insertarPeticion') }}" method="POST">
            @csrf
            <!-- progressbar -->
            <ul class="progressbar">
                <li class="progressbar__option active" id="progress1">Datos personales</li>
                <li class="progressbar__option" id="progress2">Descripción del objeto</li>
                <li class="progressbar__option" id="progress3">Lugar de perdida</li>
            </ul>
            <!-- divs -->
            <div class="step active" id="step-1">
                
                <h2 class="fs-title">Datos personales</h2>
                <h3 class="fs-subtitle">Ingrese sus datos personales y de un autorizado si es necesario</h3>
               
                    <label class="label-i">
                        <span>Nombres</span>
                        <input type="text" name="nombres" id="nombres" class="f-input"  placeholder="Nombres" maxlength="40"/>
                    </label>
                    <label class="label-i">
                        <span>Apellidos</span>
                        <input type="text" name="apellidos" id="apellidos" placeholder="Apellidos" class="f-input" maxlength="40"/>
                    </label>
                    <label class="label-i bigInput ">
                        <span>Tipo de identificación</span>
                    <select id="identificacion" name="identificacion" class="f-select ">
                        <option selected disabled value="">Seleccione tipo de identificación</option>
                        <option value="cedula" >Cédula ciudadana</option>
                        <option value="pasaporte">Pasaporte</option>
                    </select>
                    </label>


                <div class="input-form">
                    <input type="text" name="cedula" placeholder="Cedula ciudadana" id="cedula-input" class="f-input bigInput" maxlength="11"/>
                    <input type="text" name="pasaporte" placeholder="Pasaporte" id="pasaporte-input" class="f-input bigInput"   maxlength="20"/>
                </div>
                <div class="input-form">
                    <label class="label-i">
                        <span>Correo</span>
                    <input type="text" name="correo" id="correo" placeholder="Correo" class="f-input"   maxlength="50"/>
                    </label>
                    <label class="label-i">
                        <span>Telefono</span>
                    <input type="text" name="telefono" id="telefono" placeholder="Numero celular" class="f-input"   maxlength="20" />
                    <label>
                </div>
                <div id="errores">
                
                </div>
               
                <p>¿Deseas registrar una persona autorizada para reclamar tu objeto?</p>

                <div class="radiobutton-form">
                    <input type="radio" name="autorizado" id="si" value="si">
                    <label for="si">Si</label>
                    <input type="radio" name="autorizado" id="no" value="no">
                    <label for="si">No</label>
                </div>
                <div id="autorizado-form">
                    <hr>
                    <h2 class="fs-title">Datos de autorizado</h2>
                    <h3 class="fs-subtitle">Ingrese datos del autorizado</h3>
                    <div class="input-form">
                    <label class="label-i">
                        <span>Nombres</span>
                        <input type="text" name="nombresA" id="nombresA" class="f-input" placeholder="Nombres" />
                    </label>
                    <label class="label-i">
                        <span>Apellidos</span>
                        <input type="text" name="apellidosA" id="apellidosA"  class="f-input" placeholder="Apellidos" />
                    </label>
                    </div>
                    <div class="input-form">
                    <label class="label-i bigInput">
                        <span>Tipo de identificación</span>
                        <select id="identificacionA" name="identificacionA" class="f-select">
                            <option selected disabled value="">Seleccione tipo de identificación</option>
                            <option value="cedula">Cédula ciudadana</option>
                            <option value="pasaporte">Pasaporte</option>
                        </select>
                    </label>
                    </div>
                    <div class="input-form">
                        <input type="text" name="cedulaA" placeholder="Cedula ciudadana" id="cedula-inputA" class="f-input bigInput" />
                        <input type="text" name="pasaporteA" placeholder="Pasaporte" id="pasaporte-inputA" class="f-input bigInput" />
                    </div>
                    <div class="input-form">
                    <label class="label-i bigInput">
                        <span>Telefono</span>
                        <input type="text" name="telefonoA" id="telefonoA" placeholder="Numero celular" class="f-input" />
                    </label>
                    </div>
                    <div id="erroresA">
                
                    </div>
                </div>
                <div align="right">
                <button type="button" class="step__button step__button--next" id="button1">Siguiente</button>
                </div>
            </div>

            <div class="step" id="step-2">
                <h2 class="fs-title">Descripción del objeto</h2>
                <h3 class="fs-subtitle">Ingrese especificación del objeto que ha perdido</h3>
                <div class="input-form">
                    <label class="label-i bigInput">
                    <span>Tipo de objeto</span>
                    <select name="tipoObjeto" id="tipoObjeto" class="f-select">
                        <option selected disabled value="">Tipo de objeto</option>
                        @foreach ($tipos as $tipo)
                            <option value="{{ $tipo->Tipo }}">{{ $tipo->Tipo }}</option>
                        @endforeach
                    </select>
                    </label>
                </div>
                <div id="otros">
                    <div class="input-form">
                        <input type="text" name="tipoObjeto-input" id="tipoObjeto-input" placeholder="Nombre del objeto" class="f-input bigInput" />
                    </div>
                    <hr>
                </div>

                <div class="input-form">
                    <label class="label-i bigInput">
                    <span>Tamaño de objeto</span>
                    <select name="tamanoObjeto" id="tamanoObjeto" class="f-select">
                        <option selected disabled value="">Tamaño de objeto</option>
                        <option value="Grande">Grande</option>
                        <option value="Mediano">Mediano</option>
                        <option value="Pequeño">Pequeño</option>
                    </select>
                </label>
                </div>
                <div class="input-form">
                    <label class="label-i bigInput">
                    <span>Color de objeto</span>
                    <select name="colorObjeto" id="colorObjeto" class="f-select">
                        <option selected disabled value="">Color de objeto</option>
                        <option value="Amarillo">Amarillo</option>
                        <option value="Azul">Azul</option>
                        <option value="Rojo">Rojo</option>
                        <option value="Naranja">Naranja</option>
                        <option value="Morado">Morado</option>
                    </select>
                </label>
                </div>
                <div class="input-form">
                    <label class="label-i bigInput">
                    <span>Descripción</span>
                    <textarea name="descripcion" id="descripcion" placeholder="Descripción adicional del objeto"
                        onkeyup="countChars(this);" maxlength="250" class="f-input" style="margin-bottom: 10px"></textarea>
              
                    </label>
                </div>
                <p id="charNum" style="font-size: 15px;">0 caracteres</p>
                <div id="erroresO">

                </div>
                <div align="right">
                <button type="button" class="step__button step__button--back" data-to_step="1"
                    data-step="2">Regresar</button>
                <button type="button" class="step__button step__button--next" id="button2">Siguiente</button>
                </div>
            </div>

            <div class="step" id="step-3">
                <h2 class="fs-title">Lugar de perdida</h2>
                <h3 class="fs-subtitle">Seleccione el lugar mas cercano donde crea que halla perdido el objeto</h3>
                <div class="input-form">
                    <label class="label-i bigInput">
                    <span>Zona</span>
                    <select name="zona" id="zona" class="f-select"> 
                        <option selected disabled>Zona</option>
                        @foreach ($zonas as $zona)
                            <option value="{{ $zona->IdZona }}">{{ $zona->Zona }}</option>
                        @endforeach
                    </select>
                </label>
                </div>
                <div class="input-form">
                    <label class="label-i bigInput">
                    <span>Piso</span>
                    <select name="piso" id="piso" class="f-select">
                        <option selected disabled>Piso de zona</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </label>
                </div>
                <hr style="margin-bottom: 40px">
                <div class="input-form">
                    <label class="label-i bigInput">
                    <span>Lugar</span>
                    <select name="lugar" id="lugar" class="f-select">
                        <option selected disabled value="lugar">Lugar</option>

                    </select>
                </label>
                </div>
                <p style="color: #ffd54b">*Por favor seleccione primero una zona y un piso para que puedas visualizar los
                    lugares*</p>
                <div id="erroresL">
                    
                </div>    
                <div align="right">
                <button type="button" class="step__button step__button--back" data-to_step="2"
                    data-step="3">Regresar</button>
                <button type="button" class="step__button" id="btn-abrir-popup-peticion">Enviar</button>
                </div>
            </div>
        </form>
        <!-- Modal -->
        <div class="overlay" id="overlay">
            <div class="popup" id="popup">
                <h3>Información de la petición</h3>
                <div class="subtituloMover">
                    <p>Revisa detenidamente los datos a enviar</p>
                </div>
               
                <div class="contenido-popup">
                    <hr>
                    <div class="input-modal">
                        <div id="cliente-peticion">
                            <!-- Contenido cargado por js -->
                        </div>
            
                        <hr  class="hrA">
                        <div id="autorizado-peticion">
                            
                            <!-- Contenido cargado por js -->
                        </div>
                        
                    </div>
                    <hr>
                    <div class="input-modal">
                        <div id="lugar-peticion">
                            <!-- Contenido cargado por js -->
                        </div>
                        <hr  class="hrA">

                        <div id="objeto-peticion">
                            <!-- Contenido cargado por js -->
                        </div>
                    </div>
                    <hr>
                </div>
                <div align="right">
                    <button type="button" class="step__button" id="btn-cerrar-popup">Editar</button>
                    <button type="submit" class="step__button" onclick="enviarPeticion()">Enviar</button>
                </div>
            </div>
        </div>

    </div>

    <script>
    
        let form = document.querySelector('.form-register');
        let progressOptions = document.querySelectorAll('.progressbar__option');
        
        form.addEventListener('click', function(e) {
            let element = e.target;
            let isButtonNext = element.classList.contains('step__button--next');
            let isButtonBack = element.classList.contains('step__button--back');
            if (isButtonNext || isButtonBack) {
                let currentStep = document.getElementById('step-' + element.dataset.step);
                let jumpStep = document.getElementById('step-' + element.dataset.to_step);
                currentStep.addEventListener('animationend', function callback() {
                    currentStep.classList.remove('active');
                    jumpStep.classList.add('active');
                    if (isButtonNext) {
                        currentStep.classList.add('to-left');
                        progressOptions[element.dataset.to_step - 1].classList.add('active');
                    } else {
                        jumpStep.classList.remove('to-left');
                        progressOptions[element.dataset.step - 1].classList.remove('active');
                    }
                    currentStep.removeEventListener('animationend', callback);
                });
                currentStep.classList.add('inactive');
                jumpStep.classList.remove('inactive');
            }
        });

     
        </script>
@endsection


@section('scripts')

<script type="text/javascript" src="{!!  asset('js/enviosAjax/peticiones.js') !!}"></script>
@endsection
