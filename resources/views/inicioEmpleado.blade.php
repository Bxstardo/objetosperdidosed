<!--

=========================================================
* Now UI Dashboard - v1.5.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-dashboard
* Copyright 2019 Creative Tim (http://www.creative-tim.com)

* Designed by www.invisionapp.com Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

-->
@extends("layoutEmpleado")

@section('inicioEmpleado')
class="active "
@endsection

@section('titulo')
    Estadísticas
@endsection

@section('contenido')

<div class="panel-header panel-header-lg" style="background: #414141" >
    <canvas id="bigDashboardChart"></canvas>
</div>

<div class="content">
        <div class="row">
          <div class="col-lg-4">
            <div class="card card-chart">
              <div class="card-header">
                <h5 class="card-category">Objetos</h5>
                <h4 class="card-title">Objetos Perdidos</h4>
              </div>
              <div class="card-body">
                <div class="chart-area">
                  <canvas id="lineChartExample"></canvas>
                </div>
              </div>
              <div class="card-footer">
                <div class="stats">
                  Mes actual / {{ $timeNow }}
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="card card-chart">
              <div class="card-header">
                <h5 class="card-category">Objetos</h5>
                <h4 class="card-title">Objetos entregados</h4>
              </div>
              <div class="card-body">
                <div class="chart-area">
                  <canvas id="lineChartExampleWithNumbersAndGrid"></canvas>
                </div>
              </div>
              <div class="card-footer">
                <div class="stats">
                  Mes actual / {{ $timeNow }}
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="card card-chart">
              <div class="card-header">
                <h5 class="card-category">Objetos</h5>
                <h4 class="card-title">Objetos donados</h4>
              </div>
              <div class="card-body">
                <div class="chart-area">
                  <canvas id="barChartSimpleGradientsNumbers"></canvas>
                </div>
              </div>
              <div class="card-footer">
                <div class="stats">
                   Mes actual / {{ $timeNow }} 
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header ">
                <h5 class="card-category">Objetos</h5>
                <h4 class="card-title">Objetos Perdidos</h4>
              </div>
              <div class="card-body ">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" id="peticionesDatatable3">
                    <thead>
                        <tr>
                            <th>Tipo</th>
                            <th>Color</th>
                            <th>Tamaño</th>
                            <th>#</th>
                            <th>Fecha de encuentro</th>
                        </tr>
                    </thead>
                   <tbody>
                        @foreach ($objetos as $objeto)
                            <tr>
                                <td>{{ $objeto->Tipo }}</td>
                                <td>{{ $objeto->Color }}</td>
                                <td>{{ $objeto->Tamano }}</td>
                                <td>{{ $objeto->IdObjeto }}</td>
                                <td>{{ $objeto->Fecha_encuentro }}</td>

                            </tr>
                        @endforeach
                    </tbody>

                  </table>
                </div>
              </div>

            </div>
          </div>
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h5 class="card-category">Todas las peticiones</h5>
                <h4 class="card-title"> Estado de peticiones</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%" id="peticionesDatatable">
                    <thead>
                        <tr>
                            <th>No. Petición</th>
                            <th>Fecha de petición</th>
                            <th>Estado</th>
                     
                        </tr>
                    </thead>
                    <tbody id="peticiones">
                        @foreach ($peticiones as $peticion)
                            <tr>
                                <td>{{ $peticion->idpeticion }}</td>
                                <td>{{ $peticion->fechapeticion }}</td>
                                <td><i class="icon-circle" style="color: 
                                    @if($peticion->estado=='Denegado')
                                    red
                                    @elseif($peticion->estado=='Autorizado')
                                    green
                                    @elseif($peticion->estado=='Pendiente')
                                    lightslategrey
                                    @else
                                    #ffd54b
                                    @endif
                                    "></i>{{ $peticion->estado }}</td>
                            
                            </tr>
                        @endforeach
                    </tbody>

                </table>
                </div>
              </div>
            </div>
          </div>
        </div>
</div> 
     <!-- Modal -->
     <div class="modal fade" id="peticion-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document" >
         <div class="modal-content">
             <div class="modal-header">
                 <h3 class="modal-title" id="exampleModalLabel">Datos de la petición</h3>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="row">
                     <div id="cliente-peticion" class="col-md-6">
                   
                     </div>
                     <div id="autorizado-peticion" class="col-md-6">
                   
                     </div>
                     <div id="objetoSolicitado-peticion" class="col-lg-12">
                     
                     </div>
                     
                     <div id="objeto-peticion" class="col-lg-12">
                       
                     </div>
            
                 
                 </div>

             </div>

             <div class="modal-footer justify-content-right">
                 <input type="hidden" name="idPeticionObjeto" id="idPeticionObjeto">
                 <input type="hidden" name="idPeticionSolicitud" id="idPeticionSolicitud">
                 <input type="hidden" name="idObjeto" id="idObjeto">
                 <input type="hidden" name="identificacionOperario" id="identificacionOperario" value="1000199999">
                 <input type="hidden" name="detalle" id="detalle" value="">
                 <div id="botones-peticion">
                   
                 </div>
               
             </div>
         </div>
     </div>
 </div>

@endsection

@section('script')
<script type="text/javascript" src="{!!  asset('js/enviosAjax/validaciones.js') !!}"></script>
<script type="text/javascript" src="{!!  asset('js/enviosAjax/extra.js') !!}"></script>
<script type="text/javascript">

      datatablePeticion2();


</script>
@endsection