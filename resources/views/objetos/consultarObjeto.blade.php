@extends('layoutEmpleado')

@section('titulo')
    Inventario de objetos perdidos
@endsection

@section('consultarObjeto')
    class="active "
@endsection

@section('contenido')
    @csrf

    <div class="panel-header panel-header-sm" style="background: #414141">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header " style=" padding-bottom: 1px;">
                        @if (auth()->user()->Rol == 'operario')
                            <h5 class="title"> Objetos Perdidos</h5>
                        @endif
                        @if (auth()->user()->Rol == 'director')
                            @if ($tipoO == 'perdidos')
                                <h5 class="title" align="center"> Objetos Perdidos</h5>
                            @elseif($tipoO=="desechados")
                                <h5 class="title" align="center"> Objetos Desechados</h5>
                            @else
                                <h5 class="title" align="center"> Objetos Entregados</h5>
                            @endif
                        @endif
                        <hr>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="row">

                                <div class="col-md-12">
                                    @if (auth()->user()->Rol == 'operario')
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                                    data-target="#reporte-modal"><i class="icon-file-pdf"></i></button>

                                            </div>
                                            <div class="col-md-6" align="right">
                                                <button type="button" class="btn brn-secondary" data-toggle="modal"
                                                    data-target="#tipo-modal">Agregar tipo objeto</button>
                                                <button type="button" class="btn btn-warning" data-toggle="modal"
                                                    data-target="#peticion-modal-lg">Agregar objeto</button>
                                            </div>

                                        </div>
                                    @endif

                                </div>
                                @if (auth()->user()->Rol == 'director')
                                    <div class="col-md-12">
                                        @if ($tipoO == 'perdidos')
                                            <a href="#" type="button" class="btn btn-warning disabled">Objetos Perdidos</a>
                                            <a href="{{ route('consultarObjeto', $tipo = 'desechados') }}" type="button"
                                                class="btn btn-secondary">Objetos Desechados</a>
                                            <a href="{{ route('consultarObjeto', $tipo = 'entregados') }}" type="button"
                                                class="btn btn-secondary">Objetos Entregados</a>
                                        @elseif($tipoO=="desechados")
                                            <a href="{{ route('consultarObjeto', $tipo = 'perdidos') }}" type="button"
                                                class="btn btn-secondary">Objetos Perdidos</a>
                                            <a href="#" type="button" class="btn btn-warning disabled">Objetos
                                                Desechados</a>
                                            <a href="{{ route('consultarObjeto', $tipo = 'entregados') }}" type="button"
                                                class="btn btn-secondary">Objetos Entregados</a>
                                        @else
                                            <a href="{{ route('consultarObjeto', $tipo = 'perdidos') }}" type="button"
                                                class="btn btn-secondary">Objetos Perdidos</a>
                                            <a href="{{ route('consultarObjeto', $tipo = 'desechados') }}" type="button"
                                                class="btn btn-secondary">Objetos Desechados</a>
                                            <a href="#" type="button" class="btn btn-warning disabled">Objetos
                                                Entregados</a>
                                        @endif

                                    </div>
                                @endif
                        </form>

                        <div class="col-md-12">
                            <table class="table table-striped table-bordered display nowrap" id="peticionesDatatable">
                                <thead>
                                    <tr>
                                        <th>Tipo</th>
                                        <th>Color</th>
                                        <th>Tamaño</th>
                                        <th>Descripción</th>
                                        <th>#</th>
                                        @if ($tipoO == 'perdidos')
                                            <th>Fecha de encuentro</th>
                                        @elseif($tipoO=="desechados")
                                            <th>Fecha de encuentro</th>
                                            <th>Fecha de desechado</th>
                                        @else
                                            <th>Fecha de encuentro</th>
                                            <th>Fecha de entrega</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody id="objetos">

                                    @if ((auth()->user()->Rol == 'operario' && $tipoO == 'perdidos') || auth()->user()->Rol == 'director')
                                        @foreach ($objetos as $objeto)
                                            <tr>
                                                <td>{{ $objeto->Tipo }}</td>
                                                <td>{{ $objeto->Color }}</td>
                                                <td>{{ $objeto->Tamano }}</td>
                                                <td>{{ $objeto->Descripcion }}</td>
                                                <td>{{ $objeto->IdObjeto }}</td>
                                                @if ($tipoO == 'perdidos')
                                                    <td>{{ $objeto->Fecha_encuentro }}</td>
                                                @else
                                                    <td>{{ $objeto->Fecha_encuentro }}</td>
                                                    <td>{{ $objeto->FechaEntrega }}</td>
                                                @endif

                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    datatablePeticion();
                                })

                            </script>
                            @if ($errors->get('year1') || $errors->get('mes1'))
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $('#reporte-modal').modal().show()
                                    })

                                </script>
                            @endif

                        </div>

                        <!-- Modal reporte -->
                        <div class="modal fade" id="reporte-modal" tabindex="-1" aria-labelledby="reporteModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="reporteModalLabel">Reporte</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="GET" action="{{ route('generarReporte') }}" id="form">
                                        <div class="modal-body">
                                            <div class="row justify-content-center">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="year1">Año</label>
                                                        <select name="year1" class="form-control form-control-lg  mes"
                                                            id="year1">
                                                            <option selected disabled value="">Año</option>
                                                            @foreach ($years as $year)
                                                                <option value="{{ $year->fecha }}">{{ $year->fecha }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="mes1">Mes</label>
                                                        <select name="mes1" class="form-control form-control-lg" id="mes1">
                                                            <option selected disabled value="">Mes</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            @error('year1')
                                                <br>
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert"
                                                    align="center">
                                                    Por favor seleccione el año en el que desee generar reporte
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @enderror
                                            @error('mes1')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert"
                                                    align="center">
                                                    Por favor seleccione el mes en el que desee generar reporte
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @enderror
                                            <input type="hidden" name="estado" value="3">
                                        </div>
                                        <div class="modal-footer">

                                            <div class="row">
                                                <div class="col-md-2">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Cerrar</button>
                                                </div>
                                                <div class="col-md-5">
                                                    <button class=" btn btn-danger" type="submit" name="tipo"
                                                        value="month">Descargar reporte por mes <i
                                                            class="icon-file-pdf"></i></button>
                                                </div>
                                                <div class="col-md-5">
                                                    <button class=" btn btn-danger" type="submit" name="tipo"
                                                        value="year">Descargar reporte por año <i
                                                            class="icon-file-pdf"></i></button>
                                                </div>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>


                        <!-- Modal tipo -->
                        <div class="modal fade" id="tipo-modal" tabindex="-1" aria-labelledby="tipoModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="tipoModalLabel">Tipo de objeto</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                        <div class="modal-body">
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group-text">
                                                        <input type="text" name="tipo" id="tipo" class="form-control" placeholder="Tipo de objeto" aria-label="Tipo de objeto" aria-describedby="basic-addon2">
                                                        <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" onclick="agregarTipo()">Agregar</button>
                                                        </div>
                                                       
                                                    </div>
                                                    <div class="invalid-feedback" id="tipo-error">
                                                    </div>
                                                </div>
                                              
                                                <div class="col-md-12">
                                                    <table class="table table-striped table-bordered nowrap" id="tipoDatatable" style="width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Id</th>
                                                                <th>Tipo</th>
                                                                <th>Estado</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tipos">
                                                            @foreach ($tipos as $tipo)
                                                                <tr>
                                                                    <td>{{ $tipo->IdTipo }}</td>
                                                                    <td>{{ $tipo->Tipo }}</td>
                                                                    <td>
                                                                        @if ($tipo->Estado==2)
                                                                        <button class="btn btn-primary" onclick="cambiarEstadoTipo({{ $tipo->IdTipo }},1)">
                                                                            Activar
                                                                        </button>
                                                                        @else
                                                                        <button class="btn btn-danger" onclick="cambiarEstadoTipo({{ $tipo->IdTipo }},2)">
                                                                            Inactivar
                                                                        </button>
                                                                        @endif
                                                                    </td>
                                                                    <td><button class="btn btn-warning" onclick="datosTipo({{ $tipo->IdTipo }})"><i class="icon-pencil"></i></button>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                        
                                                    </table>
                                                    <script type="text/javascript">
                                                        $(document).ready(function() {
                                                            datatableTipo();
                                                        })
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal editar tipo -->
                        <div class="modal fade" id="tipoEditar-modal" tabindex="-1" aria-labelledby="tipoEditarModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="tipoEditarModalLabel">Editar</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                        <div class="modal-body">
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Tipo de objeto</label>
                                                    <input type="text" name="tipoE" id="tipoE" class="form-control" placeholder="Tipo de objeto">
                                                    <div class="invalid-feedback" id="tipoE-error">
                                                    </div>
                                                </div>
                                                <div class="col-md-12 text-right">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-secondary"
                                                        onclick="volverTipo()">Volver</button>
                                                    <button class="btn btn-warning" onclick="editarTipo()">Guardar</button>

                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    <!-- Modal Objeto -->
                    <div class="modal fade" id="peticion-modal-lg" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <input type=hidden name="IdentificacionOperario" value="1000579646">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel">Datos del objeto</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <form class="form-register needs-validation" method="POST"
                                        Action="{{ route('agregarObjeto') }}" novalidate>
                                        @csrf
                                        <input type="hidden" name="IdentificacionOperario"
                                            value="{{ auth()->user()->IdentificacionUsuario }}">
                                        <div class="content text-left">
                                            <div class="row">
                                                <div class="form-group col-lg-12">
                                                    <label for="tipoObjeto">Tipo de objeto</label>
                                                    <select name="tipoObjeto" class="form-control form-control-lg"
                                                        id="tipoObjeto" required>
                                                        <option selected disabled value="">Tipo de objeto</option>
                                                        @foreach ($tipos as $tipo)
                                                            @if ($tipo->Estado == 1)
                                                                <option value="{{ $tipo->Tipo }}">{{ $tipo->Tipo }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Por favor seleccione un tipo de objeto
                                                    </div>
                                                </div>
                                                <div class="form-group objeto-otro col-lg-12">

                                                </div>

                                                <div class="form-group col-lg-6">
                                                    <label for="colorObjeto">Color de objeto</label>
                                                    <select name="colorObjeto" class="form-control form-control-lg"
                                                        id="colorObjeto" required>
                                                        <option selected disabled value="">Color de objeto</option>
                                                        <option value="Amarillo">Amarillo</option>
                                                        <option value="Azul">Azul</option>
                                                        <option value="Rojo">Rojo</option>
                                                        <option value="Naranja">Naranja</option>
                                                        <option value="Morado">Morado</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Por favor seleccione un color
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label for="tamanoObjeto">Tamaño de objeto</label>
                                                    <select name="tamanoObjeto" class="form-control form-control-lg"
                                                        id="tamanoObjeto" required>
                                                        <option selected disabled value="">Tamaño de objeto</option>
                                                        <option value="Grande">Grande</option>
                                                        <option value="Mediano">Mediano</option>
                                                        <option value="Pequeño">Pequeño</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Por favor seleccione un tamaño
                                                    </div>
                                                </div>

                                                <div class="form-group col-lg-6">
                                                    <label for="zona">Zona</label>
                                                    <select name="zona" class="form-control form-control-lg" id="zona"
                                                        required>
                                                        <option selected disabled value="">Zona</option>
                                                        @foreach ($zonas as $zona)
                                                            <option value="{{ $zona->IdZona }}">{{ $zona->Zona }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Por favor seleccione una zona
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label for="piso">Piso</label>
                                                    <select name="piso" class="form-control form-control-lg" id="piso"
                                                        required>
                                                        <option selected disabled value="">Piso</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Por favor seleccione un piso
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-12">
                                                    <label for="lugar">Lugar</label>
                                                    <select name="lugar" class="form-control form-control-lg" id="lugar"
                                                        required>
                                                        <option selected disabled value="">Lugar</option>

                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Por favor seleccione un lugar
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-12">
                                                    <label for="descripcion">Descripción</label>
                                                    <textarea name="descripcion" class="form-control" id="descripcion"
                                                        placeholder="Descripcion adicional del objeto" maxlength="250"
                                                        required></textarea>
                                                    <div class="invalid-feedback">
                                                        Por favor ingrese una descripción / Maximo de carecteres: 250
                                                    </div>
                                                </div>
                                            </div>



                                        </div>



                                </div>

                                <div class="modal-footer justify-content-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button class="btn btn-warning">Agregar</button>
                                </div>


                                </form>

                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
    </div>


@endsection

@section('script')
    <script type="text/javascript" src="{!!  asset('js/enviosAjax/objetos.js') !!}"></script>
    <script type="text/javascript" src="{!!  asset('js/enviosAjax/reportes.js') !!}"></script>
    <script type="text/javascript" src="{!!  asset('js/validator.js') !!}"></script>
@endsection
