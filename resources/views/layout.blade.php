<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link type="text/css" rel="stylesheet" href="{!! asset('css/style.css') !!}" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="{!! asset('css/fontello.css') !!}" media="screen,projection" />
    <script src="{!!  asset('js/sweetalert.min.js') !!}"></script>
    <link type="text/css" rel="stylesheet" href="{!! asset('css/sweetalert.css') !!}" />
    <link type="text/css" rel="stylesheet" href="{!! asset('css/modal.css') !!}" />

  
    @yield('estilos')
    
    <title>Objetos Perdidos el Dorado</title>
</head>
<body>

    @if($header=="inicio")
    <section class="Principal">      
     @endif 
            @if($header=="inicio")
            <header class="header2">  
            @else    
            <header class="header3">     
            @endif    

            <div><a href="{{ route('inicioCliente') }}"><img src="{{asset('images/HeaderED.png')}}" class="imageHeader"></a></div>
            <input type="checkbox" id="btn-menu">
            <label for="btn-menu" class="icon-menu"></label>
     
            <!-- Menu Cliente -->    
        
                @if($header=="inicio")
            <nav class="menu">  
                @else
            <nav class="menu2">    
                @endif 
            <ul>
                <li><a href="{{route("realizarPeticion")}}">Realizar Petición</a></li>
                <li><a href="{{route("consultarEstPeticion")}}">Consultar Petición</a></li>
                <li><a class="btn-logout" href="{{route("login")}}">Iniciar Sesión<span class="icon-login"></span></a></li>
            </ul>  
        </header> 
         @if($header=="inicio")     
        <div>
            <img src="{{asset('images/Rombo1.png')}}" class="rombo1">
        </div>
        <div>
            <img src="{{asset('images/Rombo2.png')}}" class="rombo2">
        </div>
        <div>
            <a href="{{ route('realizarPeticion')}}"><img src="{{asset('images/BotonRecuperaloAhora.png')}}" href="#"
                    class="btnRecuperalo"></a>
        </div>
        @endif   
    </section> 
    <main>

        @yield('contenido')


    </main>
    <footer>
        <div class="contenedor-footer">
            <div class="content-foo">
                <h4>Teléfono</h4>
                <p>[+571] 439 7070 EXTENSIÓN 5112</p>
            </div>
            <div class="content-foo">
                <h4>Correo</h4>
                <p>objetosperdidosyolvidados@eldorado.aero.co</p>
            </div>
            <div class="content-foo">
                <h4>Ubicación</h4>
                <p>Ac. 26 ##103-9, Fontibón, Bogotá</p>
            </div>
        </div>
        <h2 class="titulo-final">&copy; OPAIN S.A</h2>
</footer>


<script type="text/javascript" src="{!!  asset('js/jquery.js') !!}"></script>
<script type="text/javascript" src="{!!  asset('js/animations.js') !!}"></script>

@yield('scripts')

</body>
</html>