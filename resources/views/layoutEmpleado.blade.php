<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <style>
 * {
    font-family: poppins;
 
}

@font-face {
    font-family: poppins;
    src: url("../font/Poppins.ttf");
}

  </style>
  <title>
Objetos Perdidos
  </title>

  <!--     Fonts and icons     -->
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link type="text/css" rel="stylesheet" href="{!!  asset('css/now-ui-dashboard.css?v=1.5.0') !!}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{!!  asset('demo/demo.css') !!}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{!!  asset('css/tabs.css') !!}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{!! asset('css/fontello.css') !!}" media="screen,projection" />
  <script type="text/javascript"  src="{!! asset('js/core/jquery.min.js')!!}"></script>
  <link type="text/css" rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{!! asset('css/datatablesBootstrap.min.css') !!}" media="screen,projection" />
  <script type="text/javascript"  src="{!! asset('js/datatablesJquery.min.js')!!}"></script>
  <script type="text/javascript"  src="{!! asset('js/datatablesBootstrap.min.js')!!}"></script>
  <link type="text/css" rel="stylesheet" href="{!!  asset('css/styleAdmin.css') !!}" media="screen,projection" />
  <script type="text/javascript" src="{!!  asset('js/password_strength.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('js/jquery-strength.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('js/chart.js') !!}"></script>

  <!--  extension responsive  -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
  
</head>
<body class="">
  <div class="wrapper">
    <div class="sidebar" data-color="yellow">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="{{ route('inicioEmpleado')}}" class="simple-text logo-mini">
<img src="{{asset('images/Logo.png')}}" style='margin-top:-6px'></a>
        <a href="{{ route('inicioEmpleado')}}" class="simple-text logo-normal">
          Objetos Perdidos 
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        @if (auth()->user()->Rol == "operario")
        <ul class="nav">
          <li @yield('inicioEmpleado')>
            <a href="{{ route('inicioEmpleado')}}">
              <i class="icon-home"></i>
              <p>Inicio</p>
            </a>
          </li>
          <li @yield('validarPeticion')>
            <a href="{{ route('validarPeticionVista')}}">
              <i class="icon-wpforms"></i>
              <p>Peticiones</p>
            </a>
          </li>
          <li @yield('consultarObjeto')>
            <a href="{{ route('consultarObjeto')}}">
              <i class="icon-suitcase"></i>
              <p>Inventario Objetos</p>
            </a>
          </li>
          <li @yield('consultarLugar')>
            <a href="{{ route('consultarLugar')}}">
              <i class="icon-map"></i>
              <p>Lugares</p>
            </a>
          </li>
      
        
        </ul>
        @else
        <ul class="nav">
          <li @yield('inicioEmpleado')>
            <a href="{{ route('inicioEmpleado')}}">
              <i class="icon-home"></i>
              <p>Inicio</p>
            </a>
          </li>
          <li @yield('autorizarPeticion')>
            <a href="{{ route('autorizarPeticionVista')}}">
              <i class="icon-wpforms"></i>
              <p>Peticiones</p>
            </a>
          </li>
          <li @yield('consultarObjeto')>
            <a href="{{ route('consultarObjeto')}}">
              <i class="icon-suitcase"></i>
              <p>Inventario Objetos</p>
            </a>
          </li>
          <li @yield('estadisticas')>
            <a href="{{ route('estadisticas')}}">
              <i class="icon-chart-bar"></i>
              <p>Estadísticas</p>
            </a>
          </li>
          <li @yield('reportes')>
            <a href="{{ route('reportes')}}">
              <i class="icon-doc-inv"></i>
              <p>Reportes</p>
            </a>
          </li>
  
        
        
        </ul>

        @endif
        
       
      </div>
    </div>
    <div class="main-panel" id="main-panel">

  

<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent   navbar-absolute" style="background: #414141">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#">@yield('titulo')</a>
    </div>
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="icon-user"></i>
            <p>
            <span class="d-md-block">{{ auth()->user()->Nombres }} {{ auth()->user()->Apellidos }} <i class="text-warning">({{ auth()->user()->Rol }})</i></span>
            </p>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" data-toggle="modal" data-target="#perfil-modal">Perfil</a>
            <a class="dropdown-item" data-toggle="modal" data-target="#contrasena-modal">Cambiar clave</a>
            <form action="{{ route('logout')}}" method="post">
              @csrf
            <button class="dropdown-item" href="">Cerrar sesion</button>
            </form>
          </div>
        </li>
  
      </ul>
  </div>

</nav>
<!-- End Navbar -->


<!-- Perfil -->

<div class="modal fade" id="perfil-modal" tabindex="-1" role="dialog"
aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Datos personales</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-md-6">
              <label for="Nombres">Nombres</label>
              <input id="Nombres" name="Nombres" class="form-control" type="text" placeholder="{{ auth()->user()->Nombres}}"  readonly>
            </div>
            <div class="form-group col-md-6">
              <label for="Apellidos">Apellidos</label>
              <input id="Apellidos" name="Apellidos" class="form-control" type="text" placeholder="{{ auth()->user()->Apellidos}}" readonly>
            </div>
            <div class="form-group col-md-6">
              <label for="IdentificacionUsuario">Identificación</label>
              <input id="IdentificacionUsuario" name="IdentificacionUsuario" class="form-control" type="text" value="{{ auth()->user()->IdentificacionUsuario}}" readonly>
            </div>
            <div class="form-group col-md-6">
              <label for="Rol">Cargo</label>
              <input id="Rol" name="Rol" class="form-control" type="text" placeholder="{{ ucwords(auth()->user()->Rol)}}" readonly>
            </div>
            <hr style="margin: 10px; width: 100%">
            <div class="form-group col-md-6">
              <label for="Telefono">Telefono</label>
              <input id="Telefono" name="Telefono" class="form-control" type="text" value="{{ auth()->user()->Telefono}}" readonly required>
              <div class="invalid-feedback" id="validate-telefono-n">
                Por favor escriba solo numeros
              </div> 
              <div class="invalid-feedback" id="validate-telefono-r">
                Por favor no deje el campo vacio
              </div> 
            </div>
            <div class="form-group col-md-6">
              <label for="Correo">Correo</label>
              <input id="Correo" name="Correo" class="form-control" type="text" value="{{ auth()->user()->Correo}}" readonly required>
              <div class="invalid-feedback" id="validate-correo-c">
                Por favor ingrese un correo valido
              </div> 
              <div class="invalid-feedback" id="validate-correo-r">
                Por favor no deje el campo vacio

              </div> 
            </div>
           
            <div class="form-group col-md-12 text-right">
                <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-warning" id="btnDatos" value="cambiar">Actualizar datos</button>
            </div>
          </div>
        </div>
    </div>
</div>
</div>
@if (session('mensaje'))
  <script>
    $(document).ready(function() {
      swal("La contraseña se ha cambiado correctamente","", "success");
     });
  </script>
@endif
@if (auth()->user()->Estado == 12)
    <script>
      $(document).ready(function() {
        $('#contrasena-modal').modal().show();
     });
    </script>
@endif
<!-- Cambiar contraseña -->
@if ($errors->get('clave') || $errors->get('clave_nueva') || $errors->get('confirmacion_clave') )
<script type="text/javascript">
    $(document).ready(function() {
        $('#contrasena-modal').modal().show()
    })
</script>
@endif
<div class="modal fade" id="contrasena-modal" tabindex="-1" role="dialog"
aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Cambiar clave</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          @if (auth()->user()->Estado == 12)
          <form method="get" class=" needs-validation" action="{{ route('nuevaClave') }}" novalidate>
          @else
          <form method="get" class=" needs-validation" action="{{ route('cambiarClave') }}" novalidate>
          @endif  
            @csrf
          <div class="row">
            <div class="form-group col-md-12">
              @if (auth()->user()->Estado == 12)
              <div class="alert alert-warning alert-dismissible fade show" role="alert" align="center">
                Por favor ingrese una nueva contraseña
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @else
              <label for="clave">Clave actual</label>
              <input id="clave" name="clave" class="form-control" type="password" min="5" max="60" required>
              @error('clave')
              <div class="alert alert-danger alert-dismissible fade show" role="alert" align="center">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <script>
                $('#clave').css('border-color','red')
              </script>
              @enderror
              @endif
            </div>
            <div class="form-group col-md-12">
              <label for="clave_nueva">Nueva clave</label>
              <input id="clave_nueva" name="clave_nueva" class="form-control" min="5" max="60"type="password" required>
              @error('clave_nueva')
              <div class="alert alert-danger alert-dismissible fade show" role="alert" align="center">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <script>
                $('#clave_nueva').css('border-color','red')
                $('#confirmacion_clave').css('border-color','red')
              </script>
              @enderror
            </div>
          
            <input name="Identificacion" type="hidden" value="{{ auth()->user()->IdentificacionUsuario}}">
            <div class="form-group col-md-12">
              <label for="confirmacion_clave">Confirmar nueva clave</label>
              <input id="confirmacion_clave" name="confirmacion_clave" class="form-control" min="5" max="60" type="password" required>
              @error('confirmacion_clave')
              <div class="alert alert-danger alert-dismissible fade show" role="alert" align="center">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <script>
                $('#clave_nueva').css('border-color','red')
                $('#confirmacion_clave').css('border-color','red')
              </script>
              @enderror
            </div>
            <div class="form-group col-md-12 text-right" style="margin-top: -30px;">
              <button class="btn btn-warning">Cambiar clave</button>
            </div>
        
          </div>
        </form>
        </div>
    </div>
</div>
</div>

      @yield("contenido")

      <footer class="footer">

        <div class=" container-fluid ">
                  <!--
          <nav>
            <ul>
              <li>
                <a href="https://www.creative-tim.com">
                  Creative Tim
                </a>
              </li>
              <li>
                <a href="http://presentation.creative-tim.com">
                  About Us
                </a>
              </li>
              <li>
                <a href="http://blog.creative-tim.com">
                  Blog
                </a>
              </li>
            </ul>
          </nav>
        -->
          <div class="copyright" id="copyright">
            &copy; <script>
              document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script> OPAIN S.A
          </div>
        </div>
      </footer>
    </div>
  </div>

  <script type="text/javascript" src="{!!  asset('js/core/popper.min.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('js/core/bootstrap.min.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('js/plugins/perfect-scrollbar.jquery.min.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('js/plugins/chartjs.min.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('js/plugins/bootstrap-notify.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('js/now-ui-dashboard.min.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('demo/demo.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('js/sweetalert.min.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('js/bootstrap-validator.js') !!}"></script>
  <script type="text/javascript" src="{!!  asset('js/enviosAjax/usuario.js') !!}"></script>

  <!-- extension responsive -->
  <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap.min.js"></script>
    

@yield('script')
<script>
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
  </script>

</body>

</html>