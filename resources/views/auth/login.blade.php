<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title></title>
    <link type="text/css" rel="stylesheet" href="{!! asset('css/sweetalert.css') !!}" />
    <script type="text/javascript" src="{!!  asset('js/sweetalert.min.js') !!}"></script>
    <link type="text/css" rel="stylesheet" href='{{ asset('css/login.css') }}'>
    <style>
      html {
       overflow: auto;
}
body {
       height:auto;
       overflow: auto;
}
.scrollable {
       position:inherit;
}
    </style>
  </head>
  <body align="center">

      <form class="login-form" method="POST" action="{{ route('login')}}">

         @csrf
        
  <img src="{{asset('images/Logo.png')}}" class="images2">
        <h1>Inicio de sesión</h1>
   
       
        <div class="txtb">
        <input type="text"  type="text" class="form-control  @error('IdentificacionUsuario') is-invalid @enderror" maxlength="11" name="IdentificacionUsuario"   required autocomplete="IdentificacionUsuario" >
          <span data-placeholder="Identificación"></span>      
        </div>

        <div class="txtb">
        
          <input type="password"  type="password" class="form-control @error('password') is-invalid @enderror" name="password" minlength="6" maxlength="20" required autocomplete="current-password">
          <span data-placeholder="Contraseña" ></span>
        </div>

        
        @error('IdentificacionUsuario')
        <p class="error">
            <strong>{{ $message }}</strong>
        </p>
        @enderror
        @if (session('mensaje') )
        <script>
                  swal("La contraseña se ha restablecido correctamente", "Le hemos enviado su nueva contraseña al correo electronico", "success");
        </script>
        <p class="error" style="color: green">
            <strong>{{ session('mensaje') }}</strong>
        </p>
        @endif
        @if (session('inactivo') )
        <p class="error">
            <strong>{{ session('inactivo') }}</strong>
        </p>
        @endif
 
        <input type="submit" class="logbtn" value="Ingresar">

        <div class="bottom-text">
          @if (Route::has('password.request'))
         <a href="{{ route('password.request') }}">  {{ __('¿Olvidaste la contraseña?') }}</a>
         @endif
         <br><br>
         <p class="TextEnd">Si eres parte del equipo de objetos perdidos
         del aeropuerto el dorado puedes iniciar sesion <p>
        </div>

  
      </form>
      <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
      <script type="text/javascript">
      $(".txtb input").on("focus",function(){
        $(this).addClass("focus");
      });

      $(".txtb input").on("blur",function(){
        if($(this).val() == "")
        $(this).removeClass("focus");
      });

      </script>
     
  </body>
</html>

