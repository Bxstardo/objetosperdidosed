<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title></title>
  
    <link type="text/css" rel="stylesheet" href='{{ asset('css/login.css') }}'>
  </head>
  <body align="center">

      <form class="login-form" method="POST" action="{{ route('recuperarClave')}}" style="padding-top:35px; height:65% ">

         @csrf
        
  <img src="{{asset('images/Logo.png')}}" class="images2">
  <h2 style="font-size:25px">Restablecimiento de contraseña</h2>
       
        <div class="txtb">
        <input type="text"  type="email" class="form-control" name="Correo" autocomplete="Correo" >
          <span data-placeholder="Correo"></span>      
        </div>

       
        @if (session('mensaje') )
        <p class="error">
            <strong>{{ session('mensaje') }}</strong>
        </p>
        @endif
        @error('Correo')
        <p class="error">
            <strong>{{ $message }}</strong>
        </p>
        @enderror
  
        <input type="submit" class="logbtn" value="Recuperar contraseña">
        <p style="margin-top:15px">Para restablecer tu contraseña por favor ingresa el correo electrónico que tengas registrado en tu usuario</p>

  
  
      </form>
      <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
      <script type="text/javascript">
      $(".txtb input").on("focus",function(){
        $(this).addClass("focus");
      });

      $(".txtb input").on("blur",function(){
        if($(this).val() == "")
        $(this).removeClass("focus");
      });

      </script>
     

  </body>
</html>

