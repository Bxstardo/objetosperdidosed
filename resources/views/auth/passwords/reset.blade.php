<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title></title>
  
    <link type="text/css" rel="stylesheet" href='{{ asset('css/login.css') }}'>
  </head>
  <body align="center">

      <form class="login-form" method="POST" action="{{ route('resetearClave')}}" style="padding-top:30px; height:68% ">

         @csrf
        
  <img src="{{asset('images/Logo.png')}}" class="images2">
  <h2 style="font-size:25px">Restablecimiento de contraseña</h2>

   
       
        <div class="txtb">
        <input  type="email" class="form-control focus" name="Correo" autocomplete="Correo" value="{{$Correo}}" >
          <span data-placeholder="Correo"></span>      
        </div>       @error('Correo')
        <p class="error">
            <strong>{{ $message }}</strong>
        </p>
        @enderror
        <div class="txtb">
          <input   type="number" class="form-control" name="Codigo" autocomplete="Codigo">
            <span data-placeholder="Codigo de recuperación"></span>      
        </div>
  
       
        @if (session('mensaje') )
        <p class="error">
            <strong>{{ session('mensaje') }}</strong>
        </p>
        @endif
 
        @error('Codigo')
        <p class="error">
            <strong>{{ $message }}</strong>
        </p>
        @enderror
        <input type="submit" class="logbtn" value="Enviar mi contraseña">

        <p style="margin-top:15px">Por favor ingrese el codigo de recuperación que se te fue enviado al correo</p>
  
  
      </form>
      <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
      <script type="text/javascript">
      $(".txtb input").on("focus",function(){
        $(this).addClass("focus");
      });

      $(".txtb input").on("blur",function(){
        if($(this).val() == "")
        $(this).removeClass("focus");
      });

      </script>
     

  </body>
</html>

