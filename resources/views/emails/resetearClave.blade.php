@extends('emails.layout')

@section('contenido')
<div align="left" style="border-radius:  0px 0px 10px 10px; background: #e2e2e2; height: 100%; padding: 30px">

    <p>Hola <b>{{$usuario->Nombres}} {{$usuario->Apellidos}}</b></p>
    <p>La contraseña fue restablecida con éxito, le hemos enviado un correo electrónico con su nueva contraseña para que pueda iniciar sesión.</p>
    <h2>{{$clave}}</h2>
    <p>Para iniciar sesión haga clic en el siguiente botón:</p>
    <a href="{{url('login')}}" style="
    padding: 10px 20px;
    font-size: 13px;
    background: #c8a847;
    color: rgb(0, 0, 0);
    margin-top: 30px;
    margin-bottom: 30px;
    text-decoration: none;
    border-radius: 5px;">Iniciar sesión</a>
    <p>&nbsp;</p>
    <p>Si el siguiente botón no le funciona, copie y pegue la siguiente url en su navegador:</p>
    <p>{{url('login')}}</p>
</div>
@endsection   