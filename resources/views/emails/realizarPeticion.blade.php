@extends('emails.layout')

@section('contenido')
    <div align="left" style="border-radius:  0px 0px 10px 10px; background: #e2e2e2; height: 100%; padding: 30px">
        <p  style="margin: 0; color: black;text-align:center;">Hola <span style="font-weight: bold">{{ $info->nombres." ".$info->apellidos }}</span> tu petición se ha realizado correctamente, tu numero de peticion es el <span style="font-weight: bold">No. {{$id[0]->idpeticion }}</span>.
            Por favor revise constantemente el estado de su petición en nuestra <a style="color:#ffc400" href="www.objetosperdidosed.com">pagina web</a>. </p>
        <hr>
        <h3 style="color:#ffc400 ">Datos personales</h3>
            <table style="margin-bottom: 2em; width: 273px ">
                <tr>
                    <td><span style="font-weight:bold">Nombres:</span></td>
                    <td>{{ $info->nombres }}</td>
                </tr>
                <tr>
                    <td><span style="font-weight:bold">Apellidos:</span></td>
                    <td>{{ $info->apellidos }}</td>
                </tr>
                <tr>
                    @if ($info->identificacion == "cedula")
                        <td><span style="font-weight:bold">Cedula:</span></td>
                        <td>{{ $info->cedula }}</td>
                    @else
                        <td><span style="font-weight:bold">Pasaporte:</span></td>
                        <td>{{ $info->pasaporte }}</td>   
                    @endif     
                </tr>
                <tr>
                    <td><span style="font-weight:bold">Correo:</span></td>
                    <td>{{ $info->correo }}</td>
                </tr>
                <tr>
                    <td><span style="font-weight:bold">Telefono:</span></td>
                    <td>{{ $info->telefono }}</td>
                </tr>
            </table>
        <hr>
        @if ($info->autorizado == "si")
        <h3 style="color:#ffc400 ">Datos de autorizado</h3>
        <table style="margin-bottom: 2em; width: 273px ">
            <tr>
                <td><span style="font-weight:bold">Nombres:</span></td>
                <td>{{ $info->nombresA }}</td>
            </tr>
            <tr>
                <td><span style="font-weight:bold">Apellidos:</span></td>
                <td>{{ $info->apellidosA }}</td>
            </tr>
            <tr>
                @if ($info->identificacionA == "cedula")
                    <td><span style="font-weight:bold">Cedula:</span></td>
                    <td>{{ $info->cedulaA }}</td>
                @else
                    <td><span style="font-weight:bold">Pasaporte:</span></td>
                    <td>{{ $info->pasaporteA }}</td>   
                @endif     
            </tr>
            <tr>
                <td><span style="font-weight:bold">Telefono:</span></td>
                <td>{{ $info->telefonoA }}</td>
            </tr>
        </table>
        <hr>    
        @endif
       
        <h3 style="color:#ffc400 ">Datos del objeto</h3>
        <table style="margin-bottom: 2em; width: 273px ">
            <tr>
                @if ($info->tipoObjeto == "otros")
                    <td><span style="font-weight:bold">Tipo de objeto:</span></td>
                    <td>{{ $info-> tipoObjetoInput }}</td>
                @else 
                    <td><span style="font-weight:bold">Tipo de objeto:</span></td>
                    <td>{{ $info->tipoObjeto }}</td>    
                @endif
                
            </tr>
            <tr>
                <td><span style="font-weight:bold">Tamaño:</span></td>
                <td>{{ $info->tamanoObjeto }}</td>
            </tr>
            <tr>
                <td><span style="font-weight:bold">Color:</span></td>
                <td>{{ $info->colorObjeto }}</td>
            </tr>
            <tr>
                <td><span style="font-weight:bold">Descripcion:</span></td>
                <td>{{ $info->descripcion }}</td>
            </tr>
        </table>
        <hr>
        <h3 style="color:#ffc400 ">Lugar de perdida</h3>
        <table style="margin-bottom: 2em; width: 273px ">
            <tr>
                <td><span style="font-weight:bold">Zona:</span></td>
                <td>{{ $info->nombreZona }}</td>
            </tr>
            <tr>
                <td><span style="font-weight:bold">Piso:</span></td>
                <td>{{ $info->piso }}</td>
            </tr>
            <tr>
                <td><span style="font-weight:bold">Lugar:</span></td>
                <td>{{ $info->nombreLugar }}</td>
            </tr>
        </table>
    </div>
@endsection   