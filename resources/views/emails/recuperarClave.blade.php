@extends('emails.layout')

@section('contenido')
 <div align="left" style="border-radius:  0px 0px 10px 10px; background: #e2e2e2; height: 100%; padding: 30px">

    <p>Hola <b>{{$usuario->Nombres}} {{$usuario->Apellidos}}</b></p>
    <p>Este es un correo electronico que le ayudara a reestablecer la contraseña de su cuenta en nuestra plataforma</p>
    <p>Para continuar haga clic en el siguiente botón e ingrese el siguiente código: <h2>{{$codigo}}</h2></p>
    <a href="{{url('resetearClave?Correo='.$usuario->Correo)}}" style="
    padding: 10px 20px;
    font-size: 13px;
    background: #c8a847;
    color: rgb(0, 0, 0);
    margin-top: 30px;
    margin-bottom: 30px;
    text-decoration: none;
    border-radius: 5px;">Resetear mi contraseña</a>
    <p>&nbsp;</p>
    <p>Si el siguiente botón no le funciona, copie y pegue la siguiente url en su navegador:</p>
    <p>{{url('resetearClave?Correo='.$usuario->Correo)}}</p>
@endsection   