@extends('emails.layout')
@section('contenido')
    <div align="center" style="border-radius:  0px 0px 10px 10px; background: #e2e2e2; height: 100%; padding: 50px">
        <p style="color: #ffc400; font-weight: bold; font-size: 25px; margin-top:0">Petición No. {{ $datosPeticion->idPeticionSolicitud }}</p>
        <p>Hola <span style="font-weight: bold">{{ $datosCliente[0]->Nombres." ".$datosCliente[0]->Apellidos }}</span> te informamos que tu petición <span style="font-weight: bold">No. {{ $datosPeticion->idPeticionSolicitud }}</span> ha sido 
            @if ($datosPeticion->estado == 8)
                <span style="font-weight: bold; color: green;">autorizado</span>, le pedimos por favor que se acerque a nuestra oficina de objetos perdidos entre el {{ date("Y/m/d") }}
                y el <span style="font-weight: bold">{{ date('Y/m/d', strtotime($datosCliente[0]->FechaEntrega)) }}</span> en los horarios de atención de objetos perdidos</p>
                <p><span style="font-weight: bold">Lugar:</span> Ac. 26 ##103-9, Fontibón, Bogotá - Oficina Objetos Perdidos </p>
            @else 
                <span style="font-weight: bold; color: red;">denegado</span>
                <p><span style="font-weight: bold">Detalle de negación</span></p>
                <p>{{ $datosPeticion->detalle }}</p>
            @endif
            
        
    </div>
@endsection   