@extends('layoutEmpleado')

@section('titulo')
    Consultar Lugares
@endsection

@section('consultarLugar')
    class="active "
@endsection

@section('contenido')
@csrf

<div class="panel-header panel-header-sm" style="background: #414141">
</div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header " style=" padding-bottom: 1px;">
                        <h5 class="title">Lugares</h5>
                        <hr>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="row">
                               
                                <div class="col-md-12" >
                                    @if(session('lugar'))
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        {{session('lugar')->NombreLugar}} del piso {{session('lugar')->Piso}} de la zona {{session('zona')->Zona}} ya existe
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    @endif
                                    <div class="text-right">
                                        <button type="button" class="btn btn-warning" id="agregar" >Agregar lugar</button>

                                    </div>
                                </div>
                              
                        </form>
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered display nowrap" id="peticionesDatatable">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Piso</th>
                                        <th>Zona</th>
                                        <th>#</th>
                                        <th>Estado</th>
                                        <th></th>
                                    </tr>
                                </thead>
                               <tbody id="lugares">
                                    @foreach ($lugares as $lugar)
                                        <tr>
                                            <td>{{ $lugar->NombreLugar }}</td>
                                            <td>{{ $lugar->Piso }}</td>
                                            <td>{{ $lugar->Zona }}</td>
                                            <td>{{ $lugar->IdLugar }}</td>
                                            <td>
                                                @if ($lugar->Estado=="Inactivo")
                                                <a href="cambiarEstadoLugar/{{ $lugar->IdLugar }}/{{ $lugar->Estado }}" class="btn btn-primary">
                                                    Activar
                                                </a>
                                                @else
                                                <a href="cambiarEstadoLugar/{{ $lugar->IdLugar }}/{{ $lugar->Estado }}" class="btn btn-danger" >
                                                    Inactivar
                                                </a>
                                                @endif
                                            </td>
                                            <td><a href="#" class="btn btn-warning" onclick="abrirPopupDatosLugar({{ $lugar->IdLugar }})"><i
                                                class="icon-pencil" 
                                                ></i></a>
                                        </tr>
                                       
                                    @endforeach
                                </tbody>

                            </table>
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    datatablePeticion();
                                })
    
                            </script>
    

                        </div>

            
                        
                    <div class="modal fade" id="peticion-modal-lg" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg " role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel">Datos del Lugar</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                  

                                        <form id="form" class="form-register needs-validation" action="{{ route("ingresarLugar") }}"  method="POST" novalidate>
                                            @csrf

                                            <div class="content text-left">
                                                
                                         
                                                <div class="form-group">  
                                    
                                                    <input type="hidden" name="IdLugar"  id="IdLugar" >
                                             
                                                </div>
                                                <div class="form-group">  
                                                    
                                                    <label for="NombreLugar">Nombre Lugar</label>
                                                    <input type="text" name="NombreLugar" class="form-control form-control-lg" id="NombreLugar"  required>
                                                    <div class="invalid-feedback">
                                                        Por favor escriba un lugar / Maximo de caracteres: 30
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="Piso">Piso</label>
                                                    <select name="Piso" class="form-control form-control-lg" id="Piso" required>
                                                        <option selected disabled value="">Piso</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Por favor seleccione un piso
                                                    </div>
                                                </div> 
                                                <div class="form-group">
                                                    <label for="Idzona">Zona</label>
                                                    <select name="Idzona" class="form-control form-control-lg" id="Idzona" required>
                                                        <option selected disabled value="">Zona</option>
                                                        @foreach ($zonas as $zona)
                                                        <option value="{{ $zona->IdZona }}">{{ $zona->Zona }}</option>
                                                    @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Por favor seleccione una zona
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="estado">Estado</label>
                                                    <select name="Estado" class="form-control form-control-lg" id="estado" required>
                                                        <option selected disabled value="">Estado</option>
                                                        <option value="1">Activo</option>
                                                        <option value="2">Inactivo</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Seleccione un estado
                                                    </div>
                                                </div>
                                            </div>
                           
                            


                                            <div class="modal-footer justify-content-right">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" id="boton" class="btn btn-warning">Agregar</button>
                                                
                                            </div>

                                
                                    </form>

                                </div>
                            </div>
                        </div><!--fin-->
                    </div>
                    </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

 
@endsection

@section('script')
    <script type="text/javascript" src="{!!  asset('js/enviosAjax/lugares.js') !!}"></script>
   
@endsection