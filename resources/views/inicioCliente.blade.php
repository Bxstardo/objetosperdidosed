@extends('layout')


@section('contenido')

    <!-- Proceso para recuperar objeto perdido -->
    <section class="seccion4">
        <h2 class="titulo animado">Devolución de objeto perdido</h2>
        <div class="contenedor-sobre-nosotros2">
            <div class="texto-sobre-nosotros2 animado2">
                <h3><span class="icon-flight"></span>Proceso después de perder un objeto</h3>
                <p>
                    Al momento de llegar el objeto al cuarto de objetos perdidos el operario ingresara al inventario el objeto que se encontró, una vez hecho esto se espera
                    a que este objeto sea solicitado por medio de una petición el cual deberá ser realizada por el usuario que perdió el objeto. Cuando llega una petición el operador la revisa y hace una comparación entre los objetos
                    que hay el inventario con el objeto que esta siendo solicitado, si un objeto del inventario coincide con el objeto solicitado la petición sera validada o denegada por el operario, luego
                    esta deberá pasar por el mismo proceso de comparación en un segundo filtro con el director de seguridad aeroportuaria, el cual decidirá finalmente si autorizar o no la petición.
                </p>  
            </div>
            <img src="{{asset('images/Peticiones.svg')}}" class="peticionImg animado" alt="">        
        </div>
    </section>

    <!-- ¿Como recuperar tu objeto perdido? -->
    <section class="seccion2">
        <h2 class="titulo animado2">Pasos a seguir para encontrar objeto perdido</h2>
        <div class="contenedor" align="center">
            <div class="card animado">
                <figure>
                    <img src="{{asset('images/1.png')}}">
                </figure>
                <div class="card-contenido">
                    <h3>Realizar Petición</h3>
                    <p>Realiza una petición especificando tus datos personales y datos del objeto a solicitar</p>
                </div>
            </div>
            <div class="card animado">
                <figure>
                    <img src="{{asset('images/2.png')}}">
                </figure>
                <div class="card-contenido">
                    <h3>Consultar</h3>
                    <p>Consulta constantemente el estado de tu petición y revisa tu correo electrónico</p>
                </div>
            </div>
            <div class="card animado">
                <figure>
                    <img src="{{asset('images/3.png')}}">
                </figure>
                <div class="card-contenido">
                    <h3>Reclamar</h3>
                    <p>Una vez tu petición sea validada y aprobada puedes reclamar tu objeto perdido</p>
                </div>
            </div>
        </div>
    </section>
    <!--  Sabias que... -->
    <section class="seccion3">
        <h2 class="titulo animado">Sabias Que...</h2>
        <div class="contenedorSabiasQue animado">
            <div class="ch-item">				
                <div class="ch-info">
                    <div class="ch-info-front ch-img-1"></div>
                    <div class="ch-info-back">
                        <p>Al día se pierden mas de 100 objetos en el aeropuerto el dorado, entre estos papeles personales</p>
                    </div>	
                </div>
            </div>
            <div class="ch-item">				
                <div class="ch-info">
                    <div class="ch-info-front ch-img-1"></div>
                    <div class="ch-info-back">
                        <p>Los objetos perdidos con mayor frecuencia son las maletas </p>
                    </div>	
                </div>
            </div>
            <div class="ch-item">				
                <div class="ch-info">
                    <div class="ch-info-front ch-img-1"></div>
                    <div class="ch-info-back">
                        <p>Después de 60 días los objetos perdidos que no fueron solicitados son donados</p>
                    </div>	
                </div>
            </div>
            <div class="ch-item">				
                <div class="ch-info">
                    <div class="ch-info-front ch-img-1"></div>
                    <div class="ch-info-back"> 
               
                        <p>2 de cada 5 personas que pierden sus objetos o no lo reclaman debido a
                            desconocen el proceso
                        </p>
                    </div>	
                </div>
            </div>
        </div>
    </section> 
    <section class="seccion5">
        <h2 class="titulo animado">Objetos Perdidos el Dorado</h2>
        <div class="contenedor-sobre-nosotros">
            <img src="{{asset('images/Trabajadores.svg')}}" class="animado" alt="">
            <div class="texto-sobre-nosotros animado2">
                <h3><span>1</span>Nuestra oficina</h3>
                <p>
                    Contamos con una Oficina de Objetos Perdidos y Olvidados
            ubicada en el Muelle Internacional piso 2 del Aeropuerto
            Internacional El Dorado Luis Carlos Galán Sarmiento,
            donde usted puede entregar y/o preguntar por algún artículo que
            encuentre o pierda durante su estancia en el mismo
         
                </p>
                <h3><span>2</span>Objetivo</h3>
                <p>
                    Establecer los lineamientos para el control y manejo de elementos perdidos y olvidados,
                    entregados a la oficina de seguridad aeroportuaria destinada para tal fin, dentro de las
                    instalaciones del Aeropuerto Internacional El Dorado Luis Carlos Galán Sarmiento.
                    Inicia con la recepción del objeto y finaliza con la entrega del elemento a su dueño o con la
                    disposición final, de acuerdo al Plan de Seguridad del Aeropuerto.
                </p>
            </div>
        </div>
    </section>
        <!-- alert -->

<script>
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
var peticion = getUrlParameter('peticion');
var idpeticion = getUrlParameter('idpeticion');
if (peticion==1) {
    swal({
        title: "¡La peticion se envio exitosamente!",
        text: "Tu numero de petición es el No. "+idpeticion+".Por favor revise constantemente el estado de su petición o queda atent@ a tu correo, te estaremos avisando el proceso de su petición",
        icon: "success",
        button: "Cerrar",
    }); 
}
</script>   




@endsection
