<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






/* Login */
Auth::routes();

Route::get('recuperarClave','Usuarios@getRecuperarClave')->name('recuperarClave');

Route::post('recuperarClave','Usuarios@postRecuperarClave')->name('recuperarClave');

Route::get('resetearClave','Usuarios@getResetearClave')->name('resetearClave');

Route::post('resetearClave','Usuarios@postResetearClave')->name('resetearClave');

/*  _-- Cliente --_  */

Route::get('/', function(){
    $header ="inicio";
    return view('inicioCliente', compact('header'));
})->name('inicioCliente');

Route::get('realizarPeticion', 'Peticiones@realizarPeticion')->name('realizarPeticion');

Route::post('insertarPeticion', 'Peticiones@insertarPeticion')->name('insertarPeticion');

Route::get('consultarEstPeticion', 'Peticiones@consultarEstPeticion')->name('consultarEstPeticion');


Route::get('detallePeticion', 'Peticiones@detallePeticion')->name('detallePeticion');

Route::get('datosPeticion', 'Peticiones@datosPeticion')->name('datosPeticion');

/*Lugares */

Route::get('cargarLugares','Peticiones@cargarLugares')->name('cargarLugares');

/*  _-- Autenticado --_  */
Route::group(['middleware' => 'auth'], function () {

    Route::get('generarEstadisticasInicio', 'Usuarios@estadisticas')->name('generarEstadisticasInicio');

    Route::group(['middleware' => 'inactivo'], function () {
        
        Route::get('consultarPeticion', 'Peticiones@consultarPeticion')->name('consultarPeticion');

        Route::get('peticiones', 'Peticiones@peticiones')->name('peticiones');

        Route::get('peticionesTodos', 'Peticiones@peticionesTodos')->name('peticionesTodos');

        Route::get('datosPeticionPorId', 'Validaciones@datosPeticionPorId')->name('datosPeticionPorId');

        /* Usuario */

        Route::get('datosPersonales', 'Usuarios@datosPersonales')->name('datosPersonales');

        Route::get('cambiarClave','Usuarios@cambiarClave')->name('cambiarClave');

        Route::get('nuevaClave','Usuarios@nuevaClave')->name('nuevaClave');

        Route::get('inicioEmpleado', 'Usuarios@InicioEmpleado')->name('inicioEmpleado');

        Route::group(['middleware' => 'restablecer_clave'], function () {

            /* Objeto */

            Route::get('consultarObjeto/{tipo?}', 'Objetos@consultarObjeto')->name('consultarObjeto');

            /* Reportes */

            Route::get('generarReporte','Objetos@generarReporte')->name('generarReporte');

            /* Estadisticas */
            
            Route::get('generarEstadistica','Objetos@generarEstadistica')->name('generarEstadistica');

            Route::get('mes','Objetos@mes')->name('mes');


            /* _-- Operario --_  */
            Route::group(['middleware' => 'operario'], function () {
            
                /* Validacion */


                Route::get('validarPeticionVista', 'Validaciones@validarPeticionVista')->name('validarPeticionVista');

                Route::get('peticionesPendientes', 'Validaciones@peticionesPendientes')->name('peticionesPendientes');

                Route::get('peticionesPendientesTodos', 'Validaciones@peticionesPendientesTodos')->name('peticionesPendientesTodos');

                Route::get('validarPeticion','Validaciones@validarPeticion')->name('validarPeticion');

                /* Objeto */

                Route::post('agregarObjeto', 'Objetos@agregarObjeto')->name('agregarObjeto');

                Route::post('agregarTipo', 'Objetos@agregarTipo')->name('agregarTipo');
                Route::post('editarTipo', 'Objetos@editarTipo')->name('editarTipo');
                Route::post('cambiarEstadoTipo', 'Objetos@cambiarEstadoTipo')->name('cambiarEstadoTipo');
                Route::post('datosTipo', 'Objetos@datosTipo')->name('datosTipo');
                Route::post('validarTipo', 'Objetos@validarTipo')->name('validarTipo');

                /* Lugar */

                Route::get('ConsultarLugar','Peticiones@ConsultarLugar')->name('consultarLugar');

                Route::post('ingresarLugar', 'Peticiones@ingresarLugar')->name('ingresarLugar');

                Route::get('consultarLugarId','Peticiones@consultarLugarId')->name('consultarLugarId');

                Route::post('editarLugar','Peticiones@editarLugar')->name('editarLugar');

                Route::get('cambiarEstadoLugar/{IdLugar}/{Estado}','Peticiones@cambiarEstadoLugar')->name('cambiarEstadoLugar');

            });

            /* _-- Director --_  */
            Route::group(['middleware' => 'director'], function () {
                
                /* Autorizacion */

                Route::get('autorizarPeticionVista', 'Autorizaciones@autorizarPeticionVista')->name('autorizarPeticionVista');

                Route::get('peticionesValidadas', 'Autorizaciones@peticionesValidadas')->name('peticionesValidadas');

                Route::get('peticionesValidadasTodos', 'Autorizaciones@peticionesValidadasTodos')->name('peticionesValidadasTodos');

                Route::get('datosAutorizadoPorId', 'Autorizaciones@datosPeticionPorId')->name('datosAutorizadoPorId');

                Route::get('autorizarPeticion','Autorizaciones@autorizarPeticion')->name('autorizarPeticion');

                /* Reportes */

                Route::get('/reportes','Objetos@VistaReportes')->name('reportes');


                /* Estadisticas */

                Route::get('/estadisticas','Objetos@VistaEstadisticas')->name('estadisticas');

            });
        });
    });
});






