-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-11-2020 a las 08:30:01
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `objetosperdidosed2`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE  PROCEDURE `P_AgregarLugar` (IN `_NombreLugar` VARCHAR(30), IN `_Piso` TINYINT, IN `_IdZona` INT)  begin
insert into Lugar (NombreLugar,Piso,IdZona,Estado) values(_NombreLugar,_Piso,_IdZona,1);
end$$

CREATE  PROCEDURE `P_AgregarObjeto` (`_IdentificacionUsuario` INT, `_IdLugar` INT, `_Color` VARCHAR(15), `_Tamano` VARCHAR(20), `_Descripcion` VARCHAR(250), `_Tipo` INT)  begin
insert into Peticion (IdentificacionUsuario, IdLugar) values (_IdentificacionUsuario, _IdLugar);
insert into Objeto (Color, Tamano, Descripcion, Fecha_encuentro, Tipo, IdPeticion, Estado) values (_Color,_Tamano,_Descripcion,now(),_Tipo,last_insert_id(),3);
end$$

CREATE  PROCEDURE `P_AutorizarPeticion` (`_IdentificacionDirector` INT, `_IdPeticionCliente` INT, `_Estado` INT, `_DetalleNegacion` VARCHAR(250), `_IdPeticionObjeto` INT)  begin
if(_Estado = 8) then
update peticion set ObjetoValidado = null ,Estado = 8, IdentificacionDirector = _IdentificacionDirector, FechaEntrega = date_add(now(), interval 30 day), IdentificacionUsuario = (select IdentificacionUsuario from peticion where IdPeticion = _IdPeticionObjeto ), IdLugar = (select IdLugar from peticion where IdPeticion = _IdPeticionObjeto ), DetalleNegacion = _DetalleNegacion where IdPeticion =  _IdPeticionCliente;
delete from objeto where IdPeticion = _IdPeticionCliente;
update objeto set Estado = 5, IdPeticion = _IdPeticionCliente where IdPeticion =  _IdPeticionObjeto;
delete from peticion where IdPeticion = _IdPeticionObjeto;
else 
update peticion set ObjetoValidado = null ,Estado = 9, IdentificacionDirector = _IdentificacionDirector, DetalleNegacion = _DetalleNegacion where IdPeticion = _IdPeticionCliente and Estado = 7;
update objeto set Estado = 3 where IdPeticion = _IdPeticionObjeto;
end if;
end$$

CREATE  PROCEDURE `P_CambiarContraseña` (`_IdentificacionUsuario` INT, `_Clave` VARCHAR(40))  begin
update usuario set Clave = _Clave where IdentificacionUsuario = _IdentificacionUsuario;
end$$

CREATE  PROCEDURE `P_EstadisticasObjetos` (`_Dia` VARCHAR(10), `_Estado` INT)  begin
if(_Estado = 4 || _Estado = 5) then
select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and p.FechaEntrega between concat(_Dia," 00:00:00") and  concat(_Dia," 23:59:59");
else
select count(*) from objeto where Estado = _Estado and Fecha_Encuentro between concat(_Dia," 00:00:00") and  concat(_Dia," 23:59:59");
end if;
end$$

CREATE  PROCEDURE `P_EstadisticasObjetosAnio` (`_Dia` VARCHAR(10), `_Estado` INT)  begin
if(_Estado = 4 || _Estado = 5) then
select 
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =1) as Enero,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =2) as Febrero,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =3) as Marzo,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =4) as Abril,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =5) as Mayo,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =6) as Junio,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =7) as Julio,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =8) as Agosto,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =9) as Septiembre,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =10) as Octubre,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =11) as Noviembre,
(select count(*) from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =12) as Diciembre;
else
select 
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =1) as Enero,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =2) as Febrero,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =3) as Marzo,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =4) as Abril,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =5) as Mayo,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =6) as Junio,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =7) as Julio,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =8) as Agosto,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =9) as Septiembre,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =10) as Octubre,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =11) as Noviembre,
(select count(*) from objeto
where Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =12) as Diciembre;
end if;
end$$

CREATE  PROCEDURE `P_EstadisticasObjetosDia` (`_Dia` VARCHAR(10), `_Estado` INT)  begin
if(_Estado = 4 || _Estado = 5) then
select count(*) as objeto from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
where o.Estado = _Estado and date(p.FechaEntrega) = _Dia;
else
select count(*) as objeto from objeto
where Estado = _Estado and date(Fecha_encuentro) = _Dia;
end if;
end$$

CREATE  PROCEDURE `P_InactivarLugar` (IN `ID` INT)  begin
UPDATE Lugar SET Estado = 2 WHERE IdLugar = ID;
end$$

CREATE  PROCEDURE `P_IniciarSesion` (`_IdentificacionUsuario` INT, `_Clave` VARCHAR(40))  begin
select * from usuario where IdentificacionUsuario = _IdentificacionUsuario and Clave = _Clave and Estado = 1;
end$$

CREATE  PROCEDURE `P_ModificarLugar` (IN `NEWIdLugar` INT, IN `NEWNombreLugar` VARCHAR(30), IN `NEWPiso` TINYINT, IN `NEWIdZona` INT, IN `NEWEstado` INT)  begin
UPDATE Lugar SET NombreLugar = NEWNombreLugar, Piso= NEWPiso, IdZona=NEWIdZona, Estado = NEWEstado
WHERE IdLugar=NEWIdLugar;
end$$

CREATE  PROCEDURE `P_RealizarPeticion` (`_IdLugar` INT, `_Color` VARCHAR(15), `_Tamano` VARCHAR(20), `_Descripcion` VARCHAR(250), `_Tipo` INT, `_Nombres` VARCHAR(40), `_Apellidos` VARCHAR(40), `_Identificacion` INT, `_Telefono` VARCHAR(20), `_Correo` VARCHAR(50), `_Pasaporte` VARCHAR(20))  begin
set @NumDatosCliente = (select count(*) from peticion where IdCliente = (select IdCliente from cliente where Identificacion = _Identificacion and Identificacion <> "" and Identificacion  or Pasaporte = _Pasaporte and Pasaporte <>  "" )  ) ; 
set @petAutorizadasCliente = (select count(*) from peticion where IdCliente = (select IdCliente from cliente where Identificacion = _Identificacion   and Identificacion <>  "" or Pasaporte = _Pasaporte and Pasaporte <>  "")   and estado = 8) ; 
if(@NumDatosCliente > 0) then
	if(@petAutorizadasCliente > 0) then		
		update cliente set Telefono = _Telefono, Correo = _Correo  where Identificacion = _Identificacion and Identificacion != ""  or Pasaporte = _Pasaporte and Pasaporte != "";
    else    
		update cliente set Nombres = _Nombres, Apellidos = _Apellidos, Telefono = _Telefono, Correo = _Correo, Pasaporte = _Pasaporte where Identificacion = _Identificacion and Identificacion != ""  or Pasaporte = _Pasaporte and Pasaporte != "";
	end if;
else
	insert into cliente (Nombres, Apellidos, Identificacion, Telefono, Correo, Pasaporte) values (_Nombres, _Apellidos, _Identificacion, _Telefono, _Correo, _Pasaporte);
end if;	
insert into peticion (Estado, FechaPeticion, IdCliente, IdLugar) values (6, now(), (select IdCliente from cliente where  Identificacion = _Identificacion  and Identificacion <> "" or Pasaporte = _Pasaporte  and Pasaporte <>  ""), _IdLugar);
select last_insert_id() as idpeticion;
insert into objeto (Color, Tamano, Descripcion, Tipo, IdPeticion) values (_Color , _Tamano, _Descripcion , _Tipo, last_insert_id());
end$$

CREATE  PROCEDURE `P_RealizarPeticionAutorizado` (`_IdLugar` INT, `_Color` VARCHAR(15), `_Tamano` VARCHAR(20), `_Descripcion` VARCHAR(250), `_Tipo` INT, `_NombresA` VARCHAR(40), `_ApellidosA` VARCHAR(40), `_TelefonoA` VARCHAR(20), `_IdentificacionA` INT, `_Nombres` VARCHAR(40), `_Apellidos` VARCHAR(40), `_Identificacion` INT, `_Telefono` VARCHAR(20), `_Correo` VARCHAR(50), `_Pasaporte` VARCHAR(20), `_PasaporteA` VARCHAR(20))  begin
set @petAutorizadasCliente = (select count(*) from peticion where IdCliente = (select IdCliente from cliente where Identificacion = _Identificacion  and Identificacion != "" or Pasaporte = _Pasaporte  and Pasaporte != "")   and estado = 8) ; 
set @petAutorizadasAutorizado = (select count(*) from peticion where IdAutorizado = (select IdAutorizado from autorizado where Identificacion = _IdentificacionA  and Identificacion != "" or  Pasaporte = _PasaporteA and Pasaporte != "") and estado = 8 ) ;
set @NumDatosCliente = (select count(*) from peticion where IdCliente = (select IdCliente from cliente where Identificacion = _Identificacion and Identificacion != ""  or Pasaporte = _Pasaporte and Pasaporte != "")  ) ; 
set @NumDatosAutorizado = (select count(*) from peticion where IdAutorizado = (select IdAutorizado from autorizado where Identificacion = _IdentificacionA and Identificacion != ""  or  Pasaporte = _PasaporteA and Pasaporte != "") ) ;
if(@NumDatosCliente > 0) then
	if(@petAutorizadasCliente > 0) then		
		update cliente set Telefono = _Telefono, Correo = _Correo  where Identificacion = _Identificacion and Identificacion != ""  or Pasaporte = _Pasaporte and Pasaporte != "";
    else    
		update cliente set Nombres = _Nombres, Apellidos = _Apellidos, Telefono = _Telefono, Correo = _Correo, Pasaporte = _Pasaporte where Identificacion = _Identificacion and Identificacion != ""  or Pasaporte = _Pasaporte and Pasaporte != "";
	end if;
else
	insert into cliente (Nombres, Apellidos, Identificacion, Telefono, Correo, Pasaporte) values (_Nombres, _Apellidos, _Identificacion, _Telefono, _Correo, _Pasaporte);
end if;	
if(@NumDatosAutorizado > 0) then
	if(@petAutorizadasAutorizado > 0) then	
		update autorizado set Telefono = _TelefonoA where Identificacion = _IdentificacionA and Identificacion != ""  or Pasaporte = _PasaporteA and Pasaporte != "";
    else
		update autorizado set Nombres = _NombresA, Apellidos = _ApellidosA, Telefono = _TelefonoA   where Identificacion = _IdentificacionA and Identificacion != ""  or Pasaporte = _PasaporteA and Pasaporte != "";
	end if;
else
	insert into autorizado (Nombres,Apellidos,Telefono,Identificacion,Pasaporte) values (_NombresA, _ApellidosA, _TelefonoA, _IdentificacionA, _PasaporteA);
end if;
insert into peticion (Estado, FechaPeticion, IdCliente, IdLugar, IdAutorizado) values (6, now(),(select IdCliente from cliente where  Identificacion = _Identificacion and Identificacion != "" or Pasaporte = _Pasaporte  and Pasaporte != "" ), _IdLugar, (select IdAutorizado from autorizado where Identificacion = _IdentificacionA and Identificacion != "" or  Pasaporte = _PasaporteA  and Pasaporte != ""));
select last_insert_id() as idpeticion;
insert into objeto (Color, Tamano, Descripcion, Tipo, IdPeticion) values (_Color , _Tamano, _Descripcion , _Tipo, last_insert_id());
end$$

CREATE  PROCEDURE `P_ReportesObjetos` (`_Dia` VARCHAR(10), `_Estado` INT)  begin
if(_Estado = 4 || _Estado = 5) then
select o.IdObjeto, o.Color, o.Tamano, o.Descripcion, o.Fecha_encuentro, t.Tipo, o.Estado, p.Fechaentrega from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
inner join tipo t on t.IdTipo = o.Tipo
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia) and month(p.FechaEntrega) =month(_Dia);
else
select *,t.Tipo from objeto o
inner join tipo t on t.IdTipo = o.Tipo
where O.Estado = _Estado and year(Fecha_Encuentro) = year(_Dia) and month(Fecha_Encuentro) =month(_Dia);

end if;
end$$

CREATE  PROCEDURE `P_ReportesObjetosAnio` (`_Dia` VARCHAR(10), `_Estado` INT)  begin
if(_Estado = 4 || _Estado = 5) then
select o.IdObjeto, o.Color, o.Tamano, o.Descripcion, o.Fecha_encuentro, t.Tipo, o.Estado, p.Fechaentrega from peticion p
inner join objeto o on p.IdPeticion = o.IdPeticion
inner join tipo t on t.IdTipo = o.Tipo
where o.Estado = _Estado and year(p.FechaEntrega) = year(_Dia);
else
select *,t.Tipo from objeto o
inner join tipo t on t.IdTipo = o.Tipo
where O.Estado = _Estado and year(Fecha_Encuentro) = year(_Dia);
end if;
end$$

CREATE  PROCEDURE `P_ValidarPeticion` (`_IdentificacionOperario` INT, `_IdPeticion` INT, `_Estado` INT, `_DetalleNegacion` VARCHAR(250), `_IdPeticionObjeto` INT)  begin
update peticion set Estado = _Estado ,IdentificacionOperario = _IdentificacionOperario, DetalleNegacion = _DetalleNegacion where IdPeticion = _IdPeticion;
if (_Estado = 7) then
update objeto set Estado = 7 where IdPeticion = _IdPeticionObjeto;
update peticion set ObjetoValidado = _IdPeticionObjeto where IdPeticion = _IdPeticion;
end if;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autorizado`
--

CREATE TABLE `autorizado` (
  `IdAutorizado` int(11) NOT NULL,
  `Identificacion` int(11) DEFAULT NULL,
  `Pasaporte` varchar(20) DEFAULT NULL,
  `Nombres` varchar(40) NOT NULL,
  `Apellidos` varchar(40) NOT NULL,
  `Telefono` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `autorizado`
--

INSERT INTO `autorizado` (`IdAutorizado`, `Identificacion`, `Pasaporte`, `Nombres`, `Apellidos`, `Telefono`) VALUES
(3, 1000579646, '', 'Brayan', 'Ayala', '3013213213'),
(4, NULL, 'ABC123', 'Bernardo', 'Martinez', '213123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `IdCliente` int(11) NOT NULL,
  `Identificacion` int(11) DEFAULT NULL,
  `Nombres` varchar(40) NOT NULL,
  `Apellidos` varchar(40) NOT NULL,
  `Pasaporte` varchar(20) DEFAULT NULL,
  `Telefono` varchar(20) NOT NULL,
  `Correo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`IdCliente`, `Identificacion`, `Nombres`, `Apellidos`, `Pasaporte`, `Telefono`, `Correo`) VALUES
(21, 123123, 'Brayan', 'Ayala', '', '312312', 'dekrions@gmail.com'),
(22, 1000579646, 'Brayan', 'Ayala', '', '3014213213', 'dekrions@gmail.com'),
(23, NULL, 'Laura Valentina', 'Rodriguez Mira', 'ABC123', '213131', 'dekrions@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `IdEstado` int(11) NOT NULL,
  `Estado` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`IdEstado`, `Estado`) VALUES
(1, 'Activo'),
(2, 'Inactivo'),
(3, 'Perdido'),
(4, 'Desechado'),
(5, 'Entregado'),
(6, 'Pendiente'),
(7, 'Validado'),
(8, 'Autorizado'),
(9, 'Denegado'),
(12, 'Nuevo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar`
--

CREATE TABLE `lugar` (
  `IdLugar` int(11) NOT NULL,
  `NombreLugar` varchar(30) NOT NULL,
  `Piso` tinyint(4) NOT NULL,
  `Estado` int(11) NOT NULL,
  `IdZona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lugar`
--

INSERT INTO `lugar` (`IdLugar`, `NombreLugar`, `Piso`, `Estado`, `IdZona`) VALUES
(1, 'Mac Donals', 1, 1, 1),
(2, 'Burger King', 2, 2, 2),
(3, 'Oxo', 1, 1, 1),
(4, 'Juan Valdes', 2, 1, 2),
(5, 'Pink', 2, 2, 2),
(6, 'Cafe Tacuba', 1, 1, 2),
(7, 'Raimans', 1, 2, 1),
(8, 'Zeo', 1, 1, 1),
(9, 'Mac Donals', 2, 1, 1),
(10, 'Lili Pink', 1, 1, 1),
(11, 'Koaj', 2, 1, 2),
(12, 'Mac Donals', 1, 1, 2),
(13, 'Zeo', 1, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `objeto`
--

CREATE TABLE `objeto` (
  `IdObjeto` int(11) NOT NULL,
  `Color` varchar(15) NOT NULL,
  `Tamano` varchar(20) NOT NULL,
  `Descripcion` varchar(250) NOT NULL,
  `Fecha_encuentro` datetime DEFAULT NULL,
  `Tipo` int(11) NOT NULL,
  `Estado` int(11) DEFAULT NULL,
  `IdPeticion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `objeto`
--

INSERT INTO `objeto` (`IdObjeto`, `Color`, `Tamano`, `Descripcion`, `Fecha_encuentro`, `Tipo`, `Estado`, `IdPeticion`) VALUES
(111, 'Amarillo', 'Mediano', 'perfect', '2020-11-13 21:25:46', 4, 5, 110),
(112, 'Amarillo', 'Pequeño', 'Tenia un sitcker rojo', '2020-11-14 23:38:04', 2, 5, 113),
(114, 'Amarillo', 'Grande', 'SDSADASD', NULL, 3, NULL, 114),
(115, 'Azul', 'Mediano', 'asdasd', '2020-11-15 02:10:16', 3, 3, 115),
(116, 'Amarillo', 'Grande', 'sadasd', NULL, 2, NULL, 116),
(117, 'Amarillo', 'Mediano', 'sadas', NULL, 3, NULL, 117);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peticion`
--

CREATE TABLE `peticion` (
  `IdPeticion` int(11) NOT NULL,
  `Estado` int(11) DEFAULT NULL,
  `DetalleNegacion` varchar(250) DEFAULT NULL,
  `FechaPeticion` datetime DEFAULT NULL,
  `FechaEntrega` datetime DEFAULT NULL,
  `IdentificacionOperario` int(11) DEFAULT NULL,
  `IdentificacionDirector` int(11) DEFAULT NULL,
  `IdCliente` int(11) DEFAULT NULL,
  `IdAutorizado` int(11) DEFAULT NULL,
  `idLugar` int(11) NOT NULL,
  `IdentificacionUsuario` int(11) DEFAULT NULL,
  `ObjetoValidado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `peticion`
--

INSERT INTO `peticion` (`IdPeticion`, `Estado`, `DetalleNegacion`, `FechaPeticion`, `FechaEntrega`, `IdentificacionOperario`, `IdentificacionDirector`, `IdCliente`, `IdAutorizado`, `idLugar`, `IdentificacionUsuario`, `ObjetoValidado`) VALUES
(110, 8, '¡Su peticion ha sido autorizado con exito!', '2020-11-13 21:25:08', '2020-12-13 21:27:31', 1000199999, 79743645, 21, NULL, 9, 1000579646, NULL),
(113, 8, '¡Su peticion ha sido autorizado con exito!', '2020-11-15 02:08:21', '2020-12-15 02:22:24', 1000199999, 79743645, 22, 3, 1, 1000579646, NULL),
(114, 9, 'No coinciden con nuestros objetos', '2020-11-15 02:09:23', NULL, 1000199999, NULL, 23, 4, 4, NULL, NULL),
(115, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1000579646, NULL),
(116, 6, NULL, '2020-11-15 02:23:56', NULL, NULL, NULL, 22, 3, 1, NULL, NULL),
(117, 6, NULL, '2020-11-15 02:24:42', NULL, NULL, NULL, 23, 4, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `IdTipo` int(11) NOT NULL,
  `Tipo` varchar(30) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`IdTipo`, `Tipo`, `Estado`) VALUES
(1, 'Portatil', 2),
(2, 'Celular', 1),
(3, 'Maleta', 1),
(4, 'Documentos', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `IdentificacionUsuario` int(11) NOT NULL,
  `Nombres` varchar(40) NOT NULL,
  `Apellidos` varchar(40) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `Clave` varchar(255) DEFAULT NULL,
  `Rol` varchar(10) NOT NULL,
  `Estado` int(11) NOT NULL,
  `Clave_Codigo` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`IdentificacionUsuario`, `Nombres`, `Apellidos`, `Correo`, `Telefono`, `Clave`, `Rol`, `Estado`, `Clave_Codigo`) VALUES
(79743645, 'Jose', 'Menmdez', 'josemendez@gmail.com', '3214306585', '$2y$10$sSAHuXLhcn.ALqGePcpDleIP25Xb2A/nlzY1s8nGvwkGyIdSCpJ6m', 'director', 1, NULL),
(1000579646, 'Brayan', 'Martinez Ayala', 'dekrions@gmail.com', '30143095463', '$2y$10$YFACuwGEfYNB9aS/Os32Q.uvN7We8A0sXgb6t5ZFif3xTXKZZykU.', 'operario', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_datospeticion`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_datospeticion` (
`idPeticion` int(11)
,`idObjeto` int(11)
,`FechaPeticion` datetime
,`Color` varchar(15)
,`Tamano` varchar(20)
,`Descripcion` varchar(250)
,`Fecha_encuentro` datetime
,`Tipo` varchar(30)
,`NombreLugar` varchar(30)
,`Piso` tinyint(4)
,`Zona` varchar(30)
,`EstadoPeticion` varchar(15)
,`DetalleNegacion` varchar(250)
,`FechaEntrega` datetime
,`IdentificacionOperario` int(11)
,`IdentificacionDirector` int(11)
,`Nombres` varchar(40)
,`Apellidos` varchar(40)
,`Identificacion` int(11)
,`Pasaporte` varchar(20)
,`Telefono` varchar(20)
,`Correo` varchar(50)
,`nombresautorizado` varchar(40)
,`apellidosautorizado` varchar(40)
,`telefonoautorizado` varchar(20)
,`identificacionautorizado` int(11)
,`pasaporteautorizado` varchar(20)
,`objetovalidado` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_lugares`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_lugares` (
`IdLugar` int(11)
,`NombreLugar` varchar(30)
,`Piso` tinyint(4)
,`Zona` varchar(30)
,`IdZona` int(11)
,`Estado` varchar(15)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_objetos`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_objetos` (
`FechaPeticion` datetime
,`IdentificacionUsuario` int(11)
,`Estado` varchar(15)
,`Color` varchar(15)
,`Tamano` varchar(20)
,`Descripcion` varchar(250)
,`Fecha_encuentro` datetime
,`Tipo` varchar(30)
,`NombreLugar` varchar(30)
,`Piso` tinyint(4)
,`Zona` varchar(30)
,`IdPeticion` int(11)
,`IdObjeto` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_objetosdesechados`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_objetosdesechados` (
`FechaPeticion` datetime
,`IdentificacionUsuario` int(11)
,`Estado` varchar(15)
,`Color` varchar(15)
,`Tamano` varchar(20)
,`Descripcion` varchar(250)
,`Fecha_encuentro` datetime
,`Tipo` varchar(30)
,`NombreLugar` varchar(30)
,`Piso` tinyint(4)
,`Zona` varchar(30)
,`FechaEntrega` datetime
,`IdPeticion` int(11)
,`IdObjeto` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_objetosentregados`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_objetosentregados` (
`FechaPeticion` datetime
,`IdentificacionUsuario` int(11)
,`Estado` varchar(15)
,`Color` varchar(15)
,`Tamano` varchar(20)
,`Descripcion` varchar(250)
,`Fecha_encuentro` datetime
,`Tipo` varchar(30)
,`NombreLugar` varchar(30)
,`Piso` tinyint(4)
,`Zona` varchar(30)
,`FechaEntrega` datetime
,`IdPeticion` int(11)
,`IdObjeto` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_objetosperdidos`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_objetosperdidos` (
`FechaPeticion` datetime
,`IdentificacionUsuario` int(11)
,`Estado` varchar(15)
,`Color` varchar(15)
,`Tamano` varchar(20)
,`Descripcion` varchar(250)
,`Fecha_encuentro` datetime
,`Tipo` varchar(30)
,`NombreLugar` varchar(30)
,`Piso` tinyint(4)
,`Zona` varchar(30)
,`IdPeticion` int(11)
,`IdObjeto` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_objetosvalidados`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_objetosvalidados` (
`FechaPeticion` datetime
,`IdentificacionUsuario` int(11)
,`Estado` varchar(15)
,`Color` varchar(15)
,`Tamano` varchar(20)
,`Descripcion` varchar(250)
,`Fecha_encuentro` datetime
,`Tipo` varchar(30)
,`NombreLugar` varchar(30)
,`Piso` tinyint(4)
,`Zona` varchar(30)
,`FechaEntrega` datetime
,`IdPeticion` int(11)
,`IdObjeto` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_peticionesaprobadas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_peticionesaprobadas` (
`objeto` varchar(30)
,`color` varchar(15)
,`Tamano` varchar(20)
,`descripcion` varchar(250)
,`fecha_encuentro` datetime
,`idpeticion` int(11)
,`fechapeticion` datetime
,`idcliente` int(11)
,`idautorizado` int(11)
,`identificacion` int(11)
,`pasaporte` varchar(20)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_peticionesdenegadas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_peticionesdenegadas` (
`objeto` varchar(30)
,`color` varchar(15)
,`Tamano` varchar(20)
,`descripcion` varchar(250)
,`fecha_encuentro` datetime
,`idpeticion` int(11)
,`fechapeticion` datetime
,`idcliente` int(11)
,`idautorizado` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_peticionespendientes`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_peticionespendientes` (
`objeto` varchar(30)
,`color` varchar(15)
,`Tamano` varchar(20)
,`descripcion` varchar(250)
,`fecha_encuentro` datetime
,`fechapeticion` datetime
,`idpeticion` int(11)
,`idcliente` int(11)
,`idautorizado` int(11)
,`identificacion` int(11)
,`pasaporte` varchar(20)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_peticionestado`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_peticionestado` (
`fechapeticion` datetime
,`idpeticion` int(11)
,`estado` varchar(15)
,`detallenegacion` varchar(250)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_peticionesvalidadas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_peticionesvalidadas` (
`objeto` varchar(30)
,`color` varchar(15)
,`Tamano` varchar(20)
,`descripcion` varchar(250)
,`fecha_encuentro` datetime
,`fechapeticion` datetime
,`idpeticion` int(11)
,`idcliente` int(11)
,`idautorizado` int(11)
,`identificacion` int(11)
,`pasaporte` varchar(20)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zona`
--

CREATE TABLE `zona` (
  `IdZona` int(11) NOT NULL,
  `Zona` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `zona`
--

INSERT INTO `zona` (`IdZona`, `Zona`) VALUES
(1, 'Nacional'),
(2, 'Internacional');

-- --------------------------------------------------------

--
-- Estructura para la vista `v_datospeticion`
--
DROP TABLE IF EXISTS `v_datospeticion`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_datospeticion`  AS  select `o`.`IdPeticion` AS `idPeticion`,`o`.`IdObjeto` AS `idObjeto`,`p`.`FechaPeticion` AS `FechaPeticion`,`o`.`Color` AS `Color`,`o`.`Tamano` AS `Tamano`,`o`.`Descripcion` AS `Descripcion`,`o`.`Fecha_encuentro` AS `Fecha_encuentro`,`t`.`Tipo` AS `Tipo`,`l`.`NombreLugar` AS `NombreLugar`,`l`.`Piso` AS `Piso`,`z`.`Zona` AS `Zona`,`e`.`Estado` AS `EstadoPeticion`,`p`.`DetalleNegacion` AS `DetalleNegacion`,`p`.`FechaEntrega` AS `FechaEntrega`,`p`.`IdentificacionOperario` AS `IdentificacionOperario`,`p`.`IdentificacionDirector` AS `IdentificacionDirector`,`c`.`Nombres` AS `Nombres`,`c`.`Apellidos` AS `Apellidos`,`c`.`Identificacion` AS `Identificacion`,`c`.`Pasaporte` AS `Pasaporte`,`c`.`Telefono` AS `Telefono`,`c`.`Correo` AS `Correo`,`a`.`Nombres` AS `nombresautorizado`,`a`.`Apellidos` AS `apellidosautorizado`,`a`.`Telefono` AS `telefonoautorizado`,`a`.`Identificacion` AS `identificacionautorizado`,`a`.`Pasaporte` AS `pasaporteautorizado`,`p`.`ObjetoValidado` AS `objetovalidado` from (((((((`peticion` `p` join `objeto` `o` on(`p`.`IdPeticion` = `o`.`IdPeticion`)) join `tipo` `t` on(`t`.`IdTipo` = `o`.`Tipo`)) join `lugar` `l` on(`p`.`idLugar` = `l`.`IdLugar`)) join `zona` `z` on(`l`.`IdZona` = `z`.`IdZona`)) join `cliente` `c` on(`c`.`IdCliente` = `p`.`IdCliente`)) join `estado` `e` on(`e`.`IdEstado` = `p`.`Estado`)) left join `autorizado` `a` on(`a`.`IdAutorizado` = `p`.`IdAutorizado`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_lugares`
--
DROP TABLE IF EXISTS `v_lugares`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_lugares`  AS  select `l`.`IdLugar` AS `IdLugar`,`l`.`NombreLugar` AS `NombreLugar`,`l`.`Piso` AS `Piso`,`z`.`Zona` AS `Zona`,`z`.`IdZona` AS `IdZona`,`e`.`Estado` AS `Estado` from ((`lugar` `l` join `zona` `z` on(`l`.`IdZona` = `z`.`IdZona`)) join `estado` `e` on(`l`.`Estado` = `e`.`IdEstado`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_objetos`
--
DROP TABLE IF EXISTS `v_objetos`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_objetos`  AS  select `p`.`FechaPeticion` AS `FechaPeticion`,`p`.`IdentificacionUsuario` AS `IdentificacionUsuario`,`e`.`Estado` AS `Estado`,`o`.`Color` AS `Color`,`o`.`Tamano` AS `Tamano`,`o`.`Descripcion` AS `Descripcion`,`o`.`Fecha_encuentro` AS `Fecha_encuentro`,`t`.`Tipo` AS `Tipo`,`l`.`NombreLugar` AS `NombreLugar`,`l`.`Piso` AS `Piso`,`z`.`Zona` AS `Zona`,`o`.`IdPeticion` AS `IdPeticion`,`o`.`IdObjeto` AS `IdObjeto` from (((((`peticion` `p` join `objeto` `o` on(`p`.`IdPeticion` = `o`.`IdPeticion`)) join `tipo` `t` on(`t`.`IdTipo` = `o`.`Tipo`)) join `lugar` `l` on(`p`.`idLugar` = `l`.`IdLugar`)) join `estado` `e` on(`e`.`IdEstado` = `o`.`Estado`)) join `zona` `z` on(`l`.`IdZona` = `z`.`IdZona`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_objetosdesechados`
--
DROP TABLE IF EXISTS `v_objetosdesechados`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_objetosdesechados`  AS  select `p`.`FechaPeticion` AS `FechaPeticion`,`p`.`IdentificacionUsuario` AS `IdentificacionUsuario`,`e`.`Estado` AS `Estado`,`o`.`Color` AS `Color`,`o`.`Tamano` AS `Tamano`,`o`.`Descripcion` AS `Descripcion`,`o`.`Fecha_encuentro` AS `Fecha_encuentro`,`t`.`Tipo` AS `Tipo`,`l`.`NombreLugar` AS `NombreLugar`,`l`.`Piso` AS `Piso`,`z`.`Zona` AS `Zona`,`p`.`FechaEntrega` AS `FechaEntrega`,`o`.`IdPeticion` AS `IdPeticion`,`o`.`IdObjeto` AS `IdObjeto` from (((((`peticion` `p` join `objeto` `o` on(`p`.`IdPeticion` = `o`.`IdPeticion`)) join `tipo` `t` on(`t`.`IdTipo` = `o`.`Tipo`)) join `lugar` `l` on(`p`.`idLugar` = `l`.`IdLugar`)) join `estado` `e` on(`e`.`IdEstado` = `o`.`Estado`)) join `zona` `z` on(`l`.`IdZona` = `z`.`IdZona`)) where `e`.`Estado` = 'Desechado' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_objetosentregados`
--
DROP TABLE IF EXISTS `v_objetosentregados`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_objetosentregados`  AS  select `p`.`FechaPeticion` AS `FechaPeticion`,`p`.`IdentificacionUsuario` AS `IdentificacionUsuario`,`e`.`Estado` AS `Estado`,`o`.`Color` AS `Color`,`o`.`Tamano` AS `Tamano`,`o`.`Descripcion` AS `Descripcion`,`o`.`Fecha_encuentro` AS `Fecha_encuentro`,`t`.`Tipo` AS `Tipo`,`l`.`NombreLugar` AS `NombreLugar`,`l`.`Piso` AS `Piso`,`z`.`Zona` AS `Zona`,`p`.`FechaEntrega` AS `FechaEntrega`,`o`.`IdPeticion` AS `IdPeticion`,`o`.`IdObjeto` AS `IdObjeto` from (((((`peticion` `p` join `objeto` `o` on(`p`.`IdPeticion` = `o`.`IdPeticion`)) join `tipo` `t` on(`t`.`IdTipo` = `o`.`Tipo`)) join `lugar` `l` on(`p`.`idLugar` = `l`.`IdLugar`)) join `estado` `e` on(`e`.`IdEstado` = `o`.`Estado`)) join `zona` `z` on(`l`.`IdZona` = `z`.`IdZona`)) where `e`.`Estado` = 'Entregado' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_objetosperdidos`
--
DROP TABLE IF EXISTS `v_objetosperdidos`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_objetosperdidos`  AS  select `p`.`FechaPeticion` AS `FechaPeticion`,`p`.`IdentificacionUsuario` AS `IdentificacionUsuario`,`e`.`Estado` AS `Estado`,`o`.`Color` AS `Color`,`o`.`Tamano` AS `Tamano`,`o`.`Descripcion` AS `Descripcion`,`o`.`Fecha_encuentro` AS `Fecha_encuentro`,`t`.`Tipo` AS `Tipo`,`l`.`NombreLugar` AS `NombreLugar`,`l`.`Piso` AS `Piso`,`z`.`Zona` AS `Zona`,`o`.`IdPeticion` AS `IdPeticion`,`o`.`IdObjeto` AS `IdObjeto` from (((((`peticion` `p` join `objeto` `o` on(`p`.`IdPeticion` = `o`.`IdPeticion`)) join `tipo` `t` on(`t`.`IdTipo` = `o`.`Tipo`)) join `lugar` `l` on(`p`.`idLugar` = `l`.`IdLugar`)) join `estado` `e` on(`e`.`IdEstado` = `o`.`Estado`)) join `zona` `z` on(`l`.`IdZona` = `z`.`IdZona`)) where `e`.`Estado` = 'Perdido' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_objetosvalidados`
--
DROP TABLE IF EXISTS `v_objetosvalidados`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_objetosvalidados`  AS  select `p`.`FechaPeticion` AS `FechaPeticion`,`p`.`IdentificacionUsuario` AS `IdentificacionUsuario`,`e`.`Estado` AS `Estado`,`o`.`Color` AS `Color`,`o`.`Tamano` AS `Tamano`,`o`.`Descripcion` AS `Descripcion`,`o`.`Fecha_encuentro` AS `Fecha_encuentro`,`t`.`Tipo` AS `Tipo`,`l`.`NombreLugar` AS `NombreLugar`,`l`.`Piso` AS `Piso`,`z`.`Zona` AS `Zona`,`p`.`FechaEntrega` AS `FechaEntrega`,`o`.`IdPeticion` AS `IdPeticion`,`o`.`IdObjeto` AS `IdObjeto` from (((((`peticion` `p` join `objeto` `o` on(`p`.`IdPeticion` = `o`.`IdPeticion`)) join `tipo` `t` on(`t`.`IdTipo` = `o`.`Tipo`)) join `lugar` `l` on(`p`.`idLugar` = `l`.`IdLugar`)) join `estado` `e` on(`e`.`IdEstado` = `o`.`Estado`)) join `zona` `z` on(`l`.`IdZona` = `z`.`IdZona`)) where `e`.`Estado` = 'Validado' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_peticionesaprobadas`
--
DROP TABLE IF EXISTS `v_peticionesaprobadas`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_peticionesaprobadas`  AS  select `t`.`Tipo` AS `objeto`,`o`.`Color` AS `color`,`o`.`Tamano` AS `Tamano`,`o`.`Descripcion` AS `descripcion`,`o`.`Fecha_encuentro` AS `fecha_encuentro`,`p`.`IdPeticion` AS `idpeticion`,`p`.`FechaPeticion` AS `fechapeticion`,`p`.`IdCliente` AS `idcliente`,`p`.`IdAutorizado` AS `idautorizado`,`c`.`Identificacion` AS `identificacion`,`c`.`Pasaporte` AS `pasaporte` from (((`peticion` `p` join `objeto` `o` on(`p`.`IdPeticion` = `o`.`IdPeticion`)) join `tipo` `t` on(`t`.`IdTipo` = `o`.`Tipo`)) join `cliente` `c` on(`c`.`IdCliente` = `p`.`IdCliente`)) where `p`.`Estado` = 8 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_peticionesdenegadas`
--
DROP TABLE IF EXISTS `v_peticionesdenegadas`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_peticionesdenegadas`  AS  select `t`.`Tipo` AS `objeto`,`o`.`Color` AS `color`,`o`.`Tamano` AS `Tamano`,`o`.`Descripcion` AS `descripcion`,`o`.`Fecha_encuentro` AS `fecha_encuentro`,`p`.`IdPeticion` AS `idpeticion`,`p`.`FechaPeticion` AS `fechapeticion`,`p`.`IdCliente` AS `idcliente`,`p`.`IdAutorizado` AS `idautorizado` from ((`peticion` `p` join `objeto` `o` on(`p`.`IdPeticion` = `o`.`IdPeticion`)) join `tipo` `t` on(`t`.`IdTipo` = `o`.`Tipo`)) where `p`.`Estado` = 9 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_peticionespendientes`
--
DROP TABLE IF EXISTS `v_peticionespendientes`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_peticionespendientes`  AS  select `t`.`Tipo` AS `objeto`,`o`.`Color` AS `color`,`o`.`Tamano` AS `Tamano`,`o`.`Descripcion` AS `descripcion`,`o`.`Fecha_encuentro` AS `fecha_encuentro`,`p`.`FechaPeticion` AS `fechapeticion`,`p`.`IdPeticion` AS `idpeticion`,`p`.`IdCliente` AS `idcliente`,`p`.`IdAutorizado` AS `idautorizado`,`c`.`Identificacion` AS `identificacion`,`c`.`Pasaporte` AS `pasaporte` from (((`peticion` `p` join `objeto` `o` on(`p`.`IdPeticion` = `o`.`IdPeticion`)) join `tipo` `t` on(`t`.`IdTipo` = `o`.`Tipo`)) join `cliente` `c` on(`c`.`IdCliente` = `p`.`IdCliente`)) where `p`.`Estado` = 6 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_peticionestado`
--
DROP TABLE IF EXISTS `v_peticionestado`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_peticionestado`  AS  select `p`.`FechaPeticion` AS `fechapeticion`,`p`.`IdPeticion` AS `idpeticion`,`e`.`Estado` AS `estado`,`p`.`DetalleNegacion` AS `detallenegacion` from (`peticion` `p` join `estado` `e` on(`e`.`IdEstado` = `p`.`Estado`)) where `p`.`Estado` = 6 or `p`.`Estado` = 7 or `p`.`Estado` = 8 or `p`.`Estado` = 9 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_peticionesvalidadas`
--
DROP TABLE IF EXISTS `v_peticionesvalidadas`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_peticionesvalidadas`  AS  select `t`.`Tipo` AS `objeto`,`o`.`Color` AS `color`,`o`.`Tamano` AS `Tamano`,`o`.`Descripcion` AS `descripcion`,`o`.`Fecha_encuentro` AS `fecha_encuentro`,`p`.`FechaPeticion` AS `fechapeticion`,`p`.`IdPeticion` AS `idpeticion`,`p`.`IdCliente` AS `idcliente`,`p`.`IdAutorizado` AS `idautorizado`,`c`.`Identificacion` AS `identificacion`,`c`.`Pasaporte` AS `pasaporte` from (((`peticion` `p` join `objeto` `o` on(`p`.`IdPeticion` = `o`.`IdPeticion`)) join `tipo` `t` on(`t`.`IdTipo` = `o`.`Tipo`)) join `cliente` `c` on(`c`.`IdCliente` = `p`.`IdCliente`)) where `p`.`Estado` = 7 ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `autorizado`
--
ALTER TABLE `autorizado`
  ADD PRIMARY KEY (`IdAutorizado`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`IdCliente`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`IdEstado`);

--
-- Indices de la tabla `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`IdLugar`),
  ADD KEY `FK_Lugar_Zona` (`IdZona`),
  ADD KEY `FK_Lugar_Estado` (`Estado`);

--
-- Indices de la tabla `objeto`
--
ALTER TABLE `objeto`
  ADD PRIMARY KEY (`IdObjeto`),
  ADD KEY `FK_Objeto_Peticion` (`IdPeticion`),
  ADD KEY `FK_Objeto_Estado` (`Estado`),
  ADD KEY `FK_Objeto_Tipo` (`Tipo`);

--
-- Indices de la tabla `peticion`
--
ALTER TABLE `peticion`
  ADD PRIMARY KEY (`IdPeticion`),
  ADD KEY `FK_Peticion_Usuario` (`IdentificacionUsuario`),
  ADD KEY `FK_Peticion_Cliente` (`IdCliente`),
  ADD KEY `FK_Peticion_Lugar` (`idLugar`),
  ADD KEY `FK_Peticion_Estado` (`Estado`),
  ADD KEY `FK_Peticion_Autorizado` (`IdAutorizado`),
  ADD KEY `FK_ObjetoSolicitado_ObjetoValidado` (`ObjetoValidado`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`IdTipo`),
  ADD UNIQUE KEY `Tipo` (`Tipo`),
  ADD KEY `FK_Tipo_Estado` (`Estado`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`IdentificacionUsuario`),
  ADD UNIQUE KEY `Correo` (`Correo`);

--
-- Indices de la tabla `zona`
--
ALTER TABLE `zona`
  ADD PRIMARY KEY (`IdZona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `autorizado`
--
ALTER TABLE `autorizado`
  MODIFY `IdAutorizado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `IdCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `IdEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `lugar`
--
ALTER TABLE `lugar`
  MODIFY `IdLugar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `objeto`
--
ALTER TABLE `objeto`
  MODIFY `IdObjeto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT de la tabla `peticion`
--
ALTER TABLE `peticion`
  MODIFY `IdPeticion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT de la tabla `tipo`
--
ALTER TABLE `tipo`
  MODIFY `IdTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `zona`
--
ALTER TABLE `zona`
  MODIFY `IdZona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `lugar`
--
ALTER TABLE `lugar`
  ADD CONSTRAINT `FK_Lugar_Estado` FOREIGN KEY (`Estado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `FK_Lugar_Zona` FOREIGN KEY (`IdZona`) REFERENCES `zona` (`IdZona`);

--
-- Filtros para la tabla `objeto`
--
ALTER TABLE `objeto`
  ADD CONSTRAINT `FK_Objeto_Estado` FOREIGN KEY (`Estado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `FK_Objeto_Peticion` FOREIGN KEY (`IdPeticion`) REFERENCES `peticion` (`IdPeticion`),
  ADD CONSTRAINT `FK_Objeto_Tipo` FOREIGN KEY (`Tipo`) REFERENCES `tipo` (`IdTipo`);

--
-- Filtros para la tabla `peticion`
--
ALTER TABLE `peticion`
  ADD CONSTRAINT `FK_ObjetoSolicitado_ObjetoValidado` FOREIGN KEY (`ObjetoValidado`) REFERENCES `peticion` (`IdPeticion`),
  ADD CONSTRAINT `FK_Peticion_Autorizado` FOREIGN KEY (`IdAutorizado`) REFERENCES `autorizado` (`IdAutorizado`),
  ADD CONSTRAINT `FK_Peticion_Cliente` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`IdCliente`),
  ADD CONSTRAINT `FK_Peticion_Estado` FOREIGN KEY (`Estado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `FK_Peticion_Lugar` FOREIGN KEY (`idLugar`) REFERENCES `lugar` (`IdLugar`),
  ADD CONSTRAINT `FK_Peticion_Usuario` FOREIGN KEY (`IdentificacionUsuario`) REFERENCES `usuario` (`IdentificacionUsuario`);

--
-- Filtros para la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD CONSTRAINT `FK_Tipo_Estado` FOREIGN KEY (`Estado`) REFERENCES `estado` (`IdEstado`);

DELIMITER $$
--
-- Eventos
--
CREATE  EVENT `DesecharObjeto` ON SCHEDULE EVERY 1 DAY STARTS now() ON COMPLETION PRESERVE ENABLE DO begin
update peticion p inner join objeto o on o.idpeticion = p.idpeticion
set p.FechaEntrega = now() , o.Estado = 4
where o.Estado = 3 and now() > o.Fecha_encuentro + interval 60 day;
end$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
