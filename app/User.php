<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\Notification;

class User extends Authenticatable
{

    use Notifiable;

    protected $table = "usuario";
    protected $primaryKey = "IdentificacionUsuario";
    public $timestamps = false;
    
    protected $fillable = [
        'IdentificacionUsuario','Nombres','Apellidos','Correo','Telefono','password','Rol','Estado'
    ];


    protected $email = "Correo";

    
    public function getAuthPassword()
    {
        return $this->Clave;
    }

    public function getEmailForPasswordReset() {
        return $this->Correo;
    }

    // 3
    public function getUserNameForPasswordReset(){
        return $this->Correo;
    }

    // 4


  
}
