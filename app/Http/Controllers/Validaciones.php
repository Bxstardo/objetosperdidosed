<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Objeto;
use App\Lugar;
use App\Zona;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\autorizarPeticion;

class Validaciones extends Controller
{
    public function validarPeticionVista()
    {
        $peticiones = DB::select('select * from v_peticionespendientes');

        return view('peticiones.validarPeticion')->with('peticiones',$peticiones);
    }

    public function peticionesPendientes(Request $request)
    {
        $peticiones = DB::select('select * from v_peticionespendientes where IdPeticion = ? or Pasaporte = ? or Identificacion = ?',[$request->idpeticion,$request->pasaporte,$request->identificacion]);

        return ['success' => true ,'peticiones' => $peticiones];
    }

    public function peticionesPendientesTodos(Request $request)
    {
        $peticiones = DB::select('select * from v_peticionespendientes');

        return ['success' => true ,'peticiones' => $peticiones];
    }

    public function datosPeticionPorId(Request $request)
    {
        $idpeticion = DB::select('select * from v_datospeticion where Idpeticion = ?', [$request->idpeticion]);

        $objeto = DB::select('select * from V_ObjetosPerdidos where Tipo = ?', [$idpeticion[0]->Tipo]);

        return ['success' => true ,'idpeticion' => $idpeticion , 'objeto' => $objeto ];
    }

    public function validarPeticion(Request $request)
    {
        DB::select('call P_ValidarPeticion(?,?,?,?,?)', 
        [
            $request->identificacionOperario,
            $request->idPeticionSolicitud,
            $request->estado,
            $request->detalle,
            $request->idPeticionObjeto
        ]);

        $peticiones = DB::select('select * from v_peticionespendientes');

        if ($request->estado == "9") {
            $datosCliente = DB::select('select * from v_datospeticion where IdPeticion = ?', [$request->idPeticionSolicitud]);
            Mail::to($datosCliente[0]->Correo)
            ->send(new autorizarPeticion($datosCliente,$request)); 
        }
        
        

        return ['success' => true ,'peticiones' => $peticiones];

    }
}
