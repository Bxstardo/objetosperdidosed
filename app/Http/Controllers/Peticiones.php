<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Objeto;
use App\Lugar;
use App\Zona;
use App\Peticion;
use App\Tipo;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\RealizarPeticion;
use Validator;

class Peticiones extends Controller
{
 
    public function realizarPeticion(){
        $header = "";
        $zonas = Zona::all();
        $tipos = Tipo::all()->where('Estado','1');
        return view('peticiones.realizarPeticion', compact('zonas','header','tipos'));
    }

    public function cargarLugares(Request $request){
        $zona = $request -> zona;
        $piso = $request -> piso;

        $lugares = DB::select('select * from v_lugares where IdZona = ? and Piso = ? and Estado="Activo"',[$zona,$piso]);
    

        return ['success' => true ,'lugares' => $lugares];

    }

    public function insertarPeticion(Request $request){

        $tipoIdent = $request -> identificacion;
        $tipoIdentA = $request -> identificacionA;

        if($tipoIdent == "cedula"){
            $identificacion = $request -> cedula;
            $pasaporte = null;
        }
        else{
            $identificacion = null;
            $pasaporte =  $request -> pasaporte;
        }
        
        if($tipoIdentA == "cedula"){
            $identificacionA = $request -> cedulaA;
            $pasaporteA = null;
        }
        else{
            $identificacionA = null;
            $pasaporteA =  $request -> pasaporteA;
        }
        
        $tipoObjeto = Tipo::where('Tipo',$request->tipoObjeto)->first()->IdTipo;

        $autorizado = $request -> autorizado;
        if($autorizado == "si"){
            $idpeticion = DB::select('call P_RealizarPeticionAutorizado(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
            [
                $request->lugar,
                $request->colorObjeto,
                $request->tamanoObjeto,
                $request->descripcion,     
                $tipoObjeto,     
                ucwords(strtolower($request->nombresA)),
                ucwords(strtolower($request->apellidosA)),     
                $request->telefonoA,     
                $identificacionA,
                ucwords(strtolower($request->nombres)),     
                ucwords(strtolower($request->apellidos)),
                $identificacion,
                $request->telefono,     
                $request->correo,
                strtoupper($pasaporte),
                strtoupper($pasaporteA)       
            ]
        );

        }
        else{
            $idpeticion = DB::select('call P_RealizarPeticion(?,?,?,?,?,?,?,?,?,?,?)',
            [
                $request->lugar,
                $request->colorObjeto,
                $request->tamanoObjeto,
                $request->descripcion,     
                $tipoObjeto,     
                ucwords(strtolower($request->nombres)),       
                ucwords(strtolower($request->apellidos)),
                $identificacion,
                $request->telefono,     
                $request->correo,
                strtoupper($pasaporte),
            ]    
        );

    
        }
        
        $data = $request;


        Mail::to($request->correo)
        ->send(new RealizarPeticion($data,$idpeticion)); 

        return ['success' => true, 'idpeticion' => $idpeticion];
    }

    public function consultarEstPeticion(){
        $header = "asd";
        return view('peticiones.consultarEstPeticion',compact('header'));
    }

    public function consultarPeticion(){
        $peticiones = DB::select('select * from v_peticionestado');
        
        return view('peticiones.consultarPeticion',compact('peticiones'));
    }

    public function datosPeticion(Request $request){
        
        $datosPeticion = DB::select('select IdPeticion, FechaPeticion, EstadoPeticion from v_datospeticion where IdPeticion = ? or Pasaporte = ? or Identificacion = ?',
        [
            $request->idpeticion,
            $request->pasaporte,
            $request->cedula
        ]);
        
        return ['success' => true ,'datosPeticion' => $datosPeticion];
    }

    public function detallePeticion(Request $request)
    {
        $idpeticion = DB::select('select DetalleNegacion from v_datospeticion where IdPeticion = ?', [$request->idpeticion]);

        
        return ['success' => true ,'idpeticion' => $idpeticion];
    }


    public function consultarLugar()
    {
        $lugares = DB::select('select*from v_lugares');
        $zonas = DB::select('select*from zona');

        return view('lugares.consultarLugar', compact('lugares','zonas'));
    }
    public function ingresarLugar(Request $request)
    {
        $NombreLugar = ucwords(strtolower($request->NombreLugar));
        $validarLugar = Lugar::all()->where('NombreLugar', $NombreLugar)
                                    ->where('Piso', $request->Piso)
                                    ->where('IdZona',$request->Idzona)->first();

        $zona = Zona::all()->where('IdZona', $request->Idzona)->first();                            
                     
        if($validarLugar != null){
            return redirect('ConsultarLugar')->with('lugar',$validarLugar)
                                             ->with('zona',$zona);
        }                

        $lugares = DB::select("call P_AgregarLugar(?, ?, ?)",
        [
         ucwords(strtolower($request->NombreLugar)),
         $request->Piso,
         $request->Idzona 
       
         ]);       
         return redirect('ConsultarLugar');
    }
    public function consultarLugarId(Request $request){
        $lugares = DB::select('select*from v_lugares where IdLugar=?',
        [
            $request->IdLugar
        ]);
 
        return ['success' => true ,'lugares' => $lugares];
    }
    public function editarLugar(Request $request){
        $NombreLugar = ucwords(strtolower($request->NombreLugar));
        $validarLugar = Lugar::all()->where('NombreLugar', $NombreLugar)
        ->where('Piso', $request->Piso)
        ->where('IdZona',$request->Idzona)->first();

        $zona = Zona::all()->where('IdZona', $request->Idzona)->first();                            
        
        if($validarLugar != null){
            return redirect('ConsultarLugar')->with('lugar',$validarLugar)
                    ->with('zona',$zona);
        }     

        $lugares = DB::select("call P_ModificarLugar(
            ?,?, ?, ?,?)",
        [
            $request->IdLugar,
            ucwords(strtolower($request->NombreLugar)),
            $request->Piso,
            $request->Idzona,
            $request->Estado
          
        ]);      
        return redirect('ConsultarLugar'); 
    }

    public function cambiarEstadoLugar($IdLugar, $Estado){
        $newEstado = 0;
        if ($Estado=="Activo") {
            $newEstado=2;
        } else if($Estado == "Inactivo"){
            $newEstado=1;
        }
        $estado = DB::update('update Lugar set Estado = ? where IdLugar = ?', [$newEstado,$IdLugar]);
        return redirect('ConsultarLugar'); 
    }
    public function peticiones(Request $request)
    {
        $peticiones = DB::select('select * from v_datospeticion where IdPeticion = ? or Pasaporte = ? or Identificacion = ?',[$request->idpeticion,$request->pasaporte,$request->identificacion]);

        return ['success' => true ,'peticiones' => $peticiones];
    }

    public function peticionesTodos(Request $request)
    {
        $peticiones = DB::select('select * from v_datospeticion');

        return ['success' => true ,'peticiones' => $peticiones];
    }
}
