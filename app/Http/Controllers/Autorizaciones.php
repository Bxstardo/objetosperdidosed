<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Objeto;
use App\Lugar;
use App\Zona;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\autorizarPeticion;

class Autorizaciones extends Controller
{
    public function autorizarPeticionVista()
    {
        $peticiones = DB::select('select * from v_peticionesvalidadas');

        return view('peticiones.autorizarPeticion')->with('peticiones',$peticiones);
    }

    public function peticionesValidadas(Request $request)
    {
        $peticiones = DB::select('select * from v_peticionesvalidadas where IdPeticion = ? or Pasaporte = ? or Identificacion = ?',[$request->idpeticion,$request->pasaporte,$request->identificacion]);

        return ['success' => true ,'peticiones' => $peticiones];
    }

    public function peticionesValidadasTodos(Request $request)
    {
        $peticiones = DB::select('select * from v_peticionesvalidadas');

        return ['success' => true ,'peticiones' => $peticiones];
    }

    public function datosPeticionPorId(Request $request)
    {
        $idpeticion = DB::select('select * from v_datospeticion where IdPeticion = ?', [$request->idpeticion]);

        $objeto = DB::select('select * from v_objetosvalidados where IdPeticion = ?', [$idpeticion[0]->objetovalidado]);

        return ['success' => true ,'idpeticion' => $idpeticion , 'objeto' => $objeto ];
    }

    public function autorizarPeticion(Request $request)
    {
        DB::select('call P_AutorizarPeticion(?,?,?,?,?)', 
        [
            $request->identificacionDirector,
            $request->idPeticionSolicitud,
            $request->estado,
            $request->detalle,
            $request->idPeticionObjeto
        ]);

        $peticiones = DB::select('select * from v_peticionesvalidadas');
        $datosCliente = DB::select('select * from v_datospeticion where IdPeticion = ?', [$request->idPeticionSolicitud]);

        Mail::to($datosCliente[0]->Correo)
        ->send(new autorizarPeticion($datosCliente,$request)); 

        return ['success' => true ,'peticiones' => $peticiones];

    }
}
