<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use App\Tipo;
use Validator;

class Objetos extends Controller
{
   
 
    public function agregarObjeto(Request $request)
    {
        $tipo = Tipo::where('Tipo',$request->tipoObjeto)->first()->IdTipo;

        $objetos = DB::select("call P_AgregarObjeto(?, ?, ?, ?, ? ,?)",
        [
            $request->IdentificacionOperario,
            $request->lugar,
            $request->colorObjeto,
            $request->tamanoObjeto,
            $request->descripcion,
            $tipo
        
         ]);       
         return redirect('consultarObjeto');
    }

    public function consultarObjeto($tipo = "perdidos")
    {
        $years = DB::select('select year(fecha_encuentro) as fecha from objeto where year(fecha_encuentro) != "" and estado = 3 or estado = 4 or estado = 5 group by fecha');
        $zonas = DB::select('select*from zona');
        $tipos = Tipo::all();

        if ($tipo == "perdidos") {
            $objetos = DB::select('select*from v_objetosPerdidos');
            $tipoO = "perdidos";
            return view('objetos.consultarObjeto', compact('objetos','zonas','tipoO','years','tipos'));
        }elseif ($tipo == "desechados") {
            $objetos = DB::select('select*from v_objetosDesechados');
            $tipoO = "desechados";
            return view('objetos.consultarObjeto', compact('objetos','zonas','tipoO','years','tipos'));
        }else{
            $objetos = DB::select('select*from v_objetosEntregados');
            $tipoO = "entregados";
            return view('objetos.consultarObjeto', compact('objetos','zonas','tipoO','years','tipos'));
        }
 
    }

    public function vistaReportes(){
        $years = DB::select('select year(fecha_encuentro) as fecha from objeto where year(fecha_encuentro) != "" and estado = 3 or estado = 4 or estado = 5 group by fecha');
        return view('reportes.index', compact('years'));
    }

    public function mes(Request $request){

        $year = $request->year;
        $year = $year."/01/01";
        if ($request->estado == 3) {
            $month = DB::select('select month(fecha_encuentro) as fecha from objeto where year(fecha_encuentro) != ""  and estado = ? and year(fecha_encuentro) = year(?)  group by fecha;'
            ,[
                $request->estado, 
                $year
            ]);
        }else{
            $month = DB::select('select month(p.FechaEntrega) as fecha from objeto o inner join peticion p on o.idpeticion = p.idpeticion where year(p.FechaEntrega) != ""  and o.estado = ? and year(p.FechaEntrega) = year(?)  group by fecha;'
            ,[
                $request->estado, 
                $year
            ]);
    
        }
       
        return ['success' => true ,'month' => $month];
    }

   public function generarReporte(Request $request){
        if ($request->estado == 3){
            $month = $request->mes1;
            $year = $request->year1;
            $estado = "Perdidos";
            if ($request->tipo== "month") {
                $reglas = [
                    "mes1" => ['required'],
                    "year1" => ['required'],
                ];
            }else{
                $reglas = [
                    "year1" => ['required'],
                ];
            }

        }else if($request->estado == 5){
            $month = $request->mes2;
            $year = $request->year2;
            $estado = "Entregados";
            if ($request->tipo== "month") {
                $reglas = [
                    "mes2" => ['required'],
                    "year2" => ['required'],
                ];
            }else{
                $reglas = [
                    "year2" => ['required'],
                ];
            }
        }else{
            $month = $request->mes3;
            $year = $request->year3;
            $estado = "Desechados";
            if ($request->tipo== "month") {
                $reglas = [
                    "mes3" => ['required'],
                    "year3" => ['required'],
                ];
            }else{
                $reglas = [
                    "year3" => ['required'],
                ];
            }
        }
        $validador = Validator::make($request->all(),$reglas);

        if($validador->fails()){
            return back()->withErrors($validador);
        }
            

        $tipo = $request->tipo;
        if ($request->tipo == "month") {
            $fecha = $year."/".$month."/01";
            $objetos = DB::select('call P_ReportesObjetos(?,?)', [$fecha, $request->estado]);
            $fecha = date('Y/m', strtotime($fecha));
        }else if($request->tipo == "year"){
            $fecha = $year."/01/01";
            $objetos = DB::select('call P_ReportesObjetosAnio(?,?)',[$fecha, $request->estado]);
            $fecha = date('Y', strtotime($fecha));
        }
        $pdf = PDF::loadView('reportes.objetosPerdidos',compact('objetos','fecha','tipo','estado'));
      
        return $pdf->download('Reporte_Objetos_'.$estado.'_'.$fecha.'.pdf');
    }

  
    public function generarEstadistica(Request $request){
        if ($request->estado == 3){
            $month = $request->mes1;
            $year = $request->year1;
            $estado = "Perdidos";
            if ($request->tipo== "month") {
                $reglas = [
                    "mes1" => ['required'],
                    "year1" => ['required'],
                ];
            }else{
                $reglas = [
                    "year1" => ['required'],
                ];
            }

        }else if($request->estado == 5){
            $month = $request->mes2;
            $year = $request->year2;
            $estado = "Entregados";
            if ($request->tipo== "month") {
                $reglas = [
                    "mes2" => ['required'],
                    "year2" => ['required'],
                ];
            }else{
                $reglas = [
                    "year2" => ['required'],
                ];
            }
        }else{
            $month = $request->mes3;
            $year = $request->year3;
            $estado = "Desechados";
            if ($request->tipo== "month") {
                $reglas = [
                    "mes3" => ['required'],
                    "year3" => ['required'],
                ];
            }else{
                $reglas = [
                    "year3" => ['required'],
                ];
            }
        }
        $validador = Validator::make($request->all(),$reglas);

        if($validador->fails()){
            return back()->withErrors($validador);
        }
            

        $tipo = $request->tipo;
        if ($request->tipo == "month") {
            $dias = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $objetos = array();
            $objeto = 0;
            $fecha = "";
            for ($i=1; $i < $dias+1 ; $i++) { 
                $fecha = $year."/".$month."/".$i;
                $objeto = DB::select('call P_EstadisticasObjetosDia(?,?)', [$fecha, $request->estado]);
                array_push($objetos, $objeto[0]->objeto);
            }
            $fecha = date('m/Y', strtotime($fecha));
            return ['success' => true ,'month' => $objetos, 'type' => 'month' , 'year' => $fecha, 'estado' =>$estado];
        }else if($request->tipo == "year"){
            $fecha = $year."/01/01";
            $objetos = DB::select('call P_EstadisticasObjetosAnio(?,?)',[$fecha, $request->estado]);
            $fecha = date('Y', strtotime($fecha));
        }

      
        return ['success' => true ,'month' => $objetos, 'type' => 'year', 'year' => $year, 'estado' =>$estado];

    }

    public function vistaEstadisticas(){
        $years = DB::select('select year(fecha_encuentro) as fecha from objeto where year(fecha_encuentro) != "" and estado = 3 or estado = 4 or estado = 5 group by fecha');
        return view('estadisticas.index',compact('years'));
    }

    public function agregarTipo(Request $request){
        $tipo = new Tipo();
        $tipo->Tipo = $request->Tipo;
        $tipo->Estado = 1;
        $tipo->save();
        $tipos = Tipo::all();
        return ['success' => true, 'tipos' => $tipos ];
    }

    public function editarTipo(Request $request){
        $tipo = Tipo::find($request->IdTipo);
        if ($tipo->Tipo == $request->Tipo) {
            $tipos = Tipo::all();
            return ['success' => true, 'tipos' => $tipos, 'validar' => true];
        }

        $validacion = Tipo::where('Tipo',$request->Tipo)->count();
        if ($validacion > 0) {
            return ['validar' => false];
        }

        $tipo->Tipo = $request->Tipo;
        $tipo->save();
        $tipos = Tipo::all();
        return ['success' => true, 'tipos' => $tipos, 'validar' => true ];
    }

    public function cambiarEstadoTipo(Request $request){
        $tipo = Tipo::find($request->IdTipo);
        $tipo->Estado = $request->Estado;
        $tipo->save();
        $tipos = Tipo::all();
        return ['success' => true, 'tipos' => $tipos ];
    }
   
    public function datosTipo(Request $request)
    {
        $tipo = Tipo::find($request->IdTipo);
        return ['success' => true, 'tipo' => $tipo];
    }

    public function validarTipo(Request $request)
    {
        $tipos = Tipo::where('Tipo',$request->Tipo)->count();
        if ($tipos > 0) {
            return ['validar' => false];
        }
        return ['validar' => true];
    }
  
}
