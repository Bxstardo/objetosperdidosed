<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\RecuperarClave;
use App\Mail\ResetearClave;
use DB;


class Usuarios extends Controller
{
    public function inicioEmpleado()
    {
        $objetos = DB::select('select * from v_objetosperdidos');
        $peticiones = DB::select('select * from v_peticionestado');
        $mytime =  \Carbon\Carbon::now();
        $timeNow = $mytime->format('m-Y');
        return view('inicioEmpleado', compact('peticiones','timeNow','objetos'));
    }

    public function datosPersonales(Request $request)
    {
        $usuario = User::find($request->IdentificacionUsuario);
        $usuario->Correo = $request->Correo;
        $usuario->Telefono = $request->Telefono;
        $usuario->save();

        $msj = "Se actualizo correctamente";

        return ['success' => $msj];
    }

    public function cambiarClave(Request $request)
    {

        
        $reglas = [
            'clave' => ['required','password'],
            'clave_nueva' => ['required','min:6','max:20'],
            'confirmacion_clave' => ['required','same:clave_nueva']
        ];

        $validador = Validator::make($request->all(),$reglas);

        if($validador->fails()){
            return back()->withErrors($validador);
        }

        $clave = Hash::make($request->clave_nueva);
        $usuario = User::find($request->Identificacion);
        $usuario->Clave = $clave;
        $usuario->save();
        Auth::logout();
        Session::flush();
        return redirect('/');
    }

    public function nuevaClave(Request $request)
    {
        $reglas = [
            'clave_nueva' => ['required','min:6','max:20'],
            'confirmacion_clave' => ['required','same:clave_nueva'],
        ];

        $validador = Validator::make($request->all(),$reglas);

        if($validador->fails()){
            return back()->withErrors($validador);
        }


        $usuario = User::find($request->Identificacion);
        $usuario->Clave =  Hash::make($request->clave_nueva);
        $usuario->Estado = 1;
        $usuario->save();
 
        return back()->with('mensaje','Se ha cambiado su contraseña satisfactoriamente');
    }

    public function getRecuperarClave(){
        return view('recuperarClave');
    }    
    public function postRecuperarClave(Request $request){
        $reglas = [
            'Correo' => 'required|email'
        ];

        $messages = [
            'Correo.required' => "Su correo electronico es requerido",
            'Correo.email' => "El formato de su correo electronico es invalido"
        ];

        $validador = Validator::make($request->all(),$reglas, $messages);

        if($validador->fails()){
            return back()->withErrors($validador);
        }

        $usuario = User::select('Correo','Nombres','Apellidos')->where('Correo',$request->Correo)->first();

        if ($usuario == null) {
            return back()->with('mensaje','Este correo electronico no existe');
        }

        $codigo = rand(100000,999999);
        $u = User::find(1000579646);
        $u->Clave_Codigo = $codigo;

        if ($u->save()) {
            Mail::to($usuario->Correo)
            ->send(new RecuperarClave($usuario,$codigo)); 
    
            return redirect('resetearClave?Correo='.$usuario->Correo);
        }

        return back()->with('mensaje','Error al solicitar la recuperacion de contraseña');
    }

    public function getResetearClave(Request $request)
    {
        $Correo = $request->Correo;
        return view('auth.passwords.reset',compact('Correo'));
    }
    
    public function postResetearClave(Request $request)
    {
        $reglas = [
            'Correo' => 'required|email',
            'Codigo' => 'required|numeric'
        ];

        $messages = [
            'Correo.required' => "Su correo electronico es requerido",
            'Correo.email' => "El formato de su correo electronico es invalido",
            'Codigo.required' => "El codigo de recuperación es requerido"
        ];

        $validador = Validator::make($request->all(),$reglas, $messages);

        if($validador->fails()){
            return back()->withErrors($validador);
        }

        $usuario = User::where('Correo',$request->Correo)->where('Clave_Codigo',$request->Codigo)->first();
        if ($usuario == null) {
            return back()->with('mensaje','El correo electrónico o el codigo de recuperación son erróneos.');
        }
        
        $Clave = Str::random(8);
        $usuario->Clave = Hash::make($Clave);
        $usuario->Clave_Codigo = null;
        $usuario->Estado = 12;

        if ($usuario->save()) {
            Mail::to($usuario->Correo)
            ->send(new ResetearClave($usuario,$Clave)); 
    
            return redirect('login')->with('mensaje','La contraseña se ha restablecido correctamente');
        }
        return back()->with('mensaje','Error al solicitar nueva contraseña');
    }

    
    public function estadisticas(){
        $fecha = date('Y-m-d');
        $dias = cal_days_in_month(CAL_GREGORIAN,  date('m', strtotime($fecha)),  date('Y', strtotime($fecha)));
        $objetosPerdidos = array();
        $objetosDesechados = array();
        $objetosEntregados = array();
        $objetoP = 0;
        $objetoD = 0;
        $objetoE = 0;
        $ObjetosPerdidosAnio = DB::select('call P_EstadisticasObjetosAnio(?,?)', [$fecha, 3]);
        for ($i=1; $i < $dias+1 ; $i++) { 
            $fecha =  date('Y', strtotime($fecha))."-".date('m', strtotime($fecha))."-".$i;
            $objetoP = DB::select('call P_EstadisticasObjetosDia(?,?)', [$fecha, 3]);
            $objetoD = DB::select('call P_EstadisticasObjetosDia(?,?)', [$fecha, 4]);
            $objetoE = DB::select('call P_EstadisticasObjetosDia(?,?)', [$fecha, 5]);
            array_push($objetosPerdidos, $objetoP[0]->objeto);
            array_push($objetosDesechados, $objetoD[0]->objeto);
            array_push($objetosEntregados, $objetoE[0]->objeto);
        }
        return ['success' => true ,'objetosPerdidos' => $objetosPerdidos,'objetosDesechados' => $objetosDesechados,'objetosEntregados' => $objetosEntregados,'objetosPerdidosAnio' => $ObjetosPerdidosAnio];
    }

}
