<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecuperarClave extends Mailable
{
    use Queueable, SerializesModels;

    public $usuario;
    public $codigo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($usuario,$codigo)
    {
        $this->usuario = $usuario;
        $this->codigo = $codigo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Recuperar contraseña')->view('emails.recuperarClave');
    }
}
