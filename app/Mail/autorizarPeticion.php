<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class autorizarPeticion extends Mailable
{
    use Queueable, SerializesModels;

    public $datosCliente;
    public $datosPeticion;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datosCliente, $datosPeticion)
    {
        $this->datosCliente = $datosCliente;
        $this->datosPeticion = $datosPeticion;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Peticion No. '.$this->datosPeticion->idPeticionSolicitud)->view('emails.autorizarPeticion');
    }
}
