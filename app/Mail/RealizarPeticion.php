<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RealizarPeticion extends Mailable
{
    use Queueable, SerializesModels;


    public $info;
    public $id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $peticion)
    {
        $this->info = $data;
        $this->id = $peticion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Datos de la peticion No. '.$this->id[0]->idpeticion)->view('emails.realizarPeticion');
    }
}
